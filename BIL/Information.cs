﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIL
{
    public class information
    {
        public string Connoracle {  get; set; }
        public string Course { get; set; }
        public string Label { get; set; }
        public int Stu_label { get; set; }
        public string Faculty { get; set; }
        public int Stu_fac { get; set; }
        public int Course_id { get; set; }
        public string questions { get; set; }
        public int questions_id { get; set; }
        public string Description { get; set; }

    }
}
