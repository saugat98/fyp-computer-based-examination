﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Threading;

namespace UserInterface
{
    public partial class signin :Form
    {
        public signin()
        {
            Thread th = new Thread(new ThreadStart(loadprofile));
            th.Start();
            Thread.Sleep(2300);
            
            InitializeComponent();
            th.Abort();
        }

        private void signin_Load(object sender, EventArgs e)
        {
            this.BringToFront();
        }
        public void loadprofile()
        {
            Application.Run(new profile());
        }

        private void btncalcel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public void loading_form()
        {

            Application.Run(new loading());
        }
        private void btnlogin_Click(object sender, EventArgs e)
        {
            if (LogIn_form_validation())
            {
                Thread th = new Thread(new ThreadStart(loading_form));
             
                th.Start();
                Thread.Sleep(2200);
                main mn = new main();
                mn.Show();
                this.Hide();
                mn.labelusername.Text = textusername.Text;
                th.Abort();
               
               
            }

      
            
        }

        private void textuserpassword_MouseClick(object sender, MouseEventArgs e)
        {
            textuserpassword.ForeColor = Color.DimGray;
            if(!textuserpassword.Text.Equals( "Enter Password Here"))
            {
                textuserpassword.PasswordChar= '\u2022';
            }
           
            else {
                textuserpassword.Text = "";

            }
            
        }

        private void textuserpassword_TextChanged(object sender, EventArgs e)
        {
          
                textuserpassword.PasswordChar = '\u2022';
           
        }

        private void textuserpassword_MouseLeave(object sender, EventArgs e)
        {
            if (textuserpassword.Text.Equals(""))
            {

                textuserpassword.Text = "Enter Password Here";
                textuserpassword.PasswordChar = '\0';
                textuserpassword.ForeColor = Color.LightGray;
            }
            else
            {

            }
        }

    
        private void textuserpassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (textuserpassword.Text.Equals(""))
            {

                textuserpassword.Text = "Enter Password Here";
                textuserpassword.PasswordChar = '\0';
                textuserpassword.ForeColor = Color.LightGray;
            }
            else
            {

            }
        }

        private void textusername_MouseClick(object sender, MouseEventArgs e)
        {
            if (textusername.Text.Equals("Enter Username Here"))
            {
                textusername.Text = "";
                textusername.ForeColor = Color.DimGray;
            }
            
        }

        private void textusername_KeyUp(object sender, KeyEventArgs e)
        {
            if (textusername.Text.Equals(""))
            {

                textusername.Text = "Enter Username Here";
                textusername.ForeColor = Color.LightGray;
            }
            else
            {

            }

        }



        private void textusername_MouseLeave(object sender, EventArgs e)
        {
            if (textusername.Text.Equals(""))
            {

                textusername.Text = "Enter Username Here";
                textusername.ForeColor = Color.LightGray;
            }
            else
            {

            }
        }

       public bool LogIn_form_validation()
        {
            if (textusername.Text.Equals(string.Empty) || textuserpassword.Text.Equals(string.Empty)|| textusername.Text.Equals("Enter Username Here") || textuserpassword.Text.Equals("Enter Password Here"))
            {
                MessageBox.Show(this, "Enter username and password","Form Empty Validation",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private void textusername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textusername.Text.Equals("Enter Username Here"))
            {

                textusername.Text = "";
            }
           
        }

       
    }
}
