﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface
{
    public partial class profile : Form
    {
        public profile()
        {
            
            InitializeComponent();
        }
        
        private void timersplash_Tick(object sender, EventArgs e)
        {
           
            int range= bunifuCircleProgressbar.Value+3;
            if (range >= 100)
            {
                timersplash.Stop();
            }
            else
            {
                bunifuCircleProgressbar.Value = range;
            }
        }
    }
}
