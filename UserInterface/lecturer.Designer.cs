﻿namespace UserInterface
{
    partial class lecturer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(lecturer));
            this.panelcontainer = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.panelquesans = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnview = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnadd = new Bunifu.Framework.UI.BunifuFlatButton();
            this.admissionpanelheader = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.panelcontainer.SuspendLayout();
            this.admissionpanelheader.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelcontainer
            // 
            this.panelcontainer.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panelcontainer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelcontainer.BackgroundImage")));
            this.panelcontainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelcontainer.Controls.Add(this.panelquesans);
            this.panelcontainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelcontainer.GradientBottomLeft = System.Drawing.Color.WhiteSmoke;
            this.panelcontainer.GradientBottomRight = System.Drawing.Color.WhiteSmoke;
            this.panelcontainer.GradientTopLeft = System.Drawing.Color.White;
            this.panelcontainer.GradientTopRight = System.Drawing.Color.White;
            this.panelcontainer.Location = new System.Drawing.Point(0, 64);
            this.panelcontainer.Name = "panelcontainer";
            this.panelcontainer.Quality = 10;
            this.panelcontainer.Size = new System.Drawing.Size(1074, 618);
            this.panelcontainer.TabIndex = 3;
            // 
            // panelquesans
            // 
            this.panelquesans.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelquesans.BackgroundImage")));
            this.panelquesans.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelquesans.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelquesans.GradientBottomLeft = System.Drawing.Color.White;
            this.panelquesans.GradientBottomRight = System.Drawing.Color.White;
            this.panelquesans.GradientTopLeft = System.Drawing.Color.White;
            this.panelquesans.GradientTopRight = System.Drawing.Color.White;
            this.panelquesans.Location = new System.Drawing.Point(0, 0);
            this.panelquesans.Name = "panelquesans";
            this.panelquesans.Quality = 10;
            this.panelquesans.Size = new System.Drawing.Size(1074, 618);
            this.panelquesans.TabIndex = 1;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(368, 10);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(226, 26);
            this.bunifuCustomLabel1.TabIndex = 0;
            this.bunifuCustomLabel1.Text = "Questions && Answers section";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Minion Pro", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(409, 37);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(100, 17);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "Only for office use";
            // 
            // btnview
            // 
            this.btnview.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnview.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnview.BorderRadius = 5;
            this.btnview.ButtonText = "View";
            this.btnview.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnview.DisabledColor = System.Drawing.Color.Gray;
            this.btnview.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnview.Iconcolor = System.Drawing.Color.Transparent;
            this.btnview.Iconimage = global::UserInterface.Properties.Resources.Document_96px;
            this.btnview.Iconimage_right = null;
            this.btnview.Iconimage_right_Selected = null;
            this.btnview.Iconimage_Selected = null;
            this.btnview.IconMarginLeft = 0;
            this.btnview.IconMarginRight = 0;
            this.btnview.IconRightVisible = true;
            this.btnview.IconRightZoom = 0D;
            this.btnview.IconVisible = true;
            this.btnview.IconZoom = 90D;
            this.btnview.IsTab = true;
            this.btnview.Location = new System.Drawing.Point(941, 10);
            this.btnview.Name = "btnview";
            this.btnview.Normalcolor = System.Drawing.Color.RoyalBlue;
            this.btnview.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnview.OnHoverTextColor = System.Drawing.Color.White;
            this.btnview.selected = false;
            this.btnview.Size = new System.Drawing.Size(123, 44);
            this.btnview.TabIndex = 3;
            this.btnview.Text = "View";
            this.btnview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnview.Textcolor = System.Drawing.Color.White;
            this.btnview.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnadd
            // 
            this.btnadd.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnadd.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnadd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnadd.BorderRadius = 5;
            this.btnadd.ButtonText = "Add";
            this.btnadd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnadd.DisabledColor = System.Drawing.Color.Gray;
            this.btnadd.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnadd.Iconcolor = System.Drawing.Color.Transparent;
            this.btnadd.Iconimage = global::UserInterface.Properties.Resources.Plus_100px;
            this.btnadd.Iconimage_right = null;
            this.btnadd.Iconimage_right_Selected = null;
            this.btnadd.Iconimage_Selected = null;
            this.btnadd.IconMarginLeft = 0;
            this.btnadd.IconMarginRight = 0;
            this.btnadd.IconRightVisible = true;
            this.btnadd.IconRightZoom = 0D;
            this.btnadd.IconVisible = true;
            this.btnadd.IconZoom = 90D;
            this.btnadd.IsTab = true;
            this.btnadd.Location = new System.Drawing.Point(818, 10);
            this.btnadd.Name = "btnadd";
            this.btnadd.Normalcolor = System.Drawing.Color.RoyalBlue;
            this.btnadd.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnadd.OnHoverTextColor = System.Drawing.Color.White;
            this.btnadd.selected = false;
            this.btnadd.Size = new System.Drawing.Size(123, 44);
            this.btnadd.TabIndex = 5;
            this.btnadd.Text = "Add";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Textcolor = System.Drawing.Color.White;
            this.btnadd.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // admissionpanelheader
            // 
            this.admissionpanelheader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.admissionpanelheader.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("admissionpanelheader.BackgroundImage")));
            this.admissionpanelheader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.admissionpanelheader.Controls.Add(this.btnadd);
            this.admissionpanelheader.Controls.Add(this.btnview);
            this.admissionpanelheader.Controls.Add(this.bunifuCustomLabel2);
            this.admissionpanelheader.Controls.Add(this.bunifuCustomLabel1);
            this.admissionpanelheader.Dock = System.Windows.Forms.DockStyle.Top;
            this.admissionpanelheader.GradientBottomLeft = System.Drawing.Color.DarkGray;
            this.admissionpanelheader.GradientBottomRight = System.Drawing.Color.DarkGray;
            this.admissionpanelheader.GradientTopLeft = System.Drawing.Color.White;
            this.admissionpanelheader.GradientTopRight = System.Drawing.Color.White;
            this.admissionpanelheader.Location = new System.Drawing.Point(0, 0);
            this.admissionpanelheader.Name = "admissionpanelheader";
            this.admissionpanelheader.Padding = new System.Windows.Forms.Padding(10);
            this.admissionpanelheader.Quality = 10;
            this.admissionpanelheader.Size = new System.Drawing.Size(1074, 64);
            this.admissionpanelheader.TabIndex = 2;
            // 
            // lecturer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelcontainer);
            this.Controls.Add(this.admissionpanelheader);
            this.Name = "lecturer";
            this.Size = new System.Drawing.Size(1074, 682);
            this.panelcontainer.ResumeLayout(false);
            this.admissionpanelheader.ResumeLayout(false);
            this.admissionpanelheader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuGradientPanel panelcontainer;
        private Bunifu.Framework.UI.BunifuGradientPanel panelquesans;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        public Bunifu.Framework.UI.BunifuFlatButton btnview;
        public Bunifu.Framework.UI.BunifuFlatButton btnadd;
        public Bunifu.Framework.UI.BunifuGradientPanel admissionpanelheader;
    }
}
