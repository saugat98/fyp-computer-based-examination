﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface
{
    public partial class examination : UserControl
    {
        public examination()
        {

            InitializeComponent();
            examtime.Enabled = false;
            lbltime.Text = "00";
            lbltimehr.Text = "00";
            lbltimemin.Text = "00";
            Exampanelcontainer.Visible = false;
            Buttonstop.Enabled = false;
            Buttonpause.Enabled = false;
            Buttonstop.BackColor = Color.Gray;
            Buttonpause.BackColor = Color.Gray;
            buttonsubmit.Visible = false;
        }
        int timeLeftsec;
        int timeleftmin;
        int timelefthr;
       
        private void examtime_Tick(object sender, EventArgs e)
        { 
            
            timeLeftsec = timeLeftsec - 1;
            if (timeLeftsec == -1)
            {
                timeleftmin = timeleftmin - 1;
                timeLeftsec = 59;

            }
            if (timeleftmin == -1)
            {
                timelefthr = timelefthr - 1;
                timeleftmin = 59;
            }
            if (timeleftmin==15)
            {
               // System.Media.SystemSounds..Play();
                lbltimemin.ForeColor = Color.Red;
                
                lbltimehr.ForeColor = Color.Red;
               // MessageBox.Show(this,"Warning! Time is less than 15 Minutes","Time",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                lbltime.ForeColor = Color.Red;
              
            }
            if (timeleftmin==0 && timelefthr==0 && timeLeftsec==0)
            {
                examtime.Stop();
                lbltime.Hide();
                lbltimehr.Hide();
                labeldot.Hide();
                labeldot1.Hide();
                buttonsubmit.Enabled = true;
                lbltimemin.Text = "Time's Up!";
                lbltimemin.ForeColor = Color.Red;
                lbltimehr.ForeColor = Color.Red;
                lbltime.ForeColor = Color.Red;
                MessageBox.Show(this, "Sorry, Time Is Over", "Time Expired", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);


            }

            lbltime.Text = timeLeftsec.ToString();
            lbltimehr.Text = timelefthr.ToString();
            lbltimemin.Text = timeleftmin.ToString();
            
        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show(this, "Are You Ready For examination?", "Examination Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                examtime.Enabled = true;
                timeLeftsec = 59;
                timeleftmin = 12;
                timelefthr = 1;
                examtime.Start();
                Exampanelcontainer.Visible = true;
                ButtonStart.Enabled = false;
                Buttonstop.Enabled = true;
                Buttonpause.Enabled = true;
                Buttonpause.BackColor = Color.SteelBlue;
                Buttonstop.BackColor = Color.SteelBlue;
                ButtonStart.BackColor = Color.Gray;
                Create_Group_box(g + 1);
                Create_Group_box(g+1);
            }
           
        }

        private void Buttonstop_Click(object sender, EventArgs e)
        {
            StopExamination();

        }

        private void StopExamination()
        {
            DialogResult res = MessageBox.Show(this, "Are You Sure to stop examination?", "Examination Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                examtime.Enabled = false;
                examtime.Stop();
                lbltime.Text = "00";
                lbltimehr.Text = "00";
                lbltimemin.Text = "00";
                Exampanelcontainer.Visible = false;
                ButtonStart.Enabled = true;
                Buttonstop.Enabled = false;
                Buttonpause.Enabled = false;
                Buttonpause.Image = Properties.Resources.pause_button_png_29651;
                g = 1;
                Create_Group_box(g + 1);
                ButtonStart.BackColor = Color.SteelBlue;
                Buttonstop.BackColor = Color.Gray;
                Buttonpause.BackColor = Color.Gray;
               
            }
        }
        private void SubmitExamination()
        {
            DialogResult res = MessageBox.Show(this, "Are You Sure to Submit this examination?", "Examination Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                examtime.Enabled = false;
                examtime.Stop();
                lbltime.Text = "00";
                lbltimehr.Text = "00";
                lbltimemin.Text = "00";
                Exampanelcontainer.Visible = false;
               
                Buttonstop.Enabled = false;
                Buttonpause.Enabled = false;
                Buttonpause.Image = Properties.Resources.pause_button_png_29651;
                g = 1;
                Create_Group_box(g + 1);
               
                Buttonstop.BackColor = Color.Gray;
                Buttonpause.BackColor = Color.Gray;
                paneltimer.Hide();
                
            }
        }
        private void Buttonpause_Click(object sender, EventArgs e)
        {
           
            if (examtime.Enabled == true)
            {
                examtime.Enabled = false;
                examtime.Stop();
                Buttonpause.Image = Properties.Resources.play_icon;
                Buttonpause.BackColor = Color.DodgerBlue;
            }
            else
            {
                    examtime.Enabled = true;
                    examtime.Start();
                    Buttonpause.Image = Properties.Resources.pause_button_png_29651;
                    Buttonpause.BackColor = Color.SteelBlue;
            }

        }

        private void ButtonStart_MouseHover(object sender, EventArgs e)
        {
           
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.ButtonStart, "Start Exam");
        }

        private void Buttonpause_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
           
            if (examtime.Enabled == true)
            {
                tt.SetToolTip(this.Buttonpause, "Pause Exam");
            }
            else
            {
                tt.SetToolTip(this.Buttonpause, "Start Exam");
            }
        }

        private void Buttonstop_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.Buttonstop, "Stop Exam");
        }

        int g = 1;
        public void Create_Group_box(int group_id)
        {
          
                GroupBox group = new GroupBox();
                this.Exampanelcontainer.Controls.Add(group);
                group.Top = g * 60;
                group.Left = 3;
                group.BackColor = Color.White;
                group.ForeColor = Color.SteelBlue;
                group.Font = new Font("Arial", 12, FontStyle.Bold);
                group.Dock = DockStyle.Top;
                group.Height = ((this.Exampanelcontainer.Height - this.panelbottom.Height) / 2) - 10;
             
                group.Text = "Question " + this.g.ToString();
                g = group_id;

                if (g == 1)
                {
                    this.panelPrev.Hide();
                }

                else if (g > 10)
                {
                this.panelNext.Hide();


                    SubmitExamination();
                    buttonsubmit.Visible = true;
               
               

                }
                else
                {
                    this.panelPrev.Show();
                    this.panelNext.Show();
                    buttonsubmit.Visible = false;
                }
            group.Controls.Add(Create_radio("Answer 4"));
            group.Controls.Add(Create_radio("Answer 3"));
            group.Controls.Add(Create_radio("Answer 2"));
            group.Controls.Add(Create_radio("Answer 1"));
            group.Controls.Add(Create_label("This is a Description ", FontFamily.GenericSansSerif, 9, Color.SlateGray));
            group.Controls.Add(Create_label("This is a question ", FontFamily.GenericSansSerif, 12, Color.FromArgb(64, 64, 64)));





        }

        public Label Create_label(String value, FontFamily fontfamily,int size,Color textcolor)
        {
            Label lbl = new Label();
            lbl.Text = value+g.ToString();
            lbl.Font = new Font(fontfamily, size);
            lbl.ForeColor = textcolor;
            lbl.Top = g * 20;
            lbl.Left = 3;
            lbl.BackColor = Color.Transparent;
            lbl.Dock = DockStyle.Top;
           
            return lbl;
        }
        public RadioButton Create_radio(string value)
        {
            RadioButton radio = new RadioButton();
            radio.Top = g * 50;
            radio.Padding = new Padding(10,3,0,0);
            radio.ForeColor = Color.FromArgb(64,64,64);
            radio.Font =new Font( FontFamily.GenericSansSerif,12);
            radio.Text = value;
            radio.BackColor = Color.Transparent;
            radio.Dock = DockStyle.Top;
            return radio;
        }

        private void buttonnext_Click(object sender, EventArgs e)
        {
            
            Create_Group_box(g+1);
           

        }

        private void buttonprev_Click(object sender, EventArgs e)
        {
          
                Create_Group_box(g-1);
           
            
        }
    }
}
