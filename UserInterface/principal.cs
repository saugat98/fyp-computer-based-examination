﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BLL;
using BIL;
namespace UserInterface
{
    public partial class principal : UserControl
    {
        Form_validation valid = new Form_validation();
        information info = new information();
        Database sql = new Database();
        public principal()
        {
            InitializeComponent();
            textcourse.Text = "Enter Course Name Here";
            coursedesc.Text = "Describe Course In Details";
            coursedesc.ForeColor = Color.LightGray;
            textsecurityquestion.Text = "Enter Security Questions";
            textsecurityquestion.ForeColor = Color.LightGray;
            textdesc.Text = "Describe in details";
            textdesc.ForeColor = Color.LightGray;
            combofaculty.SelectedIndex = 0;
            combolabel.SelectedIndex = 0;
            DisplayCourse();
            DisplayQuestions();
        }

        private void DisplayCourse()
        {
            for (int i = 0; i < sql.Display_course().Rows.Count; i++)
            {

                dataGridViewcourse.Rows.Add();
                dataGridViewcourse.Rows[i].Cells["colsn"].Value = i + 1;
                dataGridViewcourse.Rows[i].Cells["colCourse"].Value = sql.Display_course().Rows[i]["Course_name"].ToString();
                dataGridViewcourse.Rows[i].Cells["colcid"].Value = sql.Display_course().Rows[i]["course_id"].ToString();
                dataGridViewcourse.Rows[i].Cells["coldescription"].Value = sql.Display_course().Rows[i]["description"].ToString();
                dataGridViewcourse.Rows[i].Cells["collabel"].Value = sql.Display_course().Rows[i]["label"].ToString();
                dataGridViewcourse.Rows[i].Cells["colfaculty"].Value = sql.Display_course().Rows[i]["faculty"].ToString();
            }
        }
        private void DisplayQuestions()
        {
            for (int i = 0; i < sql.Display_Security().Rows.Count; i++)
            {

                dataGridViewsecurityquestions.Rows.Add();
                dataGridViewsecurityquestions.Rows[i].Cells["colsid"].Value = i + 1;
                dataGridViewsecurityquestions.Rows[i].Cells["colqid"].Value = sql.Display_Security().Rows[i]["question_id"].ToString();
                dataGridViewsecurityquestions.Rows[i].Cells["colquestion"].Value = sql.Display_Security().Rows[i]["questions_title"].ToString();
                dataGridViewsecurityquestions.Rows[i].Cells["coldesc"].Value = sql.Display_Security().Rows[i]["Questions_desc"].ToString();

            }
        }

        int courseid =0;
        private void btnsubmit_Click(object sender, EventArgs e)
        {
            
            info.Course = textcourse.Text;
            info.Description = coursedesc.Text;
            info.Faculty = combofaculty.SelectedItem.ToString();
            info.Label = combolabel.SelectedItem.ToString();
            info.Stu_label = combolabel.SelectedIndex;
            info.Stu_fac= combofaculty.SelectedIndex;
            info.Course_id = courseid;
            
            if (valid.Course_empty_validation(info))
            {

                if (btnsubmit.Text == "Submit") { 
                if (sql.Check_existed_course(info))
                {
                    DialogResult res = MessageBox.Show(this, "Do You Want To Register?", "Register Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (res == DialogResult.Yes)
                {
                    
                    if (sql.Course_register(info))
                    {
                        MessageBox.Show(this, "Sucessfully Registered this course", "Course Add", MessageBoxButtons.OK, MessageBoxIcon.None);
                        ClearCourseform();
                            DisplayCourse();
                        }

                    }
                }
                else
                {
                    ClearCourseform();
                        DisplayCourse();
                    }
                }
                else
                {
                    if (sql.Check_existed_course_update(info))
                    {
                        DialogResult res = MessageBox.Show(this, "Do You Want To Update?", "Update Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                        if (res == DialogResult.Yes)
                        {

                            if (sql.Course_update(info))
                            {
                                MessageBox.Show(this, "Sucessfully Updated this course", "Course Update", MessageBoxButtons.OK, MessageBoxIcon.None);
                                ClearCourseform();
                                DisplayCourse();
                            }

                        }
                    }
                    else
                    {
                        ClearCourseform();
                        DisplayCourse();
                    }
                }


            }
        }

        private void textcourse_KeyUp(object sender, KeyEventArgs e)
        {
            if (textcourse.Text.Equals(""))
            {

                textcourse.Text = "Enter Course Name Here";
                textcourse.ForeColor = Color.LightGray;
            }
        }

        private void textcourse_MouseClick(object sender, MouseEventArgs e)
        {
            if (textcourse.Text.Equals("Enter Course Name Here"))
            {
                textcourse.Text = "";
                textcourse.ForeColor = Color.DimGray;
            }
        }

        private void textcourse_MouseLeave(object sender, EventArgs e)
        {
            if (textcourse.Text.Equals(""))
            {

                textcourse.Text = "Enter Course Name Here";
                textcourse.ForeColor = Color.LightGray;
            }
        }

        private void coursedesc_MouseClick(object sender, MouseEventArgs e)
        {
            if (coursedesc.Text.Equals("Describe Course In Details"))
            {
                coursedesc.Text = "";
                coursedesc.ForeColor = Color.DimGray;
            }
        }

        private void coursedesc_MouseLeave(object sender, EventArgs e)
        {
            if (coursedesc.Text.Equals(""))
            {

                coursedesc.Text = "Describe Course In Details";
                coursedesc.ForeColor = Color.LightGray;
            }
        }

        private void coursedesc_KeyUp(object sender, KeyEventArgs e)
        {
            if (coursedesc.Text.Equals(""))
            {

                coursedesc.Text = "Describe Course In Details";
                coursedesc.ForeColor = Color.LightGray;
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            ClearCourseform();
            DisplayCourse();
        }

        private void ClearCourseform()
        {
            combofaculty.SelectedIndex = 0;
            combolabel.SelectedIndex = 0;
            coursedesc.Text = "Describe Course In Details";
            coursedesc.ForeColor = Color.LightGray;
            textcourse.Text = "Enter Course Name Here";
            textcourse.ForeColor = Color.LightGray;
            dataGridViewcourse.Rows.Clear();
            btnsubmit.Text = "Submit";
            buttondelete.Visible = false;
        }
        private void ClearSecurityform()
        {
            textsecurityquestion.Text = "Enter Security Questions";
            textsecurityquestion.ForeColor = Color.LightGray;
            textdesc.Text = "Describe in details";
            textdesc.ForeColor = Color.LightGray;
            dataGridViewsecurityquestions.Rows.Clear();

        }

        private void textsecurityquestion_Click(object sender, EventArgs e)
        {
            if (textsecurityquestion.Text.Equals("Enter Security Questions"))
            {
                textsecurityquestion.Text = "";
                textsecurityquestion.ForeColor = Color.DimGray;
            }
        }

        private void textsecurityquestion_KeyUp(object sender, KeyEventArgs e)
        {
            if (textsecurityquestion.Text.Equals(""))
            {

                textsecurityquestion.Text = "Enter Security Questions";
                textsecurityquestion.ForeColor = Color.LightGray;
            }
        }

        private void textsecurityquestion_MouseLeave(object sender, EventArgs e)
        {
            if (textsecurityquestion.Text.Equals(""))
            {

                textsecurityquestion.Text = "Enter Security Questions";
                textsecurityquestion.ForeColor = Color.LightGray;
            }
        }

        private void textdesc_Click(object sender, EventArgs e)
        {
            if (textdesc.Text.Equals("Describe in details"))
            {
                textdesc.Text = "";
                textdesc.ForeColor = Color.DimGray;
            }
        }

        private void textdesc_KeyUp(object sender, KeyEventArgs e)
        {
            if (textdesc.Text.Equals(""))
            {

                textdesc.Text = "Describe in details";
                textdesc.ForeColor = Color.LightGray;
            }
        }

        private void textdesc_MouseLeave(object sender, EventArgs e)
        {
            if (textdesc.Text.Equals(""))
            {

                textdesc.Text = "Describe in details";
                textdesc.ForeColor = Color.LightGray;
            }
        }

        private void buttonsecuritysubmit_Click(object sender, EventArgs e)
        {
           
            info.Description = textdesc.Text;
            info.questions = textsecurityquestion.Text;
            if (valid.Security_empty_validation(info))
            {
                if (sql.Check_existed_security(info))
                {
                    DialogResult res = MessageBox.Show(this, "Do You Want To Register?", "Register Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (res == DialogResult.Yes)
                    {

                        if (sql.RegisterSecurity(info))
                        {
                            MessageBox.Show(this, "Sucessfully Registered this Questions", "Security Questions", MessageBoxButtons.OK, MessageBoxIcon.None);
                            ClearSecurityform();
                            DisplayQuestions();
                        }


                    }

                }

            }
            else
            {
                ClearSecurityform();
                DisplayQuestions();
            }

        }

        private void buttonsecurityclear_Click(object sender, EventArgs e)
        {
            ClearSecurityform();
            DisplayQuestions();

        }

        private void dataGridViewcourse_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           
            info.Course_id = Convert.ToInt32(dataGridViewcourse.CurrentRow.Cells[1].Value.ToString());
            courseid = info.Course_id;
            textcourse.Text= sql.View_for_Update_course(info).Rows[0]["course_name"].ToString();
            coursedesc.Text= sql.View_for_Update_course(info).Rows[0]["description"].ToString();
            combofaculty.SelectedItem= sql.View_for_Update_course(info).Rows[0]["faculty"].ToString();
            combolabel.SelectedItem = sql.View_for_Update_course(info).Rows[0]["label"].ToString();
            textcourse.ForeColor = Color.FromArgb(64,64,64,64);
            coursedesc.ForeColor = Color.FromArgb(64, 64, 64, 64);
            btnsubmit.Text = "Update";
            buttondelete.Visible = true;
            
        }

        private void buttondelete_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show(this, "Do You Want To Delete This rows?", "Delete Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (res == DialogResult.Yes)
            {

                if (sql.Course_Delete(info))
                {
                    MessageBox.Show(this, "Sucessfully Deleted this Course", "Course Delete", MessageBoxButtons.OK, MessageBoxIcon.None);
                    ClearCourseform();
                    DisplayCourse();
                }


            }

        }
    }
}
