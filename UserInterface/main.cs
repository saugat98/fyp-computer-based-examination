﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace UserInterface
{
    public partial class main : Form
    {
      

        public main()
        {
            //loadthread = new LoadDelegate(this.loading_form);
            //IAsyncResult handle = loadthread.BeginInvoke(null, null);
            //handle.AsyncWaitHandle.WaitOne(500);
            InitializeComponent();
            hideusercontrol();
            panelcontainer.Controls.Add(hr);
            hr.Dock = DockStyle.Fill;
            hr.Show();

        }

        home hr = new home();
        admission admit = new admission();
        lecturer stf = new lecturer();
        examination exm = new examination();
        examoffice eoff = new examoffice();
        principal pal = new principal();
        review rew = new review();
        documentissue dociss = new documentissue();
        help hp = new help();
        public void hideusercontrol()
        {
            hr.Hide();
            admit.Hide();
            stf.Hide();
            exm.Hide();
            eoff.Hide();
            pal.Hide();
            rew.Hide();
            dociss.Hide();
            hp.Hide();
        }
        private void imagebuttonminimize_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void imagebuttonexit_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        public void loading_form()
        {
            Application.Run(new loading());
        }

        private void btnhome_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(new ThreadStart(loading_form));
            hideusercontrol();
            //th.Start();
            //Thread.Sleep(1000);
            hr.Dock = DockStyle.Fill;
            panelcontainer.Controls.Add(hr);
            hr.Show();
            //th.Abort();
        }

        private void btnadmission_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(new ThreadStart(loading_form));
            hideusercontrol();
            //th.Start();
            //Thread.Sleep(1000);
            admit.Dock = DockStyle.Fill;
            panelcontainer.Controls.Add(admit);
            admit.Show();
            th.Abort();
        }

        private void btnexam_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(new ThreadStart(loading_form));
            hideusercontrol();
            //th.Start();
            //Thread.Sleep(1000);
            exm.Dock = DockStyle.Fill;
            panelcontainer.Controls.Add(exm);
            exm.Show();
            th.Abort();
        }

        private void btnstaff_Click(object sender, EventArgs e)
        {
            //Thread th = new Thread(new ThreadStart(loading_form));
            hideusercontrol();
            //th.Start();
            //Thread.Sleep(1000);
            stf.Dock = DockStyle.Fill;
            panelcontainer.Controls.Add(stf);
            stf.Show();
            //th.Abort();
        }

        private void btncollegehead_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(new ThreadStart(loading_form));
            hideusercontrol();
            //    th.Start();
            //    Thread.Sleep(1000);
            pal.Dock = DockStyle.Fill;
            panelcontainer.Controls.Add(pal);
            pal.Show();
            //th.Abort();
        }

        private void btnexamoffice_Click(object sender, EventArgs e)
        {
            //Thread th = new Thread(new ThreadStart(loading_form));
            hideusercontrol();
            //th.Start();
            //Thread.Sleep(1000);
            eoff.Dock = DockStyle.Fill;
            panelcontainer.Controls.Add(eoff);
            eoff.Show();
            //th.Abort();
        }

        private void btndocument_Click(object sender, EventArgs e)
        {
            //Thread th = new Thread(new ThreadStart(loading_form));
            hideusercontrol();
            //th.Start();
            //Thread.Sleep(1000);
            dociss.Dock = DockStyle.Fill;
            panelcontainer.Controls.Add(dociss);
            dociss.Show();
            //th.Abort();
        }

        private void btnreview_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(new ThreadStart(loading_form));
            hideusercontrol();
            //th.Start();
            //Thread.Sleep(1000);
            rew.Dock = DockStyle.Fill;
            panelcontainer.Controls.Add(rew);
            rew.Show();
            th.Abort();
        }

        private void btnhelp_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(new ThreadStart(loading_form));
            hideusercontrol();
            //th.Start();
            //Thread.Sleep(1000);
            hp.Dock = DockStyle.Fill;
            panelcontainer.Controls.Add(hp);
            hp.Show();
            //th.Abort();
        }

        private void bunifuImageButtonlogout_Click(object sender, EventArgs e)
        {
            this.Hide();
            signin sign = new signin();
            sign.Show();

        }


        private void textsearch_KeyUp(object sender, EventArgs e)
        {
            if (textsearch.Text.Equals(""))
            {

                textsearch.Text = "Search Information";
                textsearch.ForeColor = Color.Silver;
            }
            else
            {

            }
        }

        private void textsearch_MouseClick(object sender, MouseEventArgs e)
        {
            if (textsearch.Text.Equals("Search Information"))
            {
                textsearch.Text = "";
                textsearch.ForeColor = Color.DimGray;
            }

        }

        private void textsearch_MouseLeave(object sender, EventArgs e)
        {

            if (textsearch.Text.Equals(""))
            {
                textsearch.Text = "Search Information";
                textsearch.ForeColor = Color.Silver;

            }
            else
            {

            }

        }

        private void main_Load(object sender, EventArgs e)
        {
            textsearch.Text = "";
            textsearch.Text = "Search Information";


        }

        private void labelusername_TextChanged(object sender, EventArgs e)
        {
            hr.textboxusername.Text = this.labelusername.Text;
        }
    }
}
