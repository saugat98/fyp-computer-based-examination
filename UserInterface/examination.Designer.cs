﻿namespace UserInterface
{
    partial class examination
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(examination));
            this.examtime = new System.Windows.Forms.Timer(this.components);
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.admissionpanelheader = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.buttonsubmit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Buttonpause = new System.Windows.Forms.PictureBox();
            this.Buttonstop = new System.Windows.Forms.PictureBox();
            this.ButtonStart = new System.Windows.Forms.PictureBox();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.labeldot1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.labeldot = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lbltimemin = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lbltimehr = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lbltime = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Exampanelcontainer = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.panelbottom = new System.Windows.Forms.Panel();
            this.panelNext = new System.Windows.Forms.Panel();
            this.buttonnext = new System.Windows.Forms.Button();
            this.panelPrev = new System.Windows.Forms.Panel();
            this.buttonprev = new System.Windows.Forms.Button();
            this.paneltimer = new System.Windows.Forms.Panel();
            this.bunifuGradientPanel1.SuspendLayout();
            this.admissionpanelheader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buttonpause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buttonstop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonStart)).BeginInit();
            this.Exampanelcontainer.SuspendLayout();
            this.panelbottom.SuspendLayout();
            this.panelNext.SuspendLayout();
            this.panelPrev.SuspendLayout();
            this.paneltimer.SuspendLayout();
            this.SuspendLayout();
            // 
            // examtime
            // 
            this.examtime.Enabled = true;
            this.examtime.Interval = 1000;
            this.examtime.Tick += new System.EventHandler(this.examtime_Tick);
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.admissionpanelheader);
            this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.DarkGray;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.DarkGray;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(1037, 63);
            this.bunifuGradientPanel1.TabIndex = 3;
            // 
            // admissionpanelheader
            // 
            this.admissionpanelheader.BackColor = System.Drawing.Color.SteelBlue;
            this.admissionpanelheader.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("admissionpanelheader.BackgroundImage")));
            this.admissionpanelheader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.admissionpanelheader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.admissionpanelheader.Controls.Add(this.paneltimer);
            this.admissionpanelheader.Controls.Add(this.buttonsubmit);
            this.admissionpanelheader.Controls.Add(this.Buttonpause);
            this.admissionpanelheader.Controls.Add(this.Buttonstop);
            this.admissionpanelheader.Controls.Add(this.ButtonStart);
            this.admissionpanelheader.Controls.Add(this.bunifuCustomLabel2);
            this.admissionpanelheader.Controls.Add(this.bunifuCustomLabel1);
            this.admissionpanelheader.Dock = System.Windows.Forms.DockStyle.Top;
            this.admissionpanelheader.GradientBottomLeft = System.Drawing.Color.DarkGray;
            this.admissionpanelheader.GradientBottomRight = System.Drawing.Color.DarkGray;
            this.admissionpanelheader.GradientTopLeft = System.Drawing.Color.White;
            this.admissionpanelheader.GradientTopRight = System.Drawing.Color.White;
            this.admissionpanelheader.Location = new System.Drawing.Point(0, 0);
            this.admissionpanelheader.Name = "admissionpanelheader";
            this.admissionpanelheader.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.admissionpanelheader.Quality = 10;
            this.admissionpanelheader.Size = new System.Drawing.Size(1037, 63);
            this.admissionpanelheader.TabIndex = 1;
            // 
            // buttonsubmit
            // 
            this.buttonsubmit.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonsubmit.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonsubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonsubmit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonsubmit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonsubmit.Location = new System.Drawing.Point(878, 0);
            this.buttonsubmit.Name = "buttonsubmit";
            this.buttonsubmit.Size = new System.Drawing.Size(145, 59);
            this.buttonsubmit.TabIndex = 23;
            this.buttonsubmit.Text = "Submit";
            this.buttonsubmit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonsubmit.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::UserInterface.Properties.Resources.blue_clock_PNG_Transparent_background;
            this.pictureBox1.Location = new System.Drawing.Point(0, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 63);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel4.ForeColor = System.Drawing.Color.SteelBlue;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(191, 20);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(34, 26);
            this.bunifuCustomLabel4.TabIndex = 15;
            this.bunifuCustomLabel4.Text = "sec";
            // 
            // Buttonpause
            // 
            this.Buttonpause.BackColor = System.Drawing.Color.SteelBlue;
            this.Buttonpause.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Buttonpause.Image = global::UserInterface.Properties.Resources.pause_button_png_29651;
            this.Buttonpause.Location = new System.Drawing.Point(78, 2);
            this.Buttonpause.Name = "Buttonpause";
            this.Buttonpause.Size = new System.Drawing.Size(59, 60);
            this.Buttonpause.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Buttonpause.TabIndex = 14;
            this.Buttonpause.TabStop = false;
            this.Buttonpause.Click += new System.EventHandler(this.Buttonpause_Click);
            this.Buttonpause.MouseHover += new System.EventHandler(this.Buttonpause_MouseHover);
            // 
            // Buttonstop
            // 
            this.Buttonstop.BackColor = System.Drawing.Color.SteelBlue;
            this.Buttonstop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Buttonstop.Image = global::UserInterface.Properties.Resources.start_icon230;
            this.Buttonstop.Location = new System.Drawing.Point(143, 2);
            this.Buttonstop.Name = "Buttonstop";
            this.Buttonstop.Size = new System.Drawing.Size(63, 61);
            this.Buttonstop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Buttonstop.TabIndex = 13;
            this.Buttonstop.TabStop = false;
            this.Buttonstop.Click += new System.EventHandler(this.Buttonstop_Click);
            this.Buttonstop.MouseHover += new System.EventHandler(this.Buttonstop_MouseHover);
            // 
            // ButtonStart
            // 
            this.ButtonStart.BackColor = System.Drawing.Color.SteelBlue;
            this.ButtonStart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ButtonStart.Image = global::UserInterface.Properties.Resources.startbutton;
            this.ButtonStart.Location = new System.Drawing.Point(13, 2);
            this.ButtonStart.Name = "ButtonStart";
            this.ButtonStart.Size = new System.Drawing.Size(59, 60);
            this.ButtonStart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ButtonStart.TabIndex = 12;
            this.ButtonStart.TabStop = false;
            this.ButtonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            this.ButtonStart.MouseHover += new System.EventHandler(this.ButtonStart_MouseHover);
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(62, -3);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(137, 26);
            this.bunifuCustomLabel3.TabIndex = 9;
            this.bunifuCustomLabel3.Text = "Remaining Time";
            // 
            // labeldot1
            // 
            this.labeldot1.AutoSize = true;
            this.labeldot1.BackColor = System.Drawing.Color.Transparent;
            this.labeldot1.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeldot1.ForeColor = System.Drawing.Color.SteelBlue;
            this.labeldot1.Location = new System.Drawing.Point(133, 18);
            this.labeldot1.Name = "labeldot1";
            this.labeldot1.Size = new System.Drawing.Size(42, 26);
            this.labeldot1.TabIndex = 8;
            this.labeldot1.Text = "min";
            // 
            // labeldot
            // 
            this.labeldot.AutoSize = true;
            this.labeldot.BackColor = System.Drawing.Color.Transparent;
            this.labeldot.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeldot.ForeColor = System.Drawing.Color.SteelBlue;
            this.labeldot.Location = new System.Drawing.Point(87, 20);
            this.labeldot.Name = "labeldot";
            this.labeldot.Size = new System.Drawing.Size(29, 26);
            this.labeldot.TabIndex = 7;
            this.labeldot.Text = "hr";
            // 
            // lbltimemin
            // 
            this.lbltimemin.AutoSize = true;
            this.lbltimemin.BackColor = System.Drawing.Color.Transparent;
            this.lbltimemin.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltimemin.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbltimemin.Location = new System.Drawing.Point(110, 20);
            this.lbltimemin.Name = "lbltimemin";
            this.lbltimemin.Size = new System.Drawing.Size(30, 26);
            this.lbltimemin.TabIndex = 6;
            this.lbltimemin.Text = "00";
            // 
            // lbltimehr
            // 
            this.lbltimehr.AutoSize = true;
            this.lbltimehr.BackColor = System.Drawing.Color.Transparent;
            this.lbltimehr.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltimehr.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbltimehr.Location = new System.Drawing.Point(63, 20);
            this.lbltimehr.Name = "lbltimehr";
            this.lbltimehr.Size = new System.Drawing.Size(30, 26);
            this.lbltimehr.TabIndex = 5;
            this.lbltimehr.Text = "00";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbltime.Location = new System.Drawing.Point(167, 20);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(30, 26);
            this.lbltime.TabIndex = 3;
            this.lbltime.Text = "00";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(348, 28);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(114, 26);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "Subject Name";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(303, 0);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(235, 26);
            this.bunifuCustomLabel1.TabIndex = 0;
            this.bunifuCustomLabel1.Text = "Computer Based Examination";
            // 
            // Exampanelcontainer
            // 
            this.Exampanelcontainer.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Exampanelcontainer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Exampanelcontainer.BackgroundImage")));
            this.Exampanelcontainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Exampanelcontainer.Controls.Add(this.panelbottom);
            this.Exampanelcontainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Exampanelcontainer.GradientBottomLeft = System.Drawing.Color.WhiteSmoke;
            this.Exampanelcontainer.GradientBottomRight = System.Drawing.Color.WhiteSmoke;
            this.Exampanelcontainer.GradientTopLeft = System.Drawing.Color.White;
            this.Exampanelcontainer.GradientTopRight = System.Drawing.Color.White;
            this.Exampanelcontainer.Location = new System.Drawing.Point(0, 63);
            this.Exampanelcontainer.Name = "Exampanelcontainer";
            this.Exampanelcontainer.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.Exampanelcontainer.Quality = 10;
            this.Exampanelcontainer.Size = new System.Drawing.Size(1037, 557);
            this.Exampanelcontainer.TabIndex = 7;
            // 
            // panelbottom
            // 
            this.panelbottom.BackColor = System.Drawing.Color.Transparent;
            this.panelbottom.Controls.Add(this.panelNext);
            this.panelbottom.Controls.Add(this.panelPrev);
            this.panelbottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelbottom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelbottom.Location = new System.Drawing.Point(0, 508);
            this.panelbottom.Name = "panelbottom";
            this.panelbottom.Size = new System.Drawing.Size(1037, 49);
            this.panelbottom.TabIndex = 8;
            // 
            // panelNext
            // 
            this.panelNext.BackColor = System.Drawing.Color.AliceBlue;
            this.panelNext.Controls.Add(this.buttonnext);
            this.panelNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelNext.Location = new System.Drawing.Point(916, 0);
            this.panelNext.Name = "panelNext";
            this.panelNext.Size = new System.Drawing.Size(121, 49);
            this.panelNext.TabIndex = 1;
            // 
            // buttonnext
            // 
            this.buttonnext.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonnext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonnext.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonnext.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonnext.Image = global::UserInterface.Properties.Resources.Next_50px;
            this.buttonnext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonnext.Location = new System.Drawing.Point(0, 0);
            this.buttonnext.Name = "buttonnext";
            this.buttonnext.Size = new System.Drawing.Size(121, 49);
            this.buttonnext.TabIndex = 21;
            this.buttonnext.Text = "Next";
            this.buttonnext.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonnext.UseVisualStyleBackColor = false;
            this.buttonnext.Click += new System.EventHandler(this.buttonnext_Click);
            // 
            // panelPrev
            // 
            this.panelPrev.BackColor = System.Drawing.Color.AliceBlue;
            this.panelPrev.Controls.Add(this.buttonprev);
            this.panelPrev.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelPrev.Location = new System.Drawing.Point(0, 0);
            this.panelPrev.Name = "panelPrev";
            this.panelPrev.Size = new System.Drawing.Size(120, 49);
            this.panelPrev.TabIndex = 0;
            // 
            // buttonprev
            // 
            this.buttonprev.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonprev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonprev.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonprev.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonprev.Image = global::UserInterface.Properties.Resources.Previous_50px;
            this.buttonprev.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonprev.Location = new System.Drawing.Point(0, 0);
            this.buttonprev.Name = "buttonprev";
            this.buttonprev.Size = new System.Drawing.Size(120, 49);
            this.buttonprev.TabIndex = 20;
            this.buttonprev.Text = "Prev";
            this.buttonprev.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonprev.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonprev.UseVisualStyleBackColor = false;
            this.buttonprev.Click += new System.EventHandler(this.buttonprev_Click);
            // 
            // paneltimer
            // 
            this.paneltimer.BackColor = System.Drawing.Color.Transparent;
            this.paneltimer.Controls.Add(this.pictureBox1);
            this.paneltimer.Controls.Add(this.lbltime);
            this.paneltimer.Controls.Add(this.lbltimehr);
            this.paneltimer.Controls.Add(this.bunifuCustomLabel4);
            this.paneltimer.Controls.Add(this.lbltimemin);
            this.paneltimer.Controls.Add(this.labeldot);
            this.paneltimer.Controls.Add(this.labeldot1);
            this.paneltimer.Controls.Add(this.bunifuCustomLabel3);
            this.paneltimer.Dock = System.Windows.Forms.DockStyle.Right;
            this.paneltimer.Location = new System.Drawing.Point(613, 0);
            this.paneltimer.Name = "paneltimer";
            this.paneltimer.Size = new System.Drawing.Size(265, 59);
            this.paneltimer.TabIndex = 24;
            // 
            // examination
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Exampanelcontainer);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Name = "examination";
            this.Size = new System.Drawing.Size(1037, 620);
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.admissionpanelheader.ResumeLayout(false);
            this.admissionpanelheader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buttonpause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buttonstop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonStart)).EndInit();
            this.Exampanelcontainer.ResumeLayout(false);
            this.panelbottom.ResumeLayout(false);
            this.panelNext.ResumeLayout(false);
            this.panelPrev.ResumeLayout(false);
            this.paneltimer.ResumeLayout(false);
            this.paneltimer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public Bunifu.Framework.UI.BunifuGradientPanel admissionpanelheader;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuCustomLabel lbltime;
        public System.Windows.Forms.Timer examtime;
        private Bunifu.Framework.UI.BunifuCustomLabel labeldot1;
        private Bunifu.Framework.UI.BunifuCustomLabel labeldot;
        private Bunifu.Framework.UI.BunifuCustomLabel lbltimemin;
        private Bunifu.Framework.UI.BunifuCustomLabel lbltimehr;
        private System.Windows.Forms.PictureBox Buttonpause;
        private System.Windows.Forms.PictureBox Buttonstop;
        private System.Windows.Forms.PictureBox ButtonStart;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuGradientPanel Exampanelcontainer;
        private System.Windows.Forms.Panel panelbottom;
        private System.Windows.Forms.Panel panelNext;
        private System.Windows.Forms.Button buttonnext;
        private System.Windows.Forms.Panel panelPrev;
        private System.Windows.Forms.Button buttonprev;
        private System.Windows.Forms.Button buttonsubmit;
        private System.Windows.Forms.Panel paneltimer;
    }
}
