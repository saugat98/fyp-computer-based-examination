﻿namespace UserInterface
{
    partial class signin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(signin));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btnclear = new System.Windows.Forms.Button();
            this.btnlogin = new System.Windows.Forms.Button();
            this.textusername = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.btncalcel = new Bunifu.Framework.UI.BunifuFlatButton();
            this.textuserpassword = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuGradientPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.btnclear);
            this.bunifuGradientPanel2.Controls.Add(this.btnlogin);
            this.bunifuGradientPanel2.Controls.Add(this.textusername);
            this.bunifuGradientPanel2.Controls.Add(this.btncalcel);
            this.bunifuGradientPanel2.Controls.Add(this.textuserpassword);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox1);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuCustomLabel1);
            this.bunifuGradientPanel2.Controls.Add(this.linkLabel1);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuSeparator1);
            this.bunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.SteelBlue;
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.SteelBlue;
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.SteelBlue;
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.SteelBlue;
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(399, 306);
            this.bunifuGradientPanel2.TabIndex = 0;
            // 
            // btnclear
            // 
            this.btnclear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnclear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclear.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnclear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnclear.Location = new System.Drawing.Point(225, 200);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(122, 55);
            this.btnclear.TabIndex = 5;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = false;
            // 
            // btnlogin
            // 
            this.btnlogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnlogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnlogin.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnlogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnlogin.Location = new System.Drawing.Point(34, 200);
            this.btnlogin.Name = "btnlogin";
            this.btnlogin.Size = new System.Drawing.Size(122, 55);
            this.btnlogin.TabIndex = 4;
            this.btnlogin.Text = "Login";
            this.btnlogin.UseVisualStyleBackColor = false;
            this.btnlogin.Click += new System.EventHandler(this.btnlogin_Click);
            // 
            // textusername
            // 
            this.textusername.BackColor = System.Drawing.SystemColors.Menu;
            this.textusername.BorderColor = System.Drawing.Color.SeaGreen;
            this.textusername.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textusername.ForeColor = System.Drawing.Color.Silver;
            this.textusername.Location = new System.Drawing.Point(10, 90);
            this.textusername.MaxLength = 40;
            this.textusername.Multiline = true;
            this.textusername.Name = "textusername";
            this.textusername.Size = new System.Drawing.Size(377, 47);
            this.textusername.TabIndex = 2;
            this.textusername.Text = "Enter Username Here";
            this.textusername.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textusername_MouseClick);
            this.textusername.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textusername_KeyPress);
            this.textusername.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textusername_KeyUp);
            this.textusername.MouseLeave += new System.EventHandler(this.textusername_MouseLeave);
            // 
            // btncalcel
            // 
            this.btncalcel.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btncalcel.BackColor = System.Drawing.Color.Transparent;
            this.btncalcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btncalcel.BorderRadius = 0;
            this.btncalcel.ButtonText = "";
            this.btncalcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btncalcel.DisabledColor = System.Drawing.Color.Gray;
            this.btncalcel.Iconcolor = System.Drawing.Color.Transparent;
            this.btncalcel.Iconimage = global::UserInterface.Properties.Resources.Cancel_48;
            this.btncalcel.Iconimage_right = null;
            this.btncalcel.Iconimage_right_Selected = null;
            this.btncalcel.Iconimage_Selected = null;
            this.btncalcel.IconMarginLeft = 0;
            this.btncalcel.IconMarginRight = 0;
            this.btncalcel.IconRightVisible = true;
            this.btncalcel.IconRightZoom = 0D;
            this.btncalcel.IconVisible = true;
            this.btncalcel.IconZoom = 90D;
            this.btncalcel.IsTab = false;
            this.btncalcel.Location = new System.Drawing.Point(355, 0);
            this.btncalcel.Name = "btncalcel";
            this.btncalcel.Normalcolor = System.Drawing.Color.Transparent;
            this.btncalcel.OnHovercolor = System.Drawing.Color.Transparent;
            this.btncalcel.OnHoverTextColor = System.Drawing.Color.White;
            this.btncalcel.selected = false;
            this.btncalcel.Size = new System.Drawing.Size(76, 47);
            this.btncalcel.TabIndex = 6;
            this.btncalcel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btncalcel.Textcolor = System.Drawing.Color.White;
            this.btncalcel.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncalcel.Click += new System.EventHandler(this.btncalcel_Click);
            // 
            // textuserpassword
            // 
            this.textuserpassword.BackColor = System.Drawing.SystemColors.Menu;
            this.textuserpassword.BorderColor = System.Drawing.Color.SeaGreen;
            this.textuserpassword.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textuserpassword.ForeColor = System.Drawing.Color.Silver;
            this.textuserpassword.Location = new System.Drawing.Point(10, 147);
            this.textuserpassword.MaxLength = 40;
            this.textuserpassword.Multiline = true;
            this.textuserpassword.Name = "textuserpassword";
            this.textuserpassword.Size = new System.Drawing.Size(377, 47);
            this.textuserpassword.TabIndex = 3;
            this.textuserpassword.Text = "Enter Password Here";
            this.textuserpassword.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textuserpassword_MouseClick);
            this.textuserpassword.TextChanged += new System.EventHandler(this.textuserpassword_TextChanged);
            this.textuserpassword.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textuserpassword_KeyUp);
            this.textuserpassword.MouseLeave += new System.EventHandler(this.textuserpassword_MouseLeave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::UserInterface.Properties.Resources._1370146_orig;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.InitialImage = global::UserInterface.Properties.Resources.signupIcon;
            this.pictureBox1.Location = new System.Drawing.Point(65, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(76, 72);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Minion Pro", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(159, 19);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(102, 44);
            this.bunifuCustomLabel1.TabIndex = 7;
            this.bunifuCustomLabel1.Text = "LogIn";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkColor = System.Drawing.Color.Silver;
            this.linkLabel1.Location = new System.Drawing.Point(69, 267);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(245, 26);
            this.linkLabel1.TabIndex = 5;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Have you forgetten password?";
            this.linkLabel1.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(12, 53);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(554, 35);
            this.bunifuSeparator1.TabIndex = 1;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // signin
            // 
            this.AcceptButton = this.btnlogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(399, 306);
            this.Controls.Add(this.bunifuGradientPanel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "signin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.signin_Load);
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuFlatButton btncalcel;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textuserpassword;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textusername;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btnlogin;
    }
}