﻿namespace UserInterface
{
    partial class addusers
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxpersonneldetail = new System.Windows.Forms.GroupBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel60 = new System.Windows.Forms.Panel();
            this.textdescription = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel59 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelfilelocation = new System.Windows.Forms.Label();
            this.buttonbrowse = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.textparents = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel47 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.radioButtonfemale = new System.Windows.Forms.RadioButton();
            this.radioButtonmale = new System.Windows.Forms.RadioButton();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.textwebsite = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel45 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.textaddress = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.dateTimePickerdob = new System.Windows.Forms.DateTimePicker();
            this.panel40 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textemail = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.textmobile = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.textfullname = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBoxeducation = new System.Windows.Forms.GroupBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.comboboxuserrole = new System.Windows.Forms.ComboBox();
            this.panel36 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.textsecurity = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel43 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.panel61 = new System.Windows.Forms.Panel();
            this.comboboxsecurity = new System.Windows.Forms.ComboBox();
            this.panel62 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel63 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.texthint = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel64 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel65 = new System.Windows.Forms.Panel();
            this.panel66 = new System.Windows.Forms.Panel();
            this.textconfirm = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel67 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel68 = new System.Windows.Forms.Panel();
            this.textpassword = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel69 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel70 = new System.Windows.Forms.Panel();
            this.panel71 = new System.Windows.Forms.Panel();
            this.textuserid = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel72 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel73 = new System.Windows.Forms.Panel();
            this.comboboxcourse = new System.Windows.Forms.ComboBox();
            this.panel74 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.panel75 = new System.Windows.Forms.Panel();
            this.panel76 = new System.Windows.Forms.Panel();
            this.comboboxlabel = new System.Windows.Forms.ComboBox();
            this.panel77 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel78 = new System.Windows.Forms.Panel();
            this.comboboxfaculty = new System.Windows.Forms.ComboBox();
            this.panel79 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel44 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.textdesc = new System.Windows.Forms.RichTextBox();
            this.panel50 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.comboboxexperience = new System.Windows.Forms.ComboBox();
            this.textinstitute = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.panel54 = new System.Windows.Forms.Panel();
            this.textspecification = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel55 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.panel56 = new System.Windows.Forms.Panel();
            this.textqualification = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.panel57 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBoxpersonneldetail.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel60.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBoxeducation.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel61.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel68.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panel70.SuspendLayout();
            this.panel71.SuspendLayout();
            this.panel72.SuspendLayout();
            this.panel73.SuspendLayout();
            this.panel74.SuspendLayout();
            this.panel75.SuspendLayout();
            this.panel76.SuspendLayout();
            this.panel77.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel79.SuspendLayout();
            this.panel7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel57.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBoxpersonneldetail);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1127, 201);
            this.panel1.TabIndex = 0;
            // 
            // groupBoxpersonneldetail
            // 
            this.groupBoxpersonneldetail.BackColor = System.Drawing.Color.Snow;
            this.groupBoxpersonneldetail.Controls.Add(this.panel22);
            this.groupBoxpersonneldetail.Controls.Add(this.panel19);
            this.groupBoxpersonneldetail.Controls.Add(this.panel16);
            this.groupBoxpersonneldetail.Controls.Add(this.panel13);
            this.groupBoxpersonneldetail.Controls.Add(this.panel2);
            this.groupBoxpersonneldetail.Controls.Add(this.panel5);
            this.groupBoxpersonneldetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxpersonneldetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxpersonneldetail.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBoxpersonneldetail.Location = new System.Drawing.Point(0, 0);
            this.groupBoxpersonneldetail.Name = "groupBoxpersonneldetail";
            this.groupBoxpersonneldetail.Size = new System.Drawing.Size(1127, 201);
            this.groupBoxpersonneldetail.TabIndex = 0;
            this.groupBoxpersonneldetail.TabStop = false;
            this.groupBoxpersonneldetail.Text = "Personnel Details";
            // 
            // panel22
            // 
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(3, 196);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(1121, 10);
            this.panel22.TabIndex = 10;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel60);
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(3, 162);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1121, 34);
            this.panel19.TabIndex = 9;
            // 
            // panel60
            // 
            this.panel60.Controls.Add(this.textdescription);
            this.panel60.Controls.Add(this.panel59);
            this.panel60.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel60.Location = new System.Drawing.Point(495, 0);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(515, 34);
            this.panel60.TabIndex = 3;
            // 
            // textdescription
            // 
            this.textdescription.BackColor = System.Drawing.Color.White;
            this.textdescription.BorderColor = System.Drawing.Color.SeaGreen;
            this.textdescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textdescription.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textdescription.ForeColor = System.Drawing.Color.Silver;
            this.textdescription.Location = new System.Drawing.Point(169, 0);
            this.textdescription.MaxLength = 40;
            this.textdescription.Multiline = true;
            this.textdescription.Name = "textdescription";
            this.textdescription.Size = new System.Drawing.Size(346, 34);
            this.textdescription.TabIndex = 11;
            this.textdescription.Text = "Describe About You";
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.label10);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel59.Location = new System.Drawing.Point(0, 0);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(169, 34);
            this.panel59.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label10.Name = "label10";
            this.label10.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label10.Size = new System.Drawing.Size(126, 28);
            this.label10.TabIndex = 2;
            this.label10.Text = "Descriptions";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.labelfilelocation);
            this.panel20.Controls.Add(this.buttonbrowse);
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(495, 34);
            this.panel20.TabIndex = 2;
            // 
            // labelfilelocation
            // 
            this.labelfilelocation.AutoSize = true;
            this.labelfilelocation.BackColor = System.Drawing.Color.Transparent;
            this.labelfilelocation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelfilelocation.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelfilelocation.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelfilelocation.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.labelfilelocation.Location = new System.Drawing.Point(251, 0);
            this.labelfilelocation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.labelfilelocation.Name = "labelfilelocation";
            this.labelfilelocation.Padding = new System.Windows.Forms.Padding(10, 5, 10, 10);
            this.labelfilelocation.Size = new System.Drawing.Size(130, 43);
            this.labelfilelocation.TabIndex = 18;
            this.labelfilelocation.Text = "File Location";
            // 
            // buttonbrowse
            // 
            this.buttonbrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonbrowse.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonbrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonbrowse.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonbrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonbrowse.Location = new System.Drawing.Point(161, 0);
            this.buttonbrowse.Name = "buttonbrowse";
            this.buttonbrowse.Size = new System.Drawing.Size(90, 34);
            this.buttonbrowse.TabIndex = 17;
            this.buttonbrowse.Text = "Browser";
            this.buttonbrowse.UseVisualStyleBackColor = false;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.label5);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(161, 34);
            this.panel21.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label5.Size = new System.Drawing.Size(79, 28);
            this.label5.TabIndex = 2;
            this.label5.Text = "Image";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.panel58);
            this.panel16.Controls.Add(this.panel17);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(3, 128);
            this.panel16.Name = "panel16";
            this.panel16.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.panel16.Size = new System.Drawing.Size(1121, 34);
            this.panel16.TabIndex = 8;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.textparents);
            this.panel58.Controls.Add(this.panel47);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel58.Location = new System.Drawing.Point(495, 0);
            this.panel58.Name = "panel58";
            this.panel58.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.panel58.Size = new System.Drawing.Size(515, 32);
            this.panel58.TabIndex = 3;
            // 
            // textparents
            // 
            this.textparents.BackColor = System.Drawing.Color.White;
            this.textparents.BorderColor = System.Drawing.Color.SeaGreen;
            this.textparents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textparents.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textparents.ForeColor = System.Drawing.Color.Silver;
            this.textparents.Location = new System.Drawing.Point(169, 2);
            this.textparents.MaxLength = 40;
            this.textparents.Multiline = true;
            this.textparents.Name = "textparents";
            this.textparents.Size = new System.Drawing.Size(346, 30);
            this.textparents.TabIndex = 11;
            this.textparents.Text = "Enter Your Parents Name";
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.label9);
            this.panel47.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel47.Location = new System.Drawing.Point(0, 2);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(169, 30);
            this.panel47.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label9.Size = new System.Drawing.Size(136, 28);
            this.label9.TabIndex = 2;
            this.label9.Text = "Parents Name";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.radioButtonfemale);
            this.panel17.Controls.Add(this.radioButtonmale);
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(495, 32);
            this.panel17.TabIndex = 2;
            // 
            // radioButtonfemale
            // 
            this.radioButtonfemale.AutoSize = true;
            this.radioButtonfemale.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonfemale.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonfemale.ForeColor = System.Drawing.Color.Gray;
            this.radioButtonfemale.Location = new System.Drawing.Point(255, 0);
            this.radioButtonfemale.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.radioButtonfemale.Name = "radioButtonfemale";
            this.radioButtonfemale.Padding = new System.Windows.Forms.Padding(30, 2, 2, 10);
            this.radioButtonfemale.Size = new System.Drawing.Size(113, 32);
            this.radioButtonfemale.TabIndex = 3;
            this.radioButtonfemale.TabStop = true;
            this.radioButtonfemale.Text = "Female";
            this.radioButtonfemale.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButtonfemale.UseVisualStyleBackColor = true;
            // 
            // radioButtonmale
            // 
            this.radioButtonmale.AutoSize = true;
            this.radioButtonmale.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonmale.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonmale.ForeColor = System.Drawing.Color.Gray;
            this.radioButtonmale.Location = new System.Drawing.Point(161, 0);
            this.radioButtonmale.Name = "radioButtonmale";
            this.radioButtonmale.Padding = new System.Windows.Forms.Padding(30, 2, 2, 10);
            this.radioButtonmale.Size = new System.Drawing.Size(94, 32);
            this.radioButtonmale.TabIndex = 2;
            this.radioButtonmale.TabStop = true;
            this.radioButtonmale.Text = "Male";
            this.radioButtonmale.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButtonmale.UseVisualStyleBackColor = true;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.label4);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(161, 32);
            this.panel18.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label4.Size = new System.Drawing.Size(87, 28);
            this.label4.TabIndex = 2;
            this.label4.Text = "Gender";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.panel46);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(3, 91);
            this.panel13.Name = "panel13";
            this.panel13.Padding = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.panel13.Size = new System.Drawing.Size(1121, 37);
            this.panel13.TabIndex = 7;
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.textwebsite);
            this.panel46.Controls.Add(this.panel45);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel46.Location = new System.Drawing.Point(495, 4);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(515, 31);
            this.panel46.TabIndex = 3;
            // 
            // textwebsite
            // 
            this.textwebsite.BackColor = System.Drawing.Color.White;
            this.textwebsite.BorderColor = System.Drawing.Color.SeaGreen;
            this.textwebsite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textwebsite.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textwebsite.ForeColor = System.Drawing.Color.Silver;
            this.textwebsite.Location = new System.Drawing.Point(169, 0);
            this.textwebsite.MaxLength = 40;
            this.textwebsite.Multiline = true;
            this.textwebsite.Name = "textwebsite";
            this.textwebsite.Size = new System.Drawing.Size(346, 31);
            this.textwebsite.TabIndex = 11;
            this.textwebsite.Text = "Enter Your website";
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.label8);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel45.Location = new System.Drawing.Point(0, 0);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(169, 31);
            this.panel45.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label8.Size = new System.Drawing.Size(91, 28);
            this.label8.TabIndex = 2;
            this.label8.Text = "Website";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.textaddress);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(495, 31);
            this.panel14.TabIndex = 2;
            // 
            // textaddress
            // 
            this.textaddress.BackColor = System.Drawing.Color.White;
            this.textaddress.BorderColor = System.Drawing.Color.SeaGreen;
            this.textaddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textaddress.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textaddress.ForeColor = System.Drawing.Color.Silver;
            this.textaddress.Location = new System.Drawing.Point(161, 0);
            this.textaddress.MaxLength = 40;
            this.textaddress.Multiline = true;
            this.textaddress.Name = "textaddress";
            this.textaddress.Size = new System.Drawing.Size(334, 31);
            this.textaddress.TabIndex = 11;
            this.textaddress.Text = "Enter Address";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label3);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(161, 31);
            this.panel15.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label3.Size = new System.Drawing.Size(93, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "Address";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel41);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 52);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.panel2.Size = new System.Drawing.Size(1121, 39);
            this.panel2.TabIndex = 6;
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.dateTimePickerdob);
            this.panel41.Controls.Add(this.panel40);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel41.Location = new System.Drawing.Point(495, 4);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(515, 33);
            this.panel41.TabIndex = 3;
            // 
            // dateTimePickerdob
            // 
            this.dateTimePickerdob.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerdob.CalendarForeColor = System.Drawing.SystemColors.ControlDark;
            this.dateTimePickerdob.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dateTimePickerdob.CustomFormat = "dd-MMM-yyyy";
            this.dateTimePickerdob.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePickerdob.Font = new System.Drawing.Font("Minion Pro", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerdob.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerdob.Location = new System.Drawing.Point(169, 0);
            this.dateTimePickerdob.Name = "dateTimePickerdob";
            this.dateTimePickerdob.Size = new System.Drawing.Size(346, 30);
            this.dateTimePickerdob.TabIndex = 2;
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.label7);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel40.Location = new System.Drawing.Point(0, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(169, 33);
            this.panel40.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label7.Size = new System.Drawing.Size(81, 28);
            this.label7.TabIndex = 2;
            this.label7.Text = "D.O.B.";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textemail);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(495, 33);
            this.panel3.TabIndex = 2;
            // 
            // textemail
            // 
            this.textemail.BackColor = System.Drawing.Color.White;
            this.textemail.BorderColor = System.Drawing.Color.SeaGreen;
            this.textemail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textemail.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textemail.ForeColor = System.Drawing.Color.Silver;
            this.textemail.Location = new System.Drawing.Point(161, 0);
            this.textemail.MaxLength = 40;
            this.textemail.Multiline = true;
            this.textemail.Name = "textemail";
            this.textemail.Size = new System.Drawing.Size(334, 33);
            this.textemail.TabIndex = 11;
            this.textemail.Text = "Enter Your Email";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(161, 33);
            this.panel4.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label2.Size = new System.Drawing.Size(81, 28);
            this.label2.TabIndex = 2;
            this.label2.Text = "E-mail";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel39);
            this.panel5.Controls.Add(this.panel23);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 20);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1121, 32);
            this.panel5.TabIndex = 5;
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.textmobile);
            this.panel39.Controls.Add(this.panel38);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel39.Location = new System.Drawing.Point(495, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(515, 32);
            this.panel39.TabIndex = 3;
            // 
            // textmobile
            // 
            this.textmobile.BackColor = System.Drawing.Color.White;
            this.textmobile.BorderColor = System.Drawing.Color.SeaGreen;
            this.textmobile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textmobile.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textmobile.ForeColor = System.Drawing.Color.Silver;
            this.textmobile.Location = new System.Drawing.Point(170, 0);
            this.textmobile.MaxLength = 40;
            this.textmobile.Multiline = true;
            this.textmobile.Name = "textmobile";
            this.textmobile.Size = new System.Drawing.Size(345, 32);
            this.textmobile.TabIndex = 11;
            this.textmobile.Text = "Enter Mobile Number";
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.label6);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel38.Location = new System.Drawing.Point(0, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(170, 32);
            this.panel38.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label6.Size = new System.Drawing.Size(97, 28);
            this.label6.TabIndex = 2;
            this.label6.Text = "Mbl. No.";
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.textfullname);
            this.panel23.Controls.Add(this.panel42);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(495, 32);
            this.panel23.TabIndex = 2;
            // 
            // textfullname
            // 
            this.textfullname.BackColor = System.Drawing.Color.White;
            this.textfullname.BorderColor = System.Drawing.Color.SeaGreen;
            this.textfullname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textfullname.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textfullname.ForeColor = System.Drawing.Color.Silver;
            this.textfullname.Location = new System.Drawing.Point(161, 0);
            this.textfullname.MaxLength = 40;
            this.textfullname.Multiline = true;
            this.textfullname.Name = "textfullname";
            this.textfullname.Size = new System.Drawing.Size(334, 32);
            this.textfullname.TabIndex = 10;
            this.textfullname.Text = "Enter Full Name";
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.label1);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel42.Location = new System.Drawing.Point(0, 0);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(161, 32);
            this.panel42.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label1.Size = new System.Drawing.Size(109, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Full Name";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.groupBoxeducation);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 201);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1127, 191);
            this.panel6.TabIndex = 5;
            // 
            // groupBoxeducation
            // 
            this.groupBoxeducation.BackColor = System.Drawing.Color.Snow;
            this.groupBoxeducation.Controls.Add(this.panel24);
            this.groupBoxeducation.Controls.Add(this.panel48);
            this.groupBoxeducation.Controls.Add(this.panel65);
            this.groupBoxeducation.Controls.Add(this.panel70);
            this.groupBoxeducation.Controls.Add(this.panel75);
            this.groupBoxeducation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxeducation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxeducation.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBoxeducation.Location = new System.Drawing.Point(0, 0);
            this.groupBoxeducation.Name = "groupBoxeducation";
            this.groupBoxeducation.Size = new System.Drawing.Size(1127, 191);
            this.groupBoxeducation.TabIndex = 1;
            this.groupBoxeducation.TabStop = false;
            this.groupBoxeducation.Text = "Education && User Details";
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.panel25);
            this.panel24.Controls.Add(this.panel37);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(3, 155);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(1121, 32);
            this.panel24.TabIndex = 9;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.comboboxuserrole);
            this.panel25.Controls.Add(this.panel36);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(495, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(515, 32);
            this.panel25.TabIndex = 3;
            // 
            // comboboxuserrole
            // 
            this.comboboxuserrole.AutoCompleteCustomSource.AddRange(new string[] {
            "Choose Course"});
            this.comboboxuserrole.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboboxuserrole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboboxuserrole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboboxuserrole.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboboxuserrole.Font = new System.Drawing.Font("Minion Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboboxuserrole.ForeColor = System.Drawing.Color.LightGray;
            this.comboboxuserrole.FormattingEnabled = true;
            this.comboboxuserrole.Items.AddRange(new object[] {
            "Choose User Role",
            "Student",
            "Staff",
            "Controller",
            "Lecturer"});
            this.comboboxuserrole.Location = new System.Drawing.Point(171, 0);
            this.comboboxuserrole.Name = "comboboxuserrole";
            this.comboboxuserrole.Size = new System.Drawing.Size(344, 30);
            this.comboboxuserrole.TabIndex = 4;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.label11);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel36.Location = new System.Drawing.Point(0, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(171, 32);
            this.panel36.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Dock = System.Windows.Forms.DockStyle.Left;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label11.Location = new System.Drawing.Point(0, 0);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label11.Size = new System.Drawing.Size(103, 28);
            this.label11.TabIndex = 2;
            this.label11.Text = "User Role";
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.textsecurity);
            this.panel37.Controls.Add(this.panel43);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(495, 32);
            this.panel37.TabIndex = 2;
            // 
            // textsecurity
            // 
            this.textsecurity.BackColor = System.Drawing.Color.White;
            this.textsecurity.BorderColor = System.Drawing.Color.SeaGreen;
            this.textsecurity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textsecurity.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textsecurity.ForeColor = System.Drawing.Color.Silver;
            this.textsecurity.Location = new System.Drawing.Point(161, 0);
            this.textsecurity.MaxLength = 40;
            this.textsecurity.Multiline = true;
            this.textsecurity.Name = "textsecurity";
            this.textsecurity.Size = new System.Drawing.Size(334, 32);
            this.textsecurity.TabIndex = 11;
            this.textsecurity.Text = "Answers For Security";
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.label12);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel43.Location = new System.Drawing.Point(0, 0);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(161, 32);
            this.panel43.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label12.Size = new System.Drawing.Size(152, 28);
            this.label12.TabIndex = 2;
            this.label12.Text = "Security Answer";
            // 
            // panel48
            // 
            this.panel48.Controls.Add(this.panel61);
            this.panel48.Controls.Add(this.panel63);
            this.panel48.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel48.Location = new System.Drawing.Point(3, 122);
            this.panel48.Name = "panel48";
            this.panel48.Padding = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.panel48.Size = new System.Drawing.Size(1121, 33);
            this.panel48.TabIndex = 8;
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.comboboxsecurity);
            this.panel61.Controls.Add(this.panel62);
            this.panel61.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel61.Location = new System.Drawing.Point(495, 4);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(515, 27);
            this.panel61.TabIndex = 3;
            // 
            // comboboxsecurity
            // 
            this.comboboxsecurity.AutoCompleteCustomSource.AddRange(new string[] {
            "Choose Course"});
            this.comboboxsecurity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboboxsecurity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboboxsecurity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboboxsecurity.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboboxsecurity.Font = new System.Drawing.Font("Minion Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboboxsecurity.ForeColor = System.Drawing.Color.LightGray;
            this.comboboxsecurity.FormattingEnabled = true;
            this.comboboxsecurity.Items.AddRange(new object[] {
            "Choose Security Questions"});
            this.comboboxsecurity.Location = new System.Drawing.Point(172, 0);
            this.comboboxsecurity.Name = "comboboxsecurity";
            this.comboboxsecurity.Size = new System.Drawing.Size(343, 30);
            this.comboboxsecurity.TabIndex = 4;
            // 
            // panel62
            // 
            this.panel62.Controls.Add(this.label14);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel62.Location = new System.Drawing.Point(0, 0);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(172, 27);
            this.panel62.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label14.Name = "label14";
            this.label14.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label14.Size = new System.Drawing.Size(170, 28);
            this.label14.TabIndex = 3;
            this.label14.Text = "Security Questions";
            // 
            // panel63
            // 
            this.panel63.Controls.Add(this.panel9);
            this.panel63.Controls.Add(this.panel64);
            this.panel63.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel63.Location = new System.Drawing.Point(0, 4);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(495, 27);
            this.panel63.TabIndex = 2;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.texthint);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(161, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(334, 27);
            this.panel9.TabIndex = 4;
            // 
            // texthint
            // 
            this.texthint.BackColor = System.Drawing.Color.White;
            this.texthint.BorderColor = System.Drawing.Color.SeaGreen;
            this.texthint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.texthint.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.texthint.ForeColor = System.Drawing.Color.Silver;
            this.texthint.Location = new System.Drawing.Point(0, 0);
            this.texthint.MaxLength = 40;
            this.texthint.Multiline = true;
            this.texthint.Name = "texthint";
            this.texthint.Size = new System.Drawing.Size(334, 27);
            this.texthint.TabIndex = 11;
            this.texthint.Text = "Enter Hints Password";
            // 
            // panel64
            // 
            this.panel64.Controls.Add(this.label13);
            this.panel64.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel64.Location = new System.Drawing.Point(0, 0);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(161, 27);
            this.panel64.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label13.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label13.Name = "label13";
            this.label13.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label13.Size = new System.Drawing.Size(141, 28);
            this.label13.TabIndex = 3;
            this.label13.Text = "Hint Password";
            // 
            // panel65
            // 
            this.panel65.Controls.Add(this.panel66);
            this.panel65.Controls.Add(this.panel68);
            this.panel65.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel65.Location = new System.Drawing.Point(3, 87);
            this.panel65.Name = "panel65";
            this.panel65.Padding = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.panel65.Size = new System.Drawing.Size(1121, 35);
            this.panel65.TabIndex = 7;
            // 
            // panel66
            // 
            this.panel66.Controls.Add(this.textconfirm);
            this.panel66.Controls.Add(this.panel67);
            this.panel66.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel66.Location = new System.Drawing.Point(495, 4);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(515, 29);
            this.panel66.TabIndex = 3;
            // 
            // textconfirm
            // 
            this.textconfirm.BackColor = System.Drawing.Color.White;
            this.textconfirm.BorderColor = System.Drawing.Color.SeaGreen;
            this.textconfirm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textconfirm.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textconfirm.ForeColor = System.Drawing.Color.Silver;
            this.textconfirm.Location = new System.Drawing.Point(172, 0);
            this.textconfirm.MaxLength = 40;
            this.textconfirm.Multiline = true;
            this.textconfirm.Name = "textconfirm";
            this.textconfirm.Size = new System.Drawing.Size(343, 29);
            this.textconfirm.TabIndex = 11;
            this.textconfirm.Text = "Confirm Your Password";
            // 
            // panel67
            // 
            this.panel67.Controls.Add(this.label15);
            this.panel67.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel67.Location = new System.Drawing.Point(0, 0);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(172, 29);
            this.panel67.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Dock = System.Windows.Forms.DockStyle.Left;
            this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label15.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label15.Size = new System.Drawing.Size(169, 28);
            this.label15.TabIndex = 2;
            this.label15.Text = "Confirm Password";
            // 
            // panel68
            // 
            this.panel68.Controls.Add(this.textpassword);
            this.panel68.Controls.Add(this.panel69);
            this.panel68.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel68.Location = new System.Drawing.Point(0, 4);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(495, 29);
            this.panel68.TabIndex = 2;
            // 
            // textpassword
            // 
            this.textpassword.BackColor = System.Drawing.Color.White;
            this.textpassword.BorderColor = System.Drawing.Color.SeaGreen;
            this.textpassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textpassword.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textpassword.ForeColor = System.Drawing.Color.Silver;
            this.textpassword.Location = new System.Drawing.Point(161, 0);
            this.textpassword.MaxLength = 40;
            this.textpassword.Multiline = true;
            this.textpassword.Name = "textpassword";
            this.textpassword.Size = new System.Drawing.Size(334, 29);
            this.textpassword.TabIndex = 11;
            this.textpassword.Text = "Enter Password";
            // 
            // panel69
            // 
            this.panel69.Controls.Add(this.label16);
            this.panel69.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel69.Location = new System.Drawing.Point(0, 0);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(161, 29);
            this.panel69.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Dock = System.Windows.Forms.DockStyle.Left;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label16.Name = "label16";
            this.label16.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label16.Size = new System.Drawing.Size(103, 28);
            this.label16.TabIndex = 2;
            this.label16.Text = "Password";
            // 
            // panel70
            // 
            this.panel70.Controls.Add(this.panel71);
            this.panel70.Controls.Add(this.panel73);
            this.panel70.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel70.Location = new System.Drawing.Point(3, 50);
            this.panel70.Name = "panel70";
            this.panel70.Padding = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.panel70.Size = new System.Drawing.Size(1121, 37);
            this.panel70.TabIndex = 6;
            // 
            // panel71
            // 
            this.panel71.Controls.Add(this.textuserid);
            this.panel71.Controls.Add(this.panel72);
            this.panel71.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel71.Location = new System.Drawing.Point(495, 4);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(515, 31);
            this.panel71.TabIndex = 3;
            // 
            // textuserid
            // 
            this.textuserid.BackColor = System.Drawing.Color.White;
            this.textuserid.BorderColor = System.Drawing.Color.SeaGreen;
            this.textuserid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textuserid.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textuserid.ForeColor = System.Drawing.Color.Silver;
            this.textuserid.Location = new System.Drawing.Point(172, 0);
            this.textuserid.MaxLength = 40;
            this.textuserid.Multiline = true;
            this.textuserid.Name = "textuserid";
            this.textuserid.Size = new System.Drawing.Size(343, 31);
            this.textuserid.TabIndex = 11;
            this.textuserid.Text = "Enter User ID";
            // 
            // panel72
            // 
            this.panel72.Controls.Add(this.label17);
            this.panel72.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel72.Location = new System.Drawing.Point(0, 0);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(172, 31);
            this.panel72.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Dock = System.Windows.Forms.DockStyle.Left;
            this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label17.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label17.Name = "label17";
            this.label17.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label17.Size = new System.Drawing.Size(86, 28);
            this.label17.TabIndex = 2;
            this.label17.Text = "User Id";
            // 
            // panel73
            // 
            this.panel73.Controls.Add(this.comboboxcourse);
            this.panel73.Controls.Add(this.panel74);
            this.panel73.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel73.Location = new System.Drawing.Point(0, 4);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(495, 31);
            this.panel73.TabIndex = 2;
            // 
            // comboboxcourse
            // 
            this.comboboxcourse.AutoCompleteCustomSource.AddRange(new string[] {
            "Choose Course"});
            this.comboboxcourse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboboxcourse.DisplayMember = "COURSE_ID";
            this.comboboxcourse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboboxcourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboboxcourse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboboxcourse.Font = new System.Drawing.Font("Minion Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboboxcourse.ForeColor = System.Drawing.Color.LightGray;
            this.comboboxcourse.FormattingEnabled = true;
            this.comboboxcourse.Items.AddRange(new object[] {
            "Choose Course"});
            this.comboboxcourse.Location = new System.Drawing.Point(161, 0);
            this.comboboxcourse.Name = "comboboxcourse";
            this.comboboxcourse.Size = new System.Drawing.Size(334, 30);
            this.comboboxcourse.TabIndex = 5;
            this.comboboxcourse.ValueMember = "COURSE_ID";
            // 
            // panel74
            // 
            this.panel74.Controls.Add(this.label20);
            this.panel74.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel74.Location = new System.Drawing.Point(0, 0);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(161, 31);
            this.panel74.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Dock = System.Windows.Forms.DockStyle.Left;
            this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label20.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label20.Name = "label20";
            this.label20.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label20.Size = new System.Drawing.Size(84, 28);
            this.label20.TabIndex = 3;
            this.label20.Text = "Course";
            // 
            // panel75
            // 
            this.panel75.Controls.Add(this.panel76);
            this.panel75.Controls.Add(this.panel78);
            this.panel75.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel75.Location = new System.Drawing.Point(3, 20);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(1121, 30);
            this.panel75.TabIndex = 5;
            // 
            // panel76
            // 
            this.panel76.Controls.Add(this.comboboxlabel);
            this.panel76.Controls.Add(this.panel77);
            this.panel76.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel76.Location = new System.Drawing.Point(495, 0);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(515, 30);
            this.panel76.TabIndex = 3;
            // 
            // comboboxlabel
            // 
            this.comboboxlabel.AutoCompleteCustomSource.AddRange(new string[] {
            "Choose Course"});
            this.comboboxlabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboboxlabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboboxlabel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboboxlabel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboboxlabel.Font = new System.Drawing.Font("Minion Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboboxlabel.ForeColor = System.Drawing.Color.LightGray;
            this.comboboxlabel.FormattingEnabled = true;
            this.comboboxlabel.Items.AddRange(new object[] {
            "Choose Education Level"});
            this.comboboxlabel.Location = new System.Drawing.Point(172, 0);
            this.comboboxlabel.Name = "comboboxlabel";
            this.comboboxlabel.Size = new System.Drawing.Size(343, 30);
            this.comboboxlabel.TabIndex = 3;
            // 
            // panel77
            // 
            this.panel77.Controls.Add(this.label19);
            this.panel77.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel77.Location = new System.Drawing.Point(0, 0);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(172, 30);
            this.panel77.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Dock = System.Windows.Forms.DockStyle.Left;
            this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label19.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label19.Name = "label19";
            this.label19.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label19.Size = new System.Drawing.Size(71, 28);
            this.label19.TabIndex = 2;
            this.label19.Text = "Lavel";
            // 
            // panel78
            // 
            this.panel78.Controls.Add(this.comboboxfaculty);
            this.panel78.Controls.Add(this.panel79);
            this.panel78.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel78.Location = new System.Drawing.Point(0, 0);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(495, 30);
            this.panel78.TabIndex = 2;
            // 
            // comboboxfaculty
            // 
            this.comboboxfaculty.AutoCompleteCustomSource.AddRange(new string[] {
            "Choose Course"});
            this.comboboxfaculty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboboxfaculty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboboxfaculty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboboxfaculty.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboboxfaculty.Font = new System.Drawing.Font("Minion Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboboxfaculty.ForeColor = System.Drawing.Color.LightGray;
            this.comboboxfaculty.FormattingEnabled = true;
            this.comboboxfaculty.Items.AddRange(new object[] {
            "Choose Faculty"});
            this.comboboxfaculty.Location = new System.Drawing.Point(161, 0);
            this.comboboxfaculty.Name = "comboboxfaculty";
            this.comboboxfaculty.Size = new System.Drawing.Size(334, 30);
            this.comboboxfaculty.TabIndex = 4;
            // 
            // panel79
            // 
            this.panel79.Controls.Add(this.label18);
            this.panel79.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel79.Location = new System.Drawing.Point(0, 0);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(161, 30);
            this.panel79.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Dock = System.Windows.Forms.DockStyle.Left;
            this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label18.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label18.Name = "label18";
            this.label18.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label18.Size = new System.Drawing.Size(86, 28);
            this.label18.TabIndex = 3;
            this.label18.Text = "Faculty";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.groupBox1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 392);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1127, 338);
            this.panel7.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Snow;
            this.groupBox1.Controls.Add(this.panel26);
            this.groupBox1.Controls.Add(this.panel44);
            this.groupBox1.Controls.Add(this.panel53);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1127, 338);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Experience Details";
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.button1);
            this.panel26.Controls.Add(this.button2);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(3, 116);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(1121, 75);
            this.panel26.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(667, -1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 51);
            this.button1.TabIndex = 16;
            this.button1.Text = "Submit";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(893, -1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 51);
            this.button2.TabIndex = 17;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.panel49);
            this.panel44.Controls.Add(this.panel51);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel44.Location = new System.Drawing.Point(3, 54);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(1121, 62);
            this.panel44.TabIndex = 6;
            // 
            // panel49
            // 
            this.panel49.Controls.Add(this.textdesc);
            this.panel49.Controls.Add(this.panel50);
            this.panel49.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel49.Location = new System.Drawing.Point(495, 0);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(515, 62);
            this.panel49.TabIndex = 3;
            // 
            // textdesc
            // 
            this.textdesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textdesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textdesc.ForeColor = System.Drawing.Color.Silver;
            this.textdesc.Location = new System.Drawing.Point(172, 1);
            this.textdesc.Name = "textdesc";
            this.textdesc.Size = new System.Drawing.Size(343, 55);
            this.textdesc.TabIndex = 5;
            this.textdesc.Text = "Describe Your Experience in Details";
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.label27);
            this.panel50.Location = new System.Drawing.Point(0, 20);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(170, 32);
            this.panel50.TabIndex = 1;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Dock = System.Windows.Forms.DockStyle.Left;
            this.label27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label27.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label27.Name = "label27";
            this.label27.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label27.Size = new System.Drawing.Size(119, 28);
            this.label27.TabIndex = 2;
            this.label27.Text = "Description";
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.comboboxexperience);
            this.panel51.Controls.Add(this.textinstitute);
            this.panel51.Controls.Add(this.panel8);
            this.panel51.Controls.Add(this.panel52);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel51.Location = new System.Drawing.Point(0, 0);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(495, 62);
            this.panel51.TabIndex = 2;
            // 
            // comboboxexperience
            // 
            this.comboboxexperience.AutoCompleteCustomSource.AddRange(new string[] {
            "Choose Course"});
            this.comboboxexperience.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboboxexperience.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboboxexperience.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboboxexperience.Font = new System.Drawing.Font("Minion Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboboxexperience.ForeColor = System.Drawing.Color.LightGray;
            this.comboboxexperience.FormattingEnabled = true;
            this.comboboxexperience.Items.AddRange(new object[] {
            "Choose Experience Duration(Years)",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22 ",
            "23 ",
            "24 ",
            "25"});
            this.comboboxexperience.Location = new System.Drawing.Point(161, 2);
            this.comboboxexperience.Name = "comboboxexperience";
            this.comboboxexperience.Size = new System.Drawing.Size(329, 30);
            this.comboboxexperience.TabIndex = 14;
            // 
            // textinstitute
            // 
            this.textinstitute.BackColor = System.Drawing.Color.White;
            this.textinstitute.BorderColor = System.Drawing.Color.SeaGreen;
            this.textinstitute.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textinstitute.ForeColor = System.Drawing.Color.Silver;
            this.textinstitute.Location = new System.Drawing.Point(161, 34);
            this.textinstitute.MaxLength = 40;
            this.textinstitute.Multiline = true;
            this.textinstitute.Name = "textinstitute";
            this.textinstitute.Size = new System.Drawing.Size(329, 28);
            this.textinstitute.TabIndex = 13;
            this.textinstitute.Text = "Enter Past Institute Name";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label21);
            this.panel8.Location = new System.Drawing.Point(3, 38);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(158, 49);
            this.panel8.TabIndex = 2;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Dock = System.Windows.Forms.DockStyle.Left;
            this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label21.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label21.Name = "label21";
            this.label21.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label21.Size = new System.Drawing.Size(144, 28);
            this.label21.TabIndex = 2;
            this.label21.Text = "Institute Name";
            // 
            // panel52
            // 
            this.panel52.Controls.Add(this.label28);
            this.panel52.Location = new System.Drawing.Point(0, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(161, 33);
            this.panel52.TabIndex = 1;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label28.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label28.Location = new System.Drawing.Point(0, 0);
            this.label28.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label28.Name = "label28";
            this.label28.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label28.Size = new System.Drawing.Size(114, 28);
            this.label28.TabIndex = 2;
            this.label28.Text = "Experience";
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.panel54);
            this.panel53.Controls.Add(this.panel56);
            this.panel53.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel53.Location = new System.Drawing.Point(3, 20);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(1121, 34);
            this.panel53.TabIndex = 5;
            // 
            // panel54
            // 
            this.panel54.Controls.Add(this.textspecification);
            this.panel54.Controls.Add(this.panel55);
            this.panel54.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel54.Location = new System.Drawing.Point(495, 0);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(515, 34);
            this.panel54.TabIndex = 3;
            // 
            // textspecification
            // 
            this.textspecification.BackColor = System.Drawing.Color.White;
            this.textspecification.BorderColor = System.Drawing.Color.SeaGreen;
            this.textspecification.Dock = System.Windows.Forms.DockStyle.Left;
            this.textspecification.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textspecification.ForeColor = System.Drawing.Color.Silver;
            this.textspecification.Location = new System.Drawing.Point(172, 0);
            this.textspecification.MaxLength = 40;
            this.textspecification.Multiline = true;
            this.textspecification.Name = "textspecification";
            this.textspecification.Size = new System.Drawing.Size(343, 34);
            this.textspecification.TabIndex = 11;
            this.textspecification.Text = "Enter Your Proficiency";
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.label29);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel55.Location = new System.Drawing.Point(0, 0);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(172, 34);
            this.panel55.TabIndex = 1;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Dock = System.Windows.Forms.DockStyle.Left;
            this.label29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label29.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label29.Location = new System.Drawing.Point(0, 0);
            this.label29.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label29.Name = "label29";
            this.label29.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label29.Size = new System.Drawing.Size(126, 28);
            this.label29.TabIndex = 2;
            this.label29.Text = "Specification";
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.textqualification);
            this.panel56.Controls.Add(this.panel57);
            this.panel56.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel56.Location = new System.Drawing.Point(0, 0);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(495, 34);
            this.panel56.TabIndex = 2;
            // 
            // textqualification
            // 
            this.textqualification.BackColor = System.Drawing.Color.White;
            this.textqualification.BorderColor = System.Drawing.Color.SeaGreen;
            this.textqualification.Dock = System.Windows.Forms.DockStyle.Left;
            this.textqualification.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textqualification.ForeColor = System.Drawing.Color.Silver;
            this.textqualification.Location = new System.Drawing.Point(161, 0);
            this.textqualification.MaxLength = 40;
            this.textqualification.Multiline = true;
            this.textqualification.Name = "textqualification";
            this.textqualification.Size = new System.Drawing.Size(334, 34);
            this.textqualification.TabIndex = 11;
            this.textqualification.Text = "Enter Your Basic Skills";
            // 
            // panel57
            // 
            this.panel57.Controls.Add(this.label30);
            this.panel57.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel57.Location = new System.Drawing.Point(0, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(161, 34);
            this.panel57.TabIndex = 1;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Dock = System.Windows.Forms.DockStyle.Left;
            this.label30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label30.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.label30.Name = "label30";
            this.label30.Padding = new System.Windows.Forms.Padding(10, 2, 10, 0);
            this.label30.Size = new System.Drawing.Size(127, 28);
            this.label30.TabIndex = 1;
            this.label30.Text = "Qualification";
            // 
            // addusers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Name = "addusers";
            this.Size = new System.Drawing.Size(1127, 730);
            this.panel1.ResumeLayout(false);
            this.groupBoxpersonneldetail.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel60.ResumeLayout(false);
            this.panel60.PerformLayout();
            this.panel59.ResumeLayout(false);
            this.panel59.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel58.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel46.PerformLayout();
            this.panel45.ResumeLayout(false);
            this.panel45.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.groupBoxeducation.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.panel43.ResumeLayout(false);
            this.panel43.PerformLayout();
            this.panel48.ResumeLayout(false);
            this.panel61.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel62.PerformLayout();
            this.panel63.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel64.ResumeLayout(false);
            this.panel64.PerformLayout();
            this.panel65.ResumeLayout(false);
            this.panel66.ResumeLayout(false);
            this.panel66.PerformLayout();
            this.panel67.ResumeLayout(false);
            this.panel67.PerformLayout();
            this.panel68.ResumeLayout(false);
            this.panel68.PerformLayout();
            this.panel69.ResumeLayout(false);
            this.panel69.PerformLayout();
            this.panel70.ResumeLayout(false);
            this.panel71.ResumeLayout(false);
            this.panel71.PerformLayout();
            this.panel72.ResumeLayout(false);
            this.panel72.PerformLayout();
            this.panel73.ResumeLayout(false);
            this.panel74.ResumeLayout(false);
            this.panel74.PerformLayout();
            this.panel75.ResumeLayout(false);
            this.panel76.ResumeLayout(false);
            this.panel77.ResumeLayout(false);
            this.panel77.PerformLayout();
            this.panel78.ResumeLayout(false);
            this.panel79.ResumeLayout(false);
            this.panel79.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            this.panel49.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel50.PerformLayout();
            this.panel51.ResumeLayout(false);
            this.panel51.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel52.ResumeLayout(false);
            this.panel52.PerformLayout();
            this.panel53.ResumeLayout(false);
            this.panel54.ResumeLayout(false);
            this.panel54.PerformLayout();
            this.panel55.ResumeLayout(false);
            this.panel55.PerformLayout();
            this.panel56.ResumeLayout(false);
            this.panel56.PerformLayout();
            this.panel57.ResumeLayout(false);
            this.panel57.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBoxpersonneldetail;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxeducation;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label21;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textfullname;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textmobile;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textemail;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textdescription;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textparents;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textwebsite;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textaddress;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textsecurity;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textconfirm;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textpassword;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textuserid;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textspecification;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textqualification;
        private System.Windows.Forms.RichTextBox textdesc;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textinstitute;
        private System.Windows.Forms.ComboBox comboboxuserrole;
        private System.Windows.Forms.ComboBox comboboxsecurity;
        private System.Windows.Forms.ComboBox comboboxlabel;
        private System.Windows.Forms.ComboBox comboboxexperience;
        private System.Windows.Forms.Button buttonbrowse;
        private System.Windows.Forms.Label labelfilelocation;
        private System.Windows.Forms.RadioButton radioButtonfemale;
        private System.Windows.Forms.RadioButton radioButtonmale;
        private System.Windows.Forms.DateTimePicker dateTimePickerdob;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Panel panel9;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox texthint;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboboxcourse;
        private System.Windows.Forms.ComboBox comboboxfaculty;
    }
}
