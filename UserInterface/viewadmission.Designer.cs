﻿namespace UserInterface
{
    partial class viewadmission
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(viewadmission));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.userdisplay = new System.Windows.Forms.DataGridView();
            this.colid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colsn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colfullname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colemail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colphone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collabel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colintake = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colimage = new System.Windows.Forms.DataGridViewImageColumn();
            this.colrole = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.textboxsearch = new Bunifu.Framework.UI.BunifuTextbox();
            this.bunifuDropdown2 = new Bunifu.Framework.UI.BunifuDropdown();
            this.bunifuDropdown3 = new Bunifu.Framework.UI.BunifuDropdown();
            this.bunifuDropdown1 = new Bunifu.Framework.UI.BunifuDropdown();
            this.label1 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel1.SuspendLayout();
            this.bunifuGradientPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userdisplay)).BeginInit();
            this.bunifuGradientPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.bunifuGradientPanel3);
            this.bunifuGradientPanel1.Controls.Add(this.bunifuGradientPanel2);
            this.bunifuGradientPanel1.Controls.Add(this.label1);
            this.bunifuGradientPanel1.Controls.Add(this.label15);
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(1157, 610);
            this.bunifuGradientPanel1.TabIndex = 0;
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel3.BackgroundImage")));
            this.bunifuGradientPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel3.Controls.Add(this.userdisplay);
            this.bunifuGradientPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(0, 58);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(1157, 552);
            this.bunifuGradientPanel3.TabIndex = 37;
            // 
            // userdisplay
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userdisplay.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.userdisplay.BackgroundColor = System.Drawing.Color.Teal;
            this.userdisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.userdisplay.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.userdisplay.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.GrayText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userdisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.userdisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userdisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colid,
            this.colsn,
            this.colfullname,
            this.colemail,
            this.colphone,
            this.collabel,
            this.colintake,
            this.colimage,
            this.colrole});
            this.userdisplay.Cursor = System.Windows.Forms.Cursors.Default;
            this.userdisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userdisplay.EnableHeadersVisualStyles = false;
            this.userdisplay.Location = new System.Drawing.Point(0, 0);
            this.userdisplay.Name = "userdisplay";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.GrayText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userdisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.userdisplay.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.userdisplay.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userdisplay.Size = new System.Drawing.Size(1157, 552);
            this.userdisplay.StandardTab = true;
            this.userdisplay.TabIndex = 0;
            this.userdisplay.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.userdisplay_RowHeaderMouseClick);
            // 
            // colid
            // 
            this.colid.HeaderText = "User Id";
            this.colid.Name = "colid";
            this.colid.Visible = false;
            this.colid.Width = 5;
            // 
            // colsn
            // 
            this.colsn.HeaderText = "S.N";
            this.colsn.Name = "colsn";
            this.colsn.Width = 60;
            // 
            // colfullname
            // 
            this.colfullname.HeaderText = "Full Name";
            this.colfullname.Name = "colfullname";
            this.colfullname.Width = 160;
            // 
            // colemail
            // 
            this.colemail.HeaderText = "E-mail";
            this.colemail.Name = "colemail";
            this.colemail.Width = 160;
            // 
            // colphone
            // 
            this.colphone.HeaderText = "Phone";
            this.colphone.Name = "colphone";
            this.colphone.Width = 180;
            // 
            // collabel
            // 
            this.collabel.HeaderText = "Label";
            this.collabel.Name = "collabel";
            // 
            // colintake
            // 
            this.colintake.HeaderText = "Intake";
            this.colintake.Name = "colintake";
            this.colintake.Width = 130;
            // 
            // colimage
            // 
            this.colimage.HeaderText = "Image";
            this.colimage.Name = "colimage";
            this.colimage.Width = 120;
            // 
            // colrole
            // 
            this.colrole.HeaderText = "Role";
            this.colrole.Name = "colrole";
            this.colrole.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colrole.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.textboxsearch);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuDropdown2);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuDropdown3);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuDropdown1);
            this.bunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Padding = new System.Windows.Forms.Padding(10);
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(1157, 58);
            this.bunifuGradientPanel2.TabIndex = 24;
            // 
            // textboxsearch
            // 
            this.textboxsearch.BackColor = System.Drawing.Color.LavenderBlush;
            this.textboxsearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("textboxsearch.BackgroundImage")));
            this.textboxsearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.textboxsearch.ForeColor = System.Drawing.Color.DodgerBlue;
            this.textboxsearch.Icon = ((System.Drawing.Image)(resources.GetObject("textboxsearch.Icon")));
            this.textboxsearch.Location = new System.Drawing.Point(773, 13);
            this.textboxsearch.Name = "textboxsearch";
            this.textboxsearch.Size = new System.Drawing.Size(245, 38);
            this.textboxsearch.TabIndex = 24;
            this.textboxsearch.text = "Search Data";
            // 
            // bunifuDropdown2
            // 
            this.bunifuDropdown2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuDropdown2.BorderRadius = 3;
            this.bunifuDropdown2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuDropdown2.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuDropdown2.ForeColor = System.Drawing.Color.White;
            this.bunifuDropdown2.Items = new string[] {
        "Sort By Intake",
        "January",
        "March",
        "June",
        "July",
        "September"};
            this.bunifuDropdown2.Location = new System.Drawing.Point(444, 10);
            this.bunifuDropdown2.Name = "bunifuDropdown2";
            this.bunifuDropdown2.NomalColor = System.Drawing.Color.Teal;
            this.bunifuDropdown2.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.bunifuDropdown2.selectedIndex = 0;
            this.bunifuDropdown2.Size = new System.Drawing.Size(217, 38);
            this.bunifuDropdown2.TabIndex = 22;
            // 
            // bunifuDropdown3
            // 
            this.bunifuDropdown3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuDropdown3.BorderRadius = 3;
            this.bunifuDropdown3.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuDropdown3.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuDropdown3.ForeColor = System.Drawing.Color.White;
            this.bunifuDropdown3.Items = new string[] {
        "Sort By Label",
        "A label",
        "Level 4",
        "Level 5",
        "Level 6"};
            this.bunifuDropdown3.Location = new System.Drawing.Point(227, 10);
            this.bunifuDropdown3.Name = "bunifuDropdown3";
            this.bunifuDropdown3.NomalColor = System.Drawing.Color.Teal;
            this.bunifuDropdown3.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.bunifuDropdown3.selectedIndex = 0;
            this.bunifuDropdown3.Size = new System.Drawing.Size(217, 38);
            this.bunifuDropdown3.TabIndex = 23;
            // 
            // bunifuDropdown1
            // 
            this.bunifuDropdown1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuDropdown1.BorderRadius = 3;
            this.bunifuDropdown1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuDropdown1.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuDropdown1.ForeColor = System.Drawing.Color.White;
            this.bunifuDropdown1.Items = new string[] {
        "Sort By",
        "Name",
        "Id",
        "Date",
        "Intake",
        "Label"};
            this.bunifuDropdown1.Location = new System.Drawing.Point(10, 10);
            this.bunifuDropdown1.Name = "bunifuDropdown1";
            this.bunifuDropdown1.NomalColor = System.Drawing.Color.Teal;
            this.bunifuDropdown1.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.bunifuDropdown1.selectedIndex = 0;
            this.bunifuDropdown1.Size = new System.Drawing.Size(217, 38);
            this.bunifuDropdown1.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(1199, 290);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 26);
            this.label1.TabIndex = 5;
            this.label1.Text = "First Name";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label15.Location = new System.Drawing.Point(-137, 290);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(125, 26);
            this.label15.TabIndex = 19;
            this.label15.Text = "Other Portfolio";
            // 
            // viewadmission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Name = "viewadmission";
            this.Size = new System.Drawing.Size(1157, 610);
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel1.PerformLayout();
            this.bunifuGradientPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userdisplay)).EndInit();
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuDropdown bunifuDropdown3;
        private Bunifu.Framework.UI.BunifuDropdown bunifuDropdown2;
        private Bunifu.Framework.UI.BunifuDropdown bunifuDropdown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
        public System.Windows.Forms.DataGridView userdisplay;
        private Bunifu.Framework.UI.BunifuTextbox textboxsearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colid;
        private System.Windows.Forms.DataGridViewTextBoxColumn colsn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colfullname;
        private System.Windows.Forms.DataGridViewTextBoxColumn colemail;
        private System.Windows.Forms.DataGridViewTextBoxColumn colphone;
        private System.Windows.Forms.DataGridViewTextBoxColumn collabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colintake;
        private System.Windows.Forms.DataGridViewImageColumn colimage;
        private System.Windows.Forms.DataGridViewTextBoxColumn colrole;
    }
}
