﻿namespace UserInterface
{
    partial class home
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(home));
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel5 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuImageButton4 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuCustomLabel21 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel16 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel15 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel19 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel20 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel17 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel18 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel14 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel13 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel12 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuGradientPanel6 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel7 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel8 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuImageButton3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.textboxusername = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.btnmessagesubmit = new Bunifu.Framework.UI.BunifuFlatButton();
            this.textboxmessage = new System.Windows.Forms.RichTextBox();
            this.bunifuCustomLabel24 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel23 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel22 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel4 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuCustomLabel11 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel10 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel9 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel8 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel7 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel6 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel5 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCircleProgressbar2 = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.bunifuCircleProgressbar1 = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuGradientPanel2.SuspendLayout();
            this.bunifuGradientPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton4)).BeginInit();
            this.bunifuGradientPanel6.SuspendLayout();
            this.bunifuGradientPanel7.SuspendLayout();
            this.bunifuGradientPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).BeginInit();
            this.bunifuGradientPanel1.SuspendLayout();
            this.bunifuGradientPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            this.bunifuGradientPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.bunifuGradientPanel5);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuGradientPanel6);
            this.bunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(0, 289);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(1055, 374);
            this.bunifuGradientPanel2.TabIndex = 1;
            // 
            // bunifuGradientPanel5
            // 
            this.bunifuGradientPanel5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel5.BackgroundImage")));
            this.bunifuGradientPanel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.bunifuGradientPanel5.Controls.Add(this.bunifuImageButton4);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel21);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel16);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel15);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel19);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel20);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel17);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel18);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel14);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel13);
            this.bunifuGradientPanel5.Controls.Add(this.bunifuCustomLabel12);
            this.bunifuGradientPanel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel5.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel5.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel5.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel5.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel5.Location = new System.Drawing.Point(560, 0);
            this.bunifuGradientPanel5.Name = "bunifuGradientPanel5";
            this.bunifuGradientPanel5.Quality = 10;
            this.bunifuGradientPanel5.Size = new System.Drawing.Size(561, 374);
            this.bunifuGradientPanel5.TabIndex = 3;
            // 
            // bunifuImageButton4
            // 
            this.bunifuImageButton4.BackColor = System.Drawing.Color.RoyalBlue;
            this.bunifuImageButton4.Image = global::UserInterface.Properties.Resources.Boarding_Pass_100px;
            this.bunifuImageButton4.ImageActive = null;
            this.bunifuImageButton4.Location = new System.Drawing.Point(166, 4);
            this.bunifuImageButton4.Name = "bunifuImageButton4";
            this.bunifuImageButton4.Size = new System.Drawing.Size(40, 40);
            this.bunifuImageButton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton4.TabIndex = 19;
            this.bunifuImageButton4.TabStop = false;
            this.bunifuImageButton4.Zoom = 0;
            // 
            // bunifuCustomLabel21
            // 
            this.bunifuCustomLabel21.AutoSize = true;
            this.bunifuCustomLabel21.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel21.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel21.ForeColor = System.Drawing.SystemColors.InfoText;
            this.bunifuCustomLabel21.Location = new System.Drawing.Point(22, 132);
            this.bunifuCustomLabel21.Name = "bunifuCustomLabel21";
            this.bunifuCustomLabel21.Size = new System.Drawing.Size(81, 21);
            this.bunifuCustomLabel21.TabIndex = 21;
            this.bunifuCustomLabel21.Text = "2 hours ago";
            // 
            // bunifuCustomLabel16
            // 
            this.bunifuCustomLabel16.AutoSize = true;
            this.bunifuCustomLabel16.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel16.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel16.ForeColor = System.Drawing.SystemColors.InfoText;
            this.bunifuCustomLabel16.Location = new System.Drawing.Point(22, 200);
            this.bunifuCustomLabel16.Name = "bunifuCustomLabel16";
            this.bunifuCustomLabel16.Size = new System.Drawing.Size(81, 21);
            this.bunifuCustomLabel16.TabIndex = 20;
            this.bunifuCustomLabel16.Text = "2 hours ago";
            // 
            // bunifuCustomLabel15
            // 
            this.bunifuCustomLabel15.AutoSize = true;
            this.bunifuCustomLabel15.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel15.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel15.ForeColor = System.Drawing.SystemColors.InfoText;
            this.bunifuCustomLabel15.Location = new System.Drawing.Point(22, 70);
            this.bunifuCustomLabel15.Name = "bunifuCustomLabel15";
            this.bunifuCustomLabel15.Size = new System.Drawing.Size(81, 21);
            this.bunifuCustomLabel15.TabIndex = 19;
            this.bunifuCustomLabel15.Text = "2 hours ago";
            // 
            // bunifuCustomLabel19
            // 
            this.bunifuCustomLabel19.AutoSize = true;
            this.bunifuCustomLabel19.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel19.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel19.Location = new System.Drawing.Point(21, 180);
            this.bunifuCustomLabel19.Name = "bunifuCustomLabel19";
            this.bunifuCustomLabel19.Size = new System.Drawing.Size(128, 26);
            this.bunifuCustomLabel19.TabIndex = 18;
            this.bunifuCustomLabel19.Text = "Students Exam";
            // 
            // bunifuCustomLabel20
            // 
            this.bunifuCustomLabel20.AutoSize = true;
            this.bunifuCustomLabel20.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel20.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.bunifuCustomLabel20.Location = new System.Drawing.Point(22, 221);
            this.bunifuCustomLabel20.Name = "bunifuCustomLabel20";
            this.bunifuCustomLabel20.Size = new System.Drawing.Size(457, 21);
            this.bunifuCustomLabel20.TabIndex = 17;
            this.bunifuCustomLabel20.Text = "this is notice for all students to inform about your final examination date ";
            // 
            // bunifuCustomLabel17
            // 
            this.bunifuCustomLabel17.AutoSize = true;
            this.bunifuCustomLabel17.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel17.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel17.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel17.Location = new System.Drawing.Point(21, 110);
            this.bunifuCustomLabel17.Name = "bunifuCustomLabel17";
            this.bunifuCustomLabel17.Size = new System.Drawing.Size(128, 26);
            this.bunifuCustomLabel17.TabIndex = 16;
            this.bunifuCustomLabel17.Text = "Students Exam";
            // 
            // bunifuCustomLabel18
            // 
            this.bunifuCustomLabel18.AutoSize = true;
            this.bunifuCustomLabel18.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel18.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.bunifuCustomLabel18.Location = new System.Drawing.Point(22, 158);
            this.bunifuCustomLabel18.Name = "bunifuCustomLabel18";
            this.bunifuCustomLabel18.Size = new System.Drawing.Size(457, 21);
            this.bunifuCustomLabel18.TabIndex = 15;
            this.bunifuCustomLabel18.Text = "this is notice for all students to inform about your final examination date ";
            // 
            // bunifuCustomLabel14
            // 
            this.bunifuCustomLabel14.AutoSize = true;
            this.bunifuCustomLabel14.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel14.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel14.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel14.Location = new System.Drawing.Point(21, 45);
            this.bunifuCustomLabel14.Name = "bunifuCustomLabel14";
            this.bunifuCustomLabel14.Size = new System.Drawing.Size(128, 26);
            this.bunifuCustomLabel14.TabIndex = 12;
            this.bunifuCustomLabel14.Text = "Students Exam";
            // 
            // bunifuCustomLabel13
            // 
            this.bunifuCustomLabel13.AutoSize = true;
            this.bunifuCustomLabel13.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel13.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel13.ForeColor = System.Drawing.SystemColors.GrayText;
            this.bunifuCustomLabel13.Location = new System.Drawing.Point(22, 91);
            this.bunifuCustomLabel13.Name = "bunifuCustomLabel13";
            this.bunifuCustomLabel13.Size = new System.Drawing.Size(457, 21);
            this.bunifuCustomLabel13.TabIndex = 10;
            this.bunifuCustomLabel13.Text = "this is notice for all students to inform about your final examination date ";
            // 
            // bunifuCustomLabel12
            // 
            this.bunifuCustomLabel12.AutoSize = true;
            this.bunifuCustomLabel12.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel12.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel12.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel12.Location = new System.Drawing.Point(215, 15);
            this.bunifuCustomLabel12.Name = "bunifuCustomLabel12";
            this.bunifuCustomLabel12.Size = new System.Drawing.Size(63, 26);
            this.bunifuCustomLabel12.TabIndex = 8;
            this.bunifuCustomLabel12.Text = "Notice";
            // 
            // bunifuGradientPanel6
            // 
            this.bunifuGradientPanel6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel6.BackgroundImage")));
            this.bunifuGradientPanel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel6.Controls.Add(this.bunifuGradientPanel7);
            this.bunifuGradientPanel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel6.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel6.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel6.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel6.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel6.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel6.Name = "bunifuGradientPanel6";
            this.bunifuGradientPanel6.Quality = 10;
            this.bunifuGradientPanel6.Size = new System.Drawing.Size(560, 374);
            this.bunifuGradientPanel6.TabIndex = 2;
            // 
            // bunifuGradientPanel7
            // 
            this.bunifuGradientPanel7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel7.BackgroundImage")));
            this.bunifuGradientPanel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel7.Controls.Add(this.bunifuGradientPanel8);
            this.bunifuGradientPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel7.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel7.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel7.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel7.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel7.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel7.Name = "bunifuGradientPanel7";
            this.bunifuGradientPanel7.Quality = 10;
            this.bunifuGradientPanel7.Size = new System.Drawing.Size(560, 374);
            this.bunifuGradientPanel7.TabIndex = 2;
            // 
            // bunifuGradientPanel8
            // 
            this.bunifuGradientPanel8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel8.BackgroundImage")));
            this.bunifuGradientPanel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.bunifuGradientPanel8.Controls.Add(this.bunifuImageButton3);
            this.bunifuGradientPanel8.Controls.Add(this.textboxusername);
            this.bunifuGradientPanel8.Controls.Add(this.btnmessagesubmit);
            this.bunifuGradientPanel8.Controls.Add(this.textboxmessage);
            this.bunifuGradientPanel8.Controls.Add(this.bunifuCustomLabel24);
            this.bunifuGradientPanel8.Controls.Add(this.bunifuCustomLabel23);
            this.bunifuGradientPanel8.Controls.Add(this.bunifuCustomLabel22);
            this.bunifuGradientPanel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel8.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel8.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel8.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel8.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel8.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel8.Name = "bunifuGradientPanel8";
            this.bunifuGradientPanel8.Quality = 10;
            this.bunifuGradientPanel8.Size = new System.Drawing.Size(561, 374);
            this.bunifuGradientPanel8.TabIndex = 2;
            // 
            // bunifuImageButton3
            // 
            this.bunifuImageButton3.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuImageButton3.Image = global::UserInterface.Properties.Resources.Message_100px;
            this.bunifuImageButton3.ImageActive = null;
            this.bunifuImageButton3.Location = new System.Drawing.Point(198, 2);
            this.bunifuImageButton3.Margin = new System.Windows.Forms.Padding(0);
            this.bunifuImageButton3.Name = "bunifuImageButton3";
            this.bunifuImageButton3.Size = new System.Drawing.Size(45, 39);
            this.bunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton3.TabIndex = 18;
            this.bunifuImageButton3.TabStop = false;
            this.bunifuImageButton3.Zoom = 0;
            // 
            // textboxusername
            // 
            this.textboxusername.BackColor = System.Drawing.SystemColors.Control;
            this.textboxusername.BorderColorFocused = System.Drawing.Color.Blue;
            this.textboxusername.BorderColorIdle = System.Drawing.Color.RoyalBlue;
            this.textboxusername.BorderColorMouseHover = System.Drawing.Color.Blue;
            this.textboxusername.BorderThickness = 3;
            this.textboxusername.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textboxusername.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.textboxusername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textboxusername.isPassword = false;
            this.textboxusername.Location = new System.Drawing.Point(196, 45);
            this.textboxusername.Margin = new System.Windows.Forms.Padding(4);
            this.textboxusername.Name = "textboxusername";
            this.textboxusername.Size = new System.Drawing.Size(276, 44);
            this.textboxusername.TabIndex = 17;
            this.textboxusername.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // btnmessagesubmit
            // 
            this.btnmessagesubmit.Activecolor = System.Drawing.Color.SteelBlue;
            this.btnmessagesubmit.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnmessagesubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnmessagesubmit.BorderRadius = 0;
            this.btnmessagesubmit.ButtonText = "send";
            this.btnmessagesubmit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnmessagesubmit.DisabledColor = System.Drawing.Color.Gray;
            this.btnmessagesubmit.Iconcolor = System.Drawing.Color.Transparent;
            this.btnmessagesubmit.Iconimage = global::UserInterface.Properties.Resources.Send_96px;
            this.btnmessagesubmit.Iconimage_right = null;
            this.btnmessagesubmit.Iconimage_right_Selected = null;
            this.btnmessagesubmit.Iconimage_Selected = null;
            this.btnmessagesubmit.IconMarginLeft = 0;
            this.btnmessagesubmit.IconMarginRight = 0;
            this.btnmessagesubmit.IconRightVisible = true;
            this.btnmessagesubmit.IconRightZoom = 0D;
            this.btnmessagesubmit.IconVisible = true;
            this.btnmessagesubmit.IconZoom = 90D;
            this.btnmessagesubmit.IsTab = false;
            this.btnmessagesubmit.Location = new System.Drawing.Point(198, 200);
            this.btnmessagesubmit.Name = "btnmessagesubmit";
            this.btnmessagesubmit.Normalcolor = System.Drawing.Color.RoyalBlue;
            this.btnmessagesubmit.OnHovercolor = System.Drawing.Color.Teal;
            this.btnmessagesubmit.OnHoverTextColor = System.Drawing.Color.White;
            this.btnmessagesubmit.selected = false;
            this.btnmessagesubmit.Size = new System.Drawing.Size(107, 42);
            this.btnmessagesubmit.TabIndex = 16;
            this.btnmessagesubmit.Text = "send";
            this.btnmessagesubmit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnmessagesubmit.Textcolor = System.Drawing.Color.White;
            this.btnmessagesubmit.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // textboxmessage
            // 
            this.textboxmessage.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.textboxmessage.Location = new System.Drawing.Point(196, 93);
            this.textboxmessage.Name = "textboxmessage";
            this.textboxmessage.Size = new System.Drawing.Size(276, 96);
            this.textboxmessage.TabIndex = 15;
            this.textboxmessage.Text = "";
            // 
            // bunifuCustomLabel24
            // 
            this.bunifuCustomLabel24.AutoSize = true;
            this.bunifuCustomLabel24.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel24.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel24.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel24.Location = new System.Drawing.Point(70, 54);
            this.bunifuCustomLabel24.Name = "bunifuCustomLabel24";
            this.bunifuCustomLabel24.Size = new System.Drawing.Size(91, 26);
            this.bunifuCustomLabel24.TabIndex = 12;
            this.bunifuCustomLabel24.Text = "Username";
            // 
            // bunifuCustomLabel23
            // 
            this.bunifuCustomLabel23.AutoSize = true;
            this.bunifuCustomLabel23.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel23.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel23.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel23.Location = new System.Drawing.Point(70, 110);
            this.bunifuCustomLabel23.Name = "bunifuCustomLabel23";
            this.bunifuCustomLabel23.Size = new System.Drawing.Size(77, 26);
            this.bunifuCustomLabel23.TabIndex = 11;
            this.bunifuCustomLabel23.Text = "Message";
            // 
            // bunifuCustomLabel22
            // 
            this.bunifuCustomLabel22.AutoSize = true;
            this.bunifuCustomLabel22.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel22.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel22.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel22.Location = new System.Drawing.Point(277, 15);
            this.bunifuCustomLabel22.Name = "bunifuCustomLabel22";
            this.bunifuCustomLabel22.Size = new System.Drawing.Size(101, 26);
            this.bunifuCustomLabel22.TabIndex = 9;
            this.bunifuCustomLabel22.Text = "Message Us";
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuGradientPanel1.Controls.Add(this.bunifuGradientPanel4);
            this.bunifuGradientPanel1.Controls.Add(this.bunifuGradientPanel3);
            this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(1055, 289);
            this.bunifuGradientPanel1.TabIndex = 0;
            // 
            // bunifuGradientPanel4
            // 
            this.bunifuGradientPanel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel4.BackgroundImage")));
            this.bunifuGradientPanel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.bunifuGradientPanel4.Controls.Add(this.bunifuImageButton2);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuCustomLabel11);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuCustomLabel10);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuCustomLabel9);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuCustomLabel8);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuCustomLabel7);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuCustomLabel6);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuCustomLabel5);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuImageButton1);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuCustomLabel4);
            this.bunifuGradientPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel4.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel4.Location = new System.Drawing.Point(560, 0);
            this.bunifuGradientPanel4.Name = "bunifuGradientPanel4";
            this.bunifuGradientPanel4.Quality = 10;
            this.bunifuGradientPanel4.Size = new System.Drawing.Size(506, 287);
            this.bunifuGradientPanel4.TabIndex = 1;
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.BackColor = System.Drawing.Color.RoyalBlue;
            this.bunifuImageButton2.Image = global::UserInterface.Properties.Resources.Accreditation_104px1;
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(129, 3);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(52, 37);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton2.TabIndex = 16;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.Zoom = 0;
            // 
            // bunifuCustomLabel11
            // 
            this.bunifuCustomLabel11.AutoSize = true;
            this.bunifuCustomLabel11.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel11.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel11.ForeColor = System.Drawing.SystemColors.GrayText;
            this.bunifuCustomLabel11.Location = new System.Drawing.Point(338, 154);
            this.bunifuCustomLabel11.Name = "bunifuCustomLabel11";
            this.bunifuCustomLabel11.Size = new System.Drawing.Size(35, 21);
            this.bunifuCustomLabel11.TabIndex = 15;
            this.bunifuCustomLabel11.Text = "99%";
            // 
            // bunifuCustomLabel10
            // 
            this.bunifuCustomLabel10.AutoSize = true;
            this.bunifuCustomLabel10.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel10.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.bunifuCustomLabel10.Location = new System.Drawing.Point(338, 194);
            this.bunifuCustomLabel10.Name = "bunifuCustomLabel10";
            this.bunifuCustomLabel10.Size = new System.Drawing.Size(28, 21);
            this.bunifuCustomLabel10.TabIndex = 14;
            this.bunifuCustomLabel10.Text = "A4";
            // 
            // bunifuCustomLabel9
            // 
            this.bunifuCustomLabel9.AutoSize = true;
            this.bunifuCustomLabel9.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel9.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.bunifuCustomLabel9.Location = new System.Drawing.Point(338, 228);
            this.bunifuCustomLabel9.Name = "bunifuCustomLabel9";
            this.bunifuCustomLabel9.Size = new System.Drawing.Size(51, 21);
            this.bunifuCustomLabel9.TabIndex = 13;
            this.bunifuCustomLabel9.Text = "Level 6";
            // 
            // bunifuCustomLabel8
            // 
            this.bunifuCustomLabel8.AutoSize = true;
            this.bunifuCustomLabel8.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel8.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel8.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel8.Location = new System.Drawing.Point(142, 225);
            this.bunifuCustomLabel8.Name = "bunifuCustomLabel8";
            this.bunifuCustomLabel8.Size = new System.Drawing.Size(52, 26);
            this.bunifuCustomLabel8.TabIndex = 12;
            this.bunifuCustomLabel8.Text = "Level";
            // 
            // bunifuCustomLabel7
            // 
            this.bunifuCustomLabel7.AutoSize = true;
            this.bunifuCustomLabel7.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel7.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel7.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel7.Location = new System.Drawing.Point(142, 189);
            this.bunifuCustomLabel7.Name = "bunifuCustomLabel7";
            this.bunifuCustomLabel7.Size = new System.Drawing.Size(69, 26);
            this.bunifuCustomLabel7.TabIndex = 11;
            this.bunifuCustomLabel7.Text = "Section";
            // 
            // bunifuCustomLabel6
            // 
            this.bunifuCustomLabel6.AutoSize = true;
            this.bunifuCustomLabel6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel6.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel6.Location = new System.Drawing.Point(142, 149);
            this.bunifuCustomLabel6.Name = "bunifuCustomLabel6";
            this.bunifuCustomLabel6.Size = new System.Drawing.Size(96, 26);
            this.bunifuCustomLabel6.TabIndex = 10;
            this.bunifuCustomLabel6.Text = "Percentage";
            // 
            // bunifuCustomLabel5
            // 
            this.bunifuCustomLabel5.AutoSize = true;
            this.bunifuCustomLabel5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel5.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.bunifuCustomLabel5.Location = new System.Drawing.Point(225, 123);
            this.bunifuCustomLabel5.Name = "bunifuCustomLabel5";
            this.bunifuCustomLabel5.Size = new System.Drawing.Size(80, 21);
            this.bunifuCustomLabel5.TabIndex = 9;
            this.bunifuCustomLabel5.Text = "abc student";
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Snow;
            this.bunifuImageButton1.Image = global::UserInterface.Properties.Resources.Admin_96px;
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(196, 29);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(135, 91);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 8;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(187, 0);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(157, 26);
            this.bunifuCustomLabel4.TabIndex = 7;
            this.bunifuCustomLabel4.Text = "Student of the year";
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel3.BackgroundImage")));
            this.bunifuGradientPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.bunifuGradientPanel3.Controls.Add(this.bunifuCustomLabel3);
            this.bunifuGradientPanel3.Controls.Add(this.bunifuCustomLabel1);
            this.bunifuGradientPanel3.Controls.Add(this.bunifuCircleProgressbar2);
            this.bunifuGradientPanel3.Controls.Add(this.bunifuCircleProgressbar1);
            this.bunifuGradientPanel3.Controls.Add(this.bunifuCustomLabel2);
            this.bunifuGradientPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(560, 287);
            this.bunifuGradientPanel3.TabIndex = 0;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(321, 252);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(171, 26);
            this.bunifuCustomLabel3.TabIndex = 6;
            this.bunifuCustomLabel3.Text = "Management 40/100";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(69, 252);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(124, 26);
            this.bunifuCustomLabel1.TabIndex = 5;
            this.bunifuCustomLabel1.Text = "Science 25/100";
            // 
            // bunifuCircleProgressbar2
            // 
            this.bunifuCircleProgressbar2.animated = false;
            this.bunifuCircleProgressbar2.animationIterval = 5;
            this.bunifuCircleProgressbar2.animationSpeed = 300;
            this.bunifuCircleProgressbar2.BackColor = System.Drawing.Color.White;
            this.bunifuCircleProgressbar2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuCircleProgressbar2.BackgroundImage")));
            this.bunifuCircleProgressbar2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.bunifuCircleProgressbar2.ForeColor = System.Drawing.Color.SeaGreen;
            this.bunifuCircleProgressbar2.LabelVisible = true;
            this.bunifuCircleProgressbar2.LineProgressThickness = 14;
            this.bunifuCircleProgressbar2.LineThickness = 15;
            this.bunifuCircleProgressbar2.Location = new System.Drawing.Point(290, 23);
            this.bunifuCircleProgressbar2.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.bunifuCircleProgressbar2.MaxValue = 100;
            this.bunifuCircleProgressbar2.Name = "bunifuCircleProgressbar2";
            this.bunifuCircleProgressbar2.ProgressBackColor = System.Drawing.Color.Gainsboro;
            this.bunifuCircleProgressbar2.ProgressColor = System.Drawing.Color.SeaGreen;
            this.bunifuCircleProgressbar2.Size = new System.Drawing.Size(228, 228);
            this.bunifuCircleProgressbar2.TabIndex = 4;
            this.bunifuCircleProgressbar2.Value = 20;
            // 
            // bunifuCircleProgressbar1
            // 
            this.bunifuCircleProgressbar1.animated = false;
            this.bunifuCircleProgressbar1.animationIterval = 5;
            this.bunifuCircleProgressbar1.animationSpeed = 300;
            this.bunifuCircleProgressbar1.BackColor = System.Drawing.Color.White;
            this.bunifuCircleProgressbar1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuCircleProgressbar1.BackgroundImage")));
            this.bunifuCircleProgressbar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.bunifuCircleProgressbar1.ForeColor = System.Drawing.Color.SeaGreen;
            this.bunifuCircleProgressbar1.LabelVisible = true;
            this.bunifuCircleProgressbar1.LineProgressThickness = 14;
            this.bunifuCircleProgressbar1.LineThickness = 15;
            this.bunifuCircleProgressbar1.Location = new System.Drawing.Point(24, 23);
            this.bunifuCircleProgressbar1.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.bunifuCircleProgressbar1.MaxValue = 100;
            this.bunifuCircleProgressbar1.Name = "bunifuCircleProgressbar1";
            this.bunifuCircleProgressbar1.ProgressBackColor = System.Drawing.Color.Gainsboro;
            this.bunifuCircleProgressbar1.ProgressColor = System.Drawing.Color.SeaGreen;
            this.bunifuCircleProgressbar1.Size = new System.Drawing.Size(228, 228);
            this.bunifuCircleProgressbar1.TabIndex = 3;
            this.bunifuCircleProgressbar1.Value = 10;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(0, 0);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(292, 26);
            this.bunifuCustomLabel2.TabIndex = 2;
            this.bunifuCustomLabel2.Text = "Students Passed Out Ratio(Per Year)";
            // 
            // home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.bunifuGradientPanel2);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Name = "home";
            this.Size = new System.Drawing.Size(1055, 663);
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel5.ResumeLayout(false);
            this.bunifuGradientPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton4)).EndInit();
            this.bunifuGradientPanel6.ResumeLayout(false);
            this.bunifuGradientPanel7.ResumeLayout(false);
            this.bunifuGradientPanel8.ResumeLayout(false);
            this.bunifuGradientPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).EndInit();
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel4.ResumeLayout(false);
            this.bunifuGradientPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            this.bunifuGradientPanel3.ResumeLayout(false);
            this.bunifuGradientPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel4;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel6;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel7;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuCircleProgressbar bunifuCircleProgressbar2;
        private Bunifu.Framework.UI.BunifuCircleProgressbar bunifuCircleProgressbar1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel5;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel8;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel7;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel6;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel11;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel10;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel9;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel8;
        private Bunifu.Framework.UI.BunifuFlatButton btnmessagesubmit;
        private System.Windows.Forms.RichTextBox textboxmessage;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel24;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel23;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel22;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel5;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel21;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel16;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel15;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel19;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel20;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel17;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel18;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel14;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel13;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel12;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton4;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton3;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        public Bunifu.Framework.UI.BunifuMetroTextbox textboxusername;
    }
}
