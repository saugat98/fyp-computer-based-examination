﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface
{
    public partial class admission : UserControl
    {
        public admission()
        {
            InitializeComponent();
            hideadmission();
            ar.Dock = DockStyle.Fill;
            admissionpanelcontainer.Controls.Add(ar);
            ar.Show();
            
            
        }
        addusers ar = new addusers();
        viewadmission vadd = new viewadmission();
        public void hideadmission()
        {
            ar.Hide();
            vadd.Hide();
        }

        private void admissionstudentadd_Click(object sender, EventArgs e)
        {
            hideadmission();
            ar.Dock = DockStyle.Fill;
            admissionpanelcontainer.Controls.Add(ar);
            ar.Show();
        }

        private void admissionstudentview_Click(object sender, EventArgs e)
        {
            hideadmission();
            vadd.Dock = DockStyle.Fill;
            admissionpanelcontainer.Controls.Add(vadd);
            vadd.Show();
        }

    }
}
