﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface
{
    public partial class viewadmission : UserControl
    {
        public viewadmission()
        {
            InitializeComponent();
        }

        private void userdisplay_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            viewusersdetail viewuser = new viewusersdetail();
            viewuser.ShowDialog();
        }
    }
}
