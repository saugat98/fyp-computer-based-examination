﻿namespace UserInterface
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.colortransition = new Bunifu.Framework.UI.BunifuColorTransition(this.components);
            this.panelcontainer = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuCustomLabel6 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.labelusername = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panelheaderone = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panelheader = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuImageButtonlogout = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel5 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.imagebuttonexit = new Bunifu.Framework.UI.BunifuImageButton();
            this.imagebuttonminimize = new Bunifu.Framework.UI.BunifuImageButton();
            this.panelsidebar = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btnhelp = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnreview = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btndocument = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnexamoffice = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btncollegehead = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnstaff = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnexam = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnadmission = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnhome = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.collegelogo = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuGradientPanel4 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.textsearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel1.SuspendLayout();
            this.panelheaderone.SuspendLayout();
            this.panelheader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButtonlogout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagebuttonexit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagebuttonminimize)).BeginInit();
            this.panelsidebar.SuspendLayout();
            this.bunifuGradientPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.collegelogo)).BeginInit();
            this.bunifuGradientPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // colortransition
            // 
            this.colortransition.Color1 = System.Drawing.Color.Gray;
            this.colortransition.Color2 = System.Drawing.Color.Maroon;
            this.colortransition.ProgessValue = 20;
            // 
            // panelcontainer
            // 
            this.panelcontainer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelcontainer.BackgroundImage")));
            this.panelcontainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelcontainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelcontainer.GradientBottomLeft = System.Drawing.Color.White;
            this.panelcontainer.GradientBottomRight = System.Drawing.Color.White;
            this.panelcontainer.GradientTopLeft = System.Drawing.Color.White;
            this.panelcontainer.GradientTopRight = System.Drawing.Color.White;
            this.panelcontainer.Location = new System.Drawing.Point(326, 106);
            this.panelcontainer.Name = "panelcontainer";
            this.panelcontainer.Quality = 10;
            this.panelcontainer.Size = new System.Drawing.Size(959, 438);
            this.panelcontainer.TabIndex = 0;
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.bunifuCustomLabel6);
            this.bunifuGradientPanel1.Controls.Add(this.labelusername);
            this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.CornflowerBlue;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.CornflowerBlue;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.SteelBlue;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.SteelBlue;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(326, 544);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(959, 40);
            this.bunifuGradientPanel1.TabIndex = 5;
            // 
            // bunifuCustomLabel6
            // 
            this.bunifuCustomLabel6.AutoSize = true;
            this.bunifuCustomLabel6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel6.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuCustomLabel6.Location = new System.Drawing.Point(10, 5);
            this.bunifuCustomLabel6.Name = "bunifuCustomLabel6";
            this.bunifuCustomLabel6.Size = new System.Drawing.Size(99, 26);
            this.bunifuCustomLabel6.TabIndex = 3;
            this.bunifuCustomLabel6.Text = "LogIn By: :";
            // 
            // labelusername
            // 
            this.labelusername.AutoSize = true;
            this.labelusername.BackColor = System.Drawing.Color.Transparent;
            this.labelusername.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelusername.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelusername.Location = new System.Drawing.Point(116, 5);
            this.labelusername.Name = "labelusername";
            this.labelusername.Size = new System.Drawing.Size(91, 26);
            this.labelusername.TabIndex = 2;
            this.labelusername.Text = "Username";
            this.labelusername.TextChanged += new System.EventHandler(this.labelusername_TextChanged);
            // 
            // panelheaderone
            // 
            this.panelheaderone.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelheaderone.BackgroundImage")));
            this.panelheaderone.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelheaderone.Controls.Add(this.bunifuCustomLabel2);
            this.panelheaderone.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelheaderone.GradientBottomLeft = System.Drawing.Color.SteelBlue;
            this.panelheaderone.GradientBottomRight = System.Drawing.Color.SteelBlue;
            this.panelheaderone.GradientTopLeft = System.Drawing.Color.SteelBlue;
            this.panelheaderone.GradientTopRight = System.Drawing.Color.RoyalBlue;
            this.panelheaderone.Location = new System.Drawing.Point(326, 67);
            this.panelheaderone.Name = "panelheaderone";
            this.panelheaderone.Padding = new System.Windows.Forms.Padding(10);
            this.panelheaderone.Quality = 10;
            this.panelheaderone.Size = new System.Drawing.Size(959, 39);
            this.panelheaderone.TabIndex = 1;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(10, 10);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(206, 26);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "We are here for your help";
            // 
            // panelheader
            // 
            this.panelheader.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelheader.BackgroundImage")));
            this.panelheader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelheader.Controls.Add(this.bunifuImageButtonlogout);
            this.panelheader.Controls.Add(this.bunifuCustomLabel4);
            this.panelheader.Controls.Add(this.bunifuCustomLabel5);
            this.panelheader.Controls.Add(this.bunifuCustomLabel3);
            this.panelheader.Controls.Add(this.bunifuCustomLabel1);
            this.panelheader.Controls.Add(this.imagebuttonexit);
            this.panelheader.Controls.Add(this.imagebuttonminimize);
            this.panelheader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelheader.GradientBottomLeft = System.Drawing.Color.WhiteSmoke;
            this.panelheader.GradientBottomRight = System.Drawing.Color.White;
            this.panelheader.GradientTopLeft = System.Drawing.Color.Linen;
            this.panelheader.GradientTopRight = System.Drawing.Color.Snow;
            this.panelheader.Location = new System.Drawing.Point(326, 0);
            this.panelheader.Name = "panelheader";
            this.panelheader.Padding = new System.Windows.Forms.Padding(10);
            this.panelheader.Quality = 10;
            this.panelheader.Size = new System.Drawing.Size(959, 67);
            this.panelheader.TabIndex = 4;
            // 
            // bunifuImageButtonlogout
            // 
            this.bunifuImageButtonlogout.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButtonlogout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.bunifuImageButtonlogout.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuImageButtonlogout.Image = global::UserInterface.Properties.Resources.logout;
            this.bunifuImageButtonlogout.ImageActive = null;
            this.bunifuImageButtonlogout.Location = new System.Drawing.Point(802, 10);
            this.bunifuImageButtonlogout.Name = "bunifuImageButtonlogout";
            this.bunifuImageButtonlogout.Size = new System.Drawing.Size(65, 47);
            this.bunifuImageButtonlogout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButtonlogout.TabIndex = 6;
            this.bunifuImageButtonlogout.TabStop = false;
            this.bunifuImageButtonlogout.Zoom = 7;
            this.bunifuImageButtonlogout.Click += new System.EventHandler(this.bunifuImageButtonlogout_Click);
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel4.ForeColor = System.Drawing.Color.DimGray;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(32, 26);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(32, 21);
            this.bunifuCustomLabel4.TabIndex = 5;
            this.bunifuCustomLabel4.Text = "60k";
            // 
            // bunifuCustomLabel5
            // 
            this.bunifuCustomLabel5.AutoSize = true;
            this.bunifuCustomLabel5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel5.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel5.ForeColor = System.Drawing.Color.DimGray;
            this.bunifuCustomLabel5.Location = new System.Drawing.Point(146, 0);
            this.bunifuCustomLabel5.Name = "bunifuCustomLabel5";
            this.bunifuCustomLabel5.Size = new System.Drawing.Size(112, 26);
            this.bunifuCustomLabel5.TabIndex = 0;
            this.bunifuCustomLabel5.Text = "Online Users";
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Minion Pro SmBd", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.DimGray;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(175, 26);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(32, 21);
            this.bunifuCustomLabel3.TabIndex = 3;
            this.bunifuCustomLabel3.Text = "60k";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Minion Pro SmBd", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.DimGray;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(10, 0);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(102, 26);
            this.bunifuCustomLabel1.TabIndex = 2;
            this.bunifuCustomLabel1.Text = "Total Users ";
            // 
            // imagebuttonexit
            // 
            this.imagebuttonexit.BackColor = System.Drawing.Color.Transparent;
            this.imagebuttonexit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imagebuttonexit.Dock = System.Windows.Forms.DockStyle.Right;
            this.imagebuttonexit.Image = global::UserInterface.Properties.Resources.Minus_64px;
            this.imagebuttonexit.ImageActive = null;
            this.imagebuttonexit.Location = new System.Drawing.Point(867, 10);
            this.imagebuttonexit.Name = "imagebuttonexit";
            this.imagebuttonexit.Size = new System.Drawing.Size(41, 47);
            this.imagebuttonexit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imagebuttonexit.TabIndex = 0;
            this.imagebuttonexit.TabStop = false;
            this.imagebuttonexit.Zoom = 7;
            this.imagebuttonexit.Click += new System.EventHandler(this.imagebuttonexit_Click);
            // 
            // imagebuttonminimize
            // 
            this.imagebuttonminimize.BackColor = System.Drawing.Color.Transparent;
            this.imagebuttonminimize.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imagebuttonminimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.imagebuttonminimize.Image = global::UserInterface.Properties.Resources.Cancel_64px;
            this.imagebuttonminimize.ImageActive = null;
            this.imagebuttonminimize.Location = new System.Drawing.Point(908, 10);
            this.imagebuttonminimize.Name = "imagebuttonminimize";
            this.imagebuttonminimize.Size = new System.Drawing.Size(41, 47);
            this.imagebuttonminimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imagebuttonminimize.TabIndex = 1;
            this.imagebuttonminimize.TabStop = false;
            this.imagebuttonminimize.Zoom = 7;
            this.imagebuttonminimize.Click += new System.EventHandler(this.imagebuttonminimize_Click);
            // 
            // panelsidebar
            // 
            this.panelsidebar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelsidebar.BackgroundImage")));
            this.panelsidebar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelsidebar.Controls.Add(this.btnhelp);
            this.panelsidebar.Controls.Add(this.btnreview);
            this.panelsidebar.Controls.Add(this.btndocument);
            this.panelsidebar.Controls.Add(this.btnexamoffice);
            this.panelsidebar.Controls.Add(this.btncollegehead);
            this.panelsidebar.Controls.Add(this.btnstaff);
            this.panelsidebar.Controls.Add(this.btnexam);
            this.panelsidebar.Controls.Add(this.btnadmission);
            this.panelsidebar.Controls.Add(this.btnhome);
            this.panelsidebar.Controls.Add(this.bunifuGradientPanel2);
            this.panelsidebar.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelsidebar.GradientBottomLeft = System.Drawing.Color.RoyalBlue;
            this.panelsidebar.GradientBottomRight = System.Drawing.Color.SteelBlue;
            this.panelsidebar.GradientTopLeft = System.Drawing.Color.SteelBlue;
            this.panelsidebar.GradientTopRight = System.Drawing.Color.SteelBlue;
            this.panelsidebar.Location = new System.Drawing.Point(0, 0);
            this.panelsidebar.Name = "panelsidebar";
            this.panelsidebar.Quality = 10;
            this.panelsidebar.Size = new System.Drawing.Size(326, 584);
            this.panelsidebar.TabIndex = 3;
            // 
            // btnhelp
            // 
            this.btnhelp.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnhelp.BackColor = System.Drawing.Color.SteelBlue;
            this.btnhelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnhelp.BorderRadius = 5;
            this.btnhelp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btnhelp.ButtonText = "Helps ";
            this.btnhelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnhelp.DisabledColor = System.Drawing.Color.Gray;
            this.btnhelp.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnhelp.Iconcolor = System.Drawing.Color.Transparent;
            this.btnhelp.Iconimage = global::UserInterface.Properties.Resources.Helping_Hand_104px;
            this.btnhelp.Iconimage_right = null;
            this.btnhelp.Iconimage_right_Selected = null;
            this.btnhelp.Iconimage_Selected = null;
            this.btnhelp.IconMarginLeft = 10;
            this.btnhelp.IconMarginRight = 0;
            this.btnhelp.IconRightVisible = true;
            this.btnhelp.IconRightZoom = 0D;
            this.btnhelp.IconVisible = true;
            this.btnhelp.IconZoom = 90D;
            this.btnhelp.IsTab = true;
            this.btnhelp.Location = new System.Drawing.Point(0, 490);
            this.btnhelp.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.btnhelp.Name = "btnhelp";
            this.btnhelp.Normalcolor = System.Drawing.Color.SteelBlue;
            this.btnhelp.OnHovercolor = System.Drawing.Color.Teal;
            this.btnhelp.OnHoverTextColor = System.Drawing.Color.White;
            this.btnhelp.selected = false;
            this.btnhelp.Size = new System.Drawing.Size(326, 48);
            this.btnhelp.TabIndex = 10;
            this.btnhelp.Text = "Helps ";
            this.btnhelp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnhelp.Textcolor = System.Drawing.Color.Gainsboro;
            this.btnhelp.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnhelp.Click += new System.EventHandler(this.btnhelp_Click);
            // 
            // btnreview
            // 
            this.btnreview.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnreview.BackColor = System.Drawing.Color.SteelBlue;
            this.btnreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnreview.BorderRadius = 5;
            this.btnreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btnreview.ButtonText = "Review ";
            this.btnreview.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnreview.DisabledColor = System.Drawing.Color.Gray;
            this.btnreview.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnreview.Iconcolor = System.Drawing.Color.Transparent;
            this.btnreview.Iconimage = global::UserInterface.Properties.Resources.Checked_Checkbox_96px;
            this.btnreview.Iconimage_right = null;
            this.btnreview.Iconimage_right_Selected = null;
            this.btnreview.Iconimage_Selected = null;
            this.btnreview.IconMarginLeft = 10;
            this.btnreview.IconMarginRight = 0;
            this.btnreview.IconRightVisible = true;
            this.btnreview.IconRightZoom = 0D;
            this.btnreview.IconVisible = true;
            this.btnreview.IconZoom = 90D;
            this.btnreview.IsTab = true;
            this.btnreview.Location = new System.Drawing.Point(0, 442);
            this.btnreview.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.btnreview.Name = "btnreview";
            this.btnreview.Normalcolor = System.Drawing.Color.SteelBlue;
            this.btnreview.OnHovercolor = System.Drawing.Color.Teal;
            this.btnreview.OnHoverTextColor = System.Drawing.Color.White;
            this.btnreview.selected = false;
            this.btnreview.Size = new System.Drawing.Size(326, 48);
            this.btnreview.TabIndex = 3;
            this.btnreview.Text = "Review ";
            this.btnreview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnreview.Textcolor = System.Drawing.Color.Gainsboro;
            this.btnreview.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnreview.Click += new System.EventHandler(this.btnreview_Click);
            // 
            // btndocument
            // 
            this.btndocument.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btndocument.BackColor = System.Drawing.Color.SteelBlue;
            this.btndocument.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btndocument.BorderRadius = 5;
            this.btndocument.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btndocument.ButtonText = "Issues Document";
            this.btndocument.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btndocument.DisabledColor = System.Drawing.Color.Gray;
            this.btndocument.Dock = System.Windows.Forms.DockStyle.Top;
            this.btndocument.Iconcolor = System.Drawing.Color.Transparent;
            this.btndocument.Iconimage = global::UserInterface.Properties.Resources.Certificate_100px;
            this.btndocument.Iconimage_right = null;
            this.btndocument.Iconimage_right_Selected = null;
            this.btndocument.Iconimage_Selected = null;
            this.btndocument.IconMarginLeft = 10;
            this.btndocument.IconMarginRight = 0;
            this.btndocument.IconRightVisible = true;
            this.btndocument.IconRightZoom = 0D;
            this.btndocument.IconVisible = true;
            this.btndocument.IconZoom = 90D;
            this.btndocument.IsTab = true;
            this.btndocument.Location = new System.Drawing.Point(0, 394);
            this.btndocument.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.btndocument.Name = "btndocument";
            this.btndocument.Normalcolor = System.Drawing.Color.SteelBlue;
            this.btndocument.OnHovercolor = System.Drawing.Color.Teal;
            this.btndocument.OnHoverTextColor = System.Drawing.Color.White;
            this.btndocument.selected = false;
            this.btndocument.Size = new System.Drawing.Size(326, 48);
            this.btndocument.TabIndex = 4;
            this.btndocument.Text = "Issues Document";
            this.btndocument.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btndocument.Textcolor = System.Drawing.Color.Gainsboro;
            this.btndocument.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndocument.Click += new System.EventHandler(this.btndocument_Click);
            // 
            // btnexamoffice
            // 
            this.btnexamoffice.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnexamoffice.BackColor = System.Drawing.Color.SteelBlue;
            this.btnexamoffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnexamoffice.BorderRadius = 5;
            this.btnexamoffice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btnexamoffice.ButtonText = "Exam Control Room";
            this.btnexamoffice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnexamoffice.DisabledColor = System.Drawing.Color.Gray;
            this.btnexamoffice.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnexamoffice.Iconcolor = System.Drawing.Color.Transparent;
            this.btnexamoffice.Iconimage = global::UserInterface.Properties.Resources.Control_Panel_96px;
            this.btnexamoffice.Iconimage_right = null;
            this.btnexamoffice.Iconimage_right_Selected = null;
            this.btnexamoffice.Iconimage_Selected = null;
            this.btnexamoffice.IconMarginLeft = 10;
            this.btnexamoffice.IconMarginRight = 0;
            this.btnexamoffice.IconRightVisible = true;
            this.btnexamoffice.IconRightZoom = 0D;
            this.btnexamoffice.IconVisible = true;
            this.btnexamoffice.IconZoom = 90D;
            this.btnexamoffice.IsTab = true;
            this.btnexamoffice.Location = new System.Drawing.Point(0, 346);
            this.btnexamoffice.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.btnexamoffice.Name = "btnexamoffice";
            this.btnexamoffice.Normalcolor = System.Drawing.Color.SteelBlue;
            this.btnexamoffice.OnHovercolor = System.Drawing.Color.Teal;
            this.btnexamoffice.OnHoverTextColor = System.Drawing.Color.White;
            this.btnexamoffice.selected = false;
            this.btnexamoffice.Size = new System.Drawing.Size(326, 48);
            this.btnexamoffice.TabIndex = 5;
            this.btnexamoffice.Text = "Exam Control Room";
            this.btnexamoffice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexamoffice.Textcolor = System.Drawing.Color.Gainsboro;
            this.btnexamoffice.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexamoffice.Click += new System.EventHandler(this.btnexamoffice_Click);
            // 
            // btncollegehead
            // 
            this.btncollegehead.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btncollegehead.BackColor = System.Drawing.Color.SteelBlue;
            this.btncollegehead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btncollegehead.BorderRadius = 5;
            this.btncollegehead.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btncollegehead.ButtonText = "College Head Responsibility";
            this.btncollegehead.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btncollegehead.DisabledColor = System.Drawing.Color.Gray;
            this.btncollegehead.Dock = System.Windows.Forms.DockStyle.Top;
            this.btncollegehead.Iconcolor = System.Drawing.Color.Transparent;
            this.btncollegehead.Iconimage = global::UserInterface.Properties.Resources.Admin_96px;
            this.btncollegehead.Iconimage_right = null;
            this.btncollegehead.Iconimage_right_Selected = null;
            this.btncollegehead.Iconimage_Selected = null;
            this.btncollegehead.IconMarginLeft = 10;
            this.btncollegehead.IconMarginRight = 0;
            this.btncollegehead.IconRightVisible = true;
            this.btncollegehead.IconRightZoom = 0D;
            this.btncollegehead.IconVisible = true;
            this.btncollegehead.IconZoom = 90D;
            this.btncollegehead.IsTab = true;
            this.btncollegehead.Location = new System.Drawing.Point(0, 298);
            this.btncollegehead.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.btncollegehead.Name = "btncollegehead";
            this.btncollegehead.Normalcolor = System.Drawing.Color.SteelBlue;
            this.btncollegehead.OnHovercolor = System.Drawing.Color.Teal;
            this.btncollegehead.OnHoverTextColor = System.Drawing.Color.White;
            this.btncollegehead.selected = false;
            this.btncollegehead.Size = new System.Drawing.Size(326, 48);
            this.btncollegehead.TabIndex = 5;
            this.btncollegehead.Text = "College Head Responsibility";
            this.btncollegehead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btncollegehead.Textcolor = System.Drawing.Color.Gainsboro;
            this.btncollegehead.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncollegehead.Click += new System.EventHandler(this.btncollegehead_Click);
            // 
            // btnstaff
            // 
            this.btnstaff.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnstaff.BackColor = System.Drawing.Color.SteelBlue;
            this.btnstaff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnstaff.BorderRadius = 5;
            this.btnstaff.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btnstaff.ButtonText = "Lecturer Responsibility";
            this.btnstaff.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnstaff.DisabledColor = System.Drawing.Color.Gray;
            this.btnstaff.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnstaff.Iconcolor = System.Drawing.Color.Transparent;
            this.btnstaff.Iconimage = global::UserInterface.Properties.Resources.Staff_96px;
            this.btnstaff.Iconimage_right = null;
            this.btnstaff.Iconimage_right_Selected = null;
            this.btnstaff.Iconimage_Selected = null;
            this.btnstaff.IconMarginLeft = 10;
            this.btnstaff.IconMarginRight = 0;
            this.btnstaff.IconRightVisible = true;
            this.btnstaff.IconRightZoom = 0D;
            this.btnstaff.IconVisible = true;
            this.btnstaff.IconZoom = 90D;
            this.btnstaff.IsTab = true;
            this.btnstaff.Location = new System.Drawing.Point(0, 250);
            this.btnstaff.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.btnstaff.Name = "btnstaff";
            this.btnstaff.Normalcolor = System.Drawing.Color.SteelBlue;
            this.btnstaff.OnHovercolor = System.Drawing.Color.Teal;
            this.btnstaff.OnHoverTextColor = System.Drawing.Color.White;
            this.btnstaff.selected = false;
            this.btnstaff.Size = new System.Drawing.Size(326, 48);
            this.btnstaff.TabIndex = 4;
            this.btnstaff.Text = "Lecturer Responsibility";
            this.btnstaff.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnstaff.Textcolor = System.Drawing.Color.Gainsboro;
            this.btnstaff.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnstaff.Click += new System.EventHandler(this.btnstaff_Click);
            // 
            // btnexam
            // 
            this.btnexam.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnexam.BackColor = System.Drawing.Color.SteelBlue;
            this.btnexam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnexam.BorderRadius = 5;
            this.btnexam.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btnexam.ButtonText = "Examination ";
            this.btnexam.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnexam.DisabledColor = System.Drawing.Color.Gray;
            this.btnexam.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnexam.Iconcolor = System.Drawing.Color.Transparent;
            this.btnexam.Iconimage = global::UserInterface.Properties.Resources.Exam_100px;
            this.btnexam.Iconimage_right = null;
            this.btnexam.Iconimage_right_Selected = null;
            this.btnexam.Iconimage_Selected = null;
            this.btnexam.IconMarginLeft = 10;
            this.btnexam.IconMarginRight = 0;
            this.btnexam.IconRightVisible = true;
            this.btnexam.IconRightZoom = 0D;
            this.btnexam.IconVisible = true;
            this.btnexam.IconZoom = 90D;
            this.btnexam.IsTab = true;
            this.btnexam.Location = new System.Drawing.Point(0, 202);
            this.btnexam.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.btnexam.Name = "btnexam";
            this.btnexam.Normalcolor = System.Drawing.Color.SteelBlue;
            this.btnexam.OnHovercolor = System.Drawing.Color.Teal;
            this.btnexam.OnHoverTextColor = System.Drawing.Color.White;
            this.btnexam.selected = false;
            this.btnexam.Size = new System.Drawing.Size(326, 48);
            this.btnexam.TabIndex = 3;
            this.btnexam.Text = "Examination ";
            this.btnexam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexam.Textcolor = System.Drawing.Color.Gainsboro;
            this.btnexam.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexam.Click += new System.EventHandler(this.btnexam_Click);
            // 
            // btnadmission
            // 
            this.btnadmission.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnadmission.BackColor = System.Drawing.Color.SteelBlue;
            this.btnadmission.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnadmission.BorderRadius = 5;
            this.btnadmission.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btnadmission.ButtonText = "Admission Office";
            this.btnadmission.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnadmission.DisabledColor = System.Drawing.Color.Gray;
            this.btnadmission.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnadmission.Iconcolor = System.Drawing.Color.Transparent;
            this.btnadmission.Iconimage = global::UserInterface.Properties.Resources.Plus_100px;
            this.btnadmission.Iconimage_right = null;
            this.btnadmission.Iconimage_right_Selected = null;
            this.btnadmission.Iconimage_Selected = null;
            this.btnadmission.IconMarginLeft = 10;
            this.btnadmission.IconMarginRight = 0;
            this.btnadmission.IconRightVisible = true;
            this.btnadmission.IconRightZoom = 0D;
            this.btnadmission.IconVisible = true;
            this.btnadmission.IconZoom = 90D;
            this.btnadmission.IsTab = true;
            this.btnadmission.Location = new System.Drawing.Point(0, 154);
            this.btnadmission.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.btnadmission.Name = "btnadmission";
            this.btnadmission.Normalcolor = System.Drawing.Color.SteelBlue;
            this.btnadmission.OnHovercolor = System.Drawing.Color.Teal;
            this.btnadmission.OnHoverTextColor = System.Drawing.Color.White;
            this.btnadmission.selected = false;
            this.btnadmission.Size = new System.Drawing.Size(326, 48);
            this.btnadmission.TabIndex = 1;
            this.btnadmission.Text = "Admission Office";
            this.btnadmission.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadmission.Textcolor = System.Drawing.Color.Gainsboro;
            this.btnadmission.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadmission.Click += new System.EventHandler(this.btnadmission_Click);
            // 
            // btnhome
            // 
            this.btnhome.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnhome.BackColor = System.Drawing.Color.SteelBlue;
            this.btnhome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnhome.BorderRadius = 5;
            this.btnhome.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btnhome.ButtonText = "Home ";
            this.btnhome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnhome.DisabledColor = System.Drawing.Color.Gray;
            this.btnhome.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnhome.Iconcolor = System.Drawing.Color.Transparent;
            this.btnhome.Iconimage = global::UserInterface.Properties.Resources.Home_96px1;
            this.btnhome.Iconimage_right = null;
            this.btnhome.Iconimage_right_Selected = null;
            this.btnhome.Iconimage_Selected = null;
            this.btnhome.IconMarginLeft = 10;
            this.btnhome.IconMarginRight = 0;
            this.btnhome.IconRightVisible = true;
            this.btnhome.IconRightZoom = 0D;
            this.btnhome.IconVisible = true;
            this.btnhome.IconZoom = 90D;
            this.btnhome.IsTab = true;
            this.btnhome.Location = new System.Drawing.Point(0, 106);
            this.btnhome.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.btnhome.Name = "btnhome";
            this.btnhome.Normalcolor = System.Drawing.Color.SteelBlue;
            this.btnhome.OnHovercolor = System.Drawing.Color.Teal;
            this.btnhome.OnHoverTextColor = System.Drawing.Color.White;
            this.btnhome.selected = false;
            this.btnhome.Size = new System.Drawing.Size(326, 48);
            this.btnhome.TabIndex = 0;
            this.btnhome.Text = "Home ";
            this.btnhome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnhome.Textcolor = System.Drawing.Color.Gainsboro;
            this.btnhome.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnhome.Click += new System.EventHandler(this.btnhome_Click);
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.collegelogo);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuGradientPanel4);
            this.bunifuGradientPanel2.Controls.Add(this.label1);
            this.bunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.Silver;
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.Gray;
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(326, 106);
            this.bunifuGradientPanel2.TabIndex = 2;
            // 
            // collegelogo
            // 
            this.collegelogo.BackColor = System.Drawing.Color.Transparent;
            this.collegelogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.collegelogo.Image = global::UserInterface.Properties.Resources.logo;
            this.collegelogo.ImageActive = null;
            this.collegelogo.Location = new System.Drawing.Point(0, 0);
            this.collegelogo.Name = "collegelogo";
            this.collegelogo.Size = new System.Drawing.Size(65, 56);
            this.collegelogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.collegelogo.TabIndex = 6;
            this.collegelogo.TabStop = false;
            this.collegelogo.Zoom = 0;
            // 
            // bunifuGradientPanel4
            // 
            this.bunifuGradientPanel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel4.BackgroundImage")));
            this.bunifuGradientPanel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel4.Controls.Add(this.textsearch);
            this.bunifuGradientPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuGradientPanel4.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel4.Location = new System.Drawing.Point(0, 56);
            this.bunifuGradientPanel4.Name = "bunifuGradientPanel4";
            this.bunifuGradientPanel4.Padding = new System.Windows.Forms.Padding(5);
            this.bunifuGradientPanel4.Quality = 10;
            this.bunifuGradientPanel4.Size = new System.Drawing.Size(326, 50);
            this.bunifuGradientPanel4.TabIndex = 7;
            // 
            // textsearch
            // 
            this.textsearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textsearch.ForeColor = System.Drawing.Color.Silver;
            this.textsearch.Location = new System.Drawing.Point(3, 8);
            this.textsearch.Multiline = true;
            this.textsearch.Name = "textsearch";
            this.textsearch.Size = new System.Drawing.Size(320, 34);
            this.textsearch.TabIndex = 0;
            this.textsearch.Text = "Search Information";
            this.textsearch.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textsearch_MouseClick);
            this.textsearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textsearch_KeyUp);
            this.textsearch.MouseLeave += new System.EventHandler(this.textsearch_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(60, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Computer Based Examination";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1285, 584);
            this.Controls.Add(this.panelcontainer);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Controls.Add(this.panelheaderone);
            this.Controls.Add(this.panelheader);
            this.Controls.Add(this.panelsidebar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Computer based Exam";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.main_Load);
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel1.PerformLayout();
            this.panelheaderone.ResumeLayout(false);
            this.panelheaderone.PerformLayout();
            this.panelheader.ResumeLayout(false);
            this.panelheader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButtonlogout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagebuttonexit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagebuttonminimize)).EndInit();
            this.panelsidebar.ResumeLayout(false);
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.collegelogo)).EndInit();
            this.bunifuGradientPanel4.ResumeLayout(false);
            this.bunifuGradientPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuGradientPanel panelheader;
        private Bunifu.Framework.UI.BunifuImageButton imagebuttonexit;
        private Bunifu.Framework.UI.BunifuImageButton imagebuttonminimize;
        private Bunifu.Framework.UI.BunifuGradientPanel panelsidebar;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private Bunifu.Framework.UI.BunifuGradientPanel panelheaderone;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuImageButton collegelogo;
        private Bunifu.Framework.UI.BunifuGradientPanel panelcontainer;
        private Bunifu.Framework.UI.BunifuFlatButton btnhome;
        private Bunifu.Framework.UI.BunifuFlatButton btnstaff;
        private Bunifu.Framework.UI.BunifuFlatButton btncollegehead;
        private Bunifu.Framework.UI.BunifuFlatButton btnadmission;
        private Bunifu.Framework.UI.BunifuFlatButton btnexamoffice;
        private Bunifu.Framework.UI.BunifuFlatButton btndocument;
        private Bunifu.Framework.UI.BunifuFlatButton btnreview;
        private Bunifu.Framework.UI.BunifuFlatButton btnexam;
        private Bunifu.Framework.UI.BunifuFlatButton btnhelp;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        public Bunifu.Framework.UI.BunifuColorTransition colortransition;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel4;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel5;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButtonlogout;
        private System.Windows.Forms.TextBox textsearch;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel6;
        public Bunifu.Framework.UI.BunifuCustomLabel labelusername;
    }
}

