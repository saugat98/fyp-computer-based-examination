﻿namespace UserInterface
{
    partial class principal
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(principal));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabPageaddcourse = new System.Windows.Forms.TabPage();
            this.dataGridViewcourse = new System.Windows.Forms.DataGridView();
            this.bunifuGradientPanel11 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.buttondelete = new System.Windows.Forms.Button();
            this.btnclear = new System.Windows.Forms.Button();
            this.btnsubmit = new System.Windows.Forms.Button();
            this.bunifuGradientPanel4 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.textcourse = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.bunifuGradientPanel24 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel21 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.coursedesc = new System.Windows.Forms.RichTextBox();
            this.bunifuGradientPanel22 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel33 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.combofaculty = new System.Windows.Forms.ComboBox();
            this.bunifuGradientPanel34 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel14 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.combolabel = new System.Windows.Forms.ComboBox();
            this.bunifuGradientPanel13 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControlprincipal = new System.Windows.Forms.TabControl();
            this.tabPagesecurityQuestions = new System.Windows.Forms.TabPage();
            this.dataGridViewsecurityquestions = new System.Windows.Forms.DataGridView();
            this.colsid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colqid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colquestion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.buttonsecurityclear = new System.Windows.Forms.Button();
            this.buttonsecuritysubmit = new System.Windows.Forms.Button();
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel6 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.textdesc = new System.Windows.Forms.RichTextBox();
            this.bunifuGradientPanel7 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel8 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel12 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.textsecurityquestion = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.bunifuGradientPanel15 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.colsn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colcid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collabel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colfaculty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCourse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageaddcourse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewcourse)).BeginInit();
            this.bunifuGradientPanel11.SuspendLayout();
            this.bunifuGradientPanel4.SuspendLayout();
            this.bunifuGradientPanel24.SuspendLayout();
            this.bunifuGradientPanel21.SuspendLayout();
            this.bunifuGradientPanel22.SuspendLayout();
            this.bunifuGradientPanel2.SuspendLayout();
            this.bunifuGradientPanel33.SuspendLayout();
            this.bunifuGradientPanel34.SuspendLayout();
            this.bunifuGradientPanel14.SuspendLayout();
            this.bunifuGradientPanel13.SuspendLayout();
            this.tabControlprincipal.SuspendLayout();
            this.tabPagesecurityQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewsecurityquestions)).BeginInit();
            this.bunifuGradientPanel1.SuspendLayout();
            this.bunifuGradientPanel3.SuspendLayout();
            this.bunifuGradientPanel6.SuspendLayout();
            this.bunifuGradientPanel7.SuspendLayout();
            this.bunifuGradientPanel8.SuspendLayout();
            this.bunifuGradientPanel12.SuspendLayout();
            this.bunifuGradientPanel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPageaddcourse
            // 
            this.tabPageaddcourse.BackColor = System.Drawing.Color.White;
            this.tabPageaddcourse.Controls.Add(this.dataGridViewcourse);
            this.tabPageaddcourse.Controls.Add(this.bunifuGradientPanel11);
            this.tabPageaddcourse.Controls.Add(this.bunifuGradientPanel4);
            this.tabPageaddcourse.Controls.Add(this.bunifuGradientPanel2);
            this.tabPageaddcourse.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabPageaddcourse.Location = new System.Drawing.Point(4, 44);
            this.tabPageaddcourse.Name = "tabPageaddcourse";
            this.tabPageaddcourse.Padding = new System.Windows.Forms.Padding(5);
            this.tabPageaddcourse.Size = new System.Drawing.Size(942, 447);
            this.tabPageaddcourse.TabIndex = 0;
            this.tabPageaddcourse.Text = "Add Course";
            // 
            // dataGridViewcourse
            // 
            this.dataGridViewcourse.AllowUserToAddRows = false;
            this.dataGridViewcourse.AllowUserToDeleteRows = false;
            this.dataGridViewcourse.AllowUserToResizeColumns = false;
            this.dataGridViewcourse.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dataGridViewcourse.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewcourse.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewcourse.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewcourse.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridViewcourse.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GrayText;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewcourse.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewcourse.ColumnHeadersHeight = 50;
            this.dataGridViewcourse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewcourse.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colsn,
            this.colcid,
            this.collabel,
            this.colfaculty,
            this.colCourse,
            this.coldescription});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.GrayText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewcourse.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewcourse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewcourse.EnableHeadersVisualStyles = false;
            this.dataGridViewcourse.GridColor = System.Drawing.Color.Gainsboro;
            this.dataGridViewcourse.Location = new System.Drawing.Point(5, 193);
            this.dataGridViewcourse.Name = "dataGridViewcourse";
            this.dataGridViewcourse.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowFrame;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GrayText;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewcourse.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewcourse.RowHeadersWidth = 10;
            this.dataGridViewcourse.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewcourse.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewcourse.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewcourse.ShowEditingIcon = false;
            this.dataGridViewcourse.Size = new System.Drawing.Size(932, 249);
            this.dataGridViewcourse.TabIndex = 35;
            this.dataGridViewcourse.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewcourse_RowHeaderMouseDoubleClick);
            // 
            // bunifuGradientPanel11
            // 
            this.bunifuGradientPanel11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel11.BackgroundImage")));
            this.bunifuGradientPanel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel11.Controls.Add(this.buttondelete);
            this.bunifuGradientPanel11.Controls.Add(this.btnclear);
            this.bunifuGradientPanel11.Controls.Add(this.btnsubmit);
            this.bunifuGradientPanel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel11.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel11.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel11.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel11.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel11.Location = new System.Drawing.Point(5, 130);
            this.bunifuGradientPanel11.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.bunifuGradientPanel11.Name = "bunifuGradientPanel11";
            this.bunifuGradientPanel11.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.bunifuGradientPanel11.Quality = 10;
            this.bunifuGradientPanel11.Size = new System.Drawing.Size(932, 63);
            this.bunifuGradientPanel11.TabIndex = 34;
            // 
            // buttondelete
            // 
            this.buttondelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttondelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttondelete.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttondelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttondelete.Location = new System.Drawing.Point(630, 4);
            this.buttondelete.Name = "buttondelete";
            this.buttondelete.Size = new System.Drawing.Size(122, 55);
            this.buttondelete.TabIndex = 8;
            this.buttondelete.Text = "Delete";
            this.buttondelete.UseVisualStyleBackColor = false;
            this.buttondelete.Visible = false;
            this.buttondelete.Click += new System.EventHandler(this.buttondelete_Click);
            // 
            // btnclear
            // 
            this.btnclear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnclear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclear.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnclear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnclear.Location = new System.Drawing.Point(475, 4);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(122, 55);
            this.btnclear.TabIndex = 7;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = false;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // btnsubmit
            // 
            this.btnsubmit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnsubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsubmit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsubmit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsubmit.Location = new System.Drawing.Point(310, 4);
            this.btnsubmit.Name = "btnsubmit";
            this.btnsubmit.Size = new System.Drawing.Size(122, 55);
            this.btnsubmit.TabIndex = 6;
            this.btnsubmit.Text = "Submit";
            this.btnsubmit.UseVisualStyleBackColor = false;
            this.btnsubmit.Click += new System.EventHandler(this.btnsubmit_Click);
            // 
            // bunifuGradientPanel4
            // 
            this.bunifuGradientPanel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel4.BackgroundImage")));
            this.bunifuGradientPanel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel4.Controls.Add(this.textcourse);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuGradientPanel24);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuGradientPanel21);
            this.bunifuGradientPanel4.Controls.Add(this.bunifuGradientPanel22);
            this.bunifuGradientPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel4.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel4.Location = new System.Drawing.Point(5, 56);
            this.bunifuGradientPanel4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.bunifuGradientPanel4.Name = "bunifuGradientPanel4";
            this.bunifuGradientPanel4.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.bunifuGradientPanel4.Quality = 10;
            this.bunifuGradientPanel4.Size = new System.Drawing.Size(932, 74);
            this.bunifuGradientPanel4.TabIndex = 27;
            // 
            // textcourse
            // 
            this.textcourse.BackColor = System.Drawing.SystemColors.Menu;
            this.textcourse.BorderColor = System.Drawing.Color.SeaGreen;
            this.textcourse.Dock = System.Windows.Forms.DockStyle.Top;
            this.textcourse.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textcourse.ForeColor = System.Drawing.Color.Silver;
            this.textcourse.Location = new System.Drawing.Point(687, 5);
            this.textcourse.MaxLength = 40;
            this.textcourse.Multiline = true;
            this.textcourse.Name = "textcourse";
            this.textcourse.Size = new System.Drawing.Size(245, 47);
            this.textcourse.TabIndex = 9;
            this.textcourse.Text = "Enter New course";
            this.textcourse.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textcourse_MouseClick);
            this.textcourse.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textcourse_KeyUp);
            this.textcourse.MouseLeave += new System.EventHandler(this.textcourse_MouseLeave);
            // 
            // bunifuGradientPanel24
            // 
            this.bunifuGradientPanel24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel24.BackgroundImage")));
            this.bunifuGradientPanel24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel24.Controls.Add(this.label3);
            this.bunifuGradientPanel24.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel24.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel24.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel24.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel24.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel24.Location = new System.Drawing.Point(510, 5);
            this.bunifuGradientPanel24.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel24.Name = "bunifuGradientPanel24";
            this.bunifuGradientPanel24.Quality = 10;
            this.bunifuGradientPanel24.Size = new System.Drawing.Size(177, 69);
            this.bunifuGradientPanel24.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 26);
            this.label3.TabIndex = 1;
            this.label3.Text = "New Course ";
            // 
            // bunifuGradientPanel21
            // 
            this.bunifuGradientPanel21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel21.BackgroundImage")));
            this.bunifuGradientPanel21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel21.Controls.Add(this.coursedesc);
            this.bunifuGradientPanel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel21.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel21.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel21.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel21.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel21.Location = new System.Drawing.Point(187, 5);
            this.bunifuGradientPanel21.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel21.Name = "bunifuGradientPanel21";
            this.bunifuGradientPanel21.Quality = 10;
            this.bunifuGradientPanel21.Size = new System.Drawing.Size(323, 69);
            this.bunifuGradientPanel21.TabIndex = 7;
            // 
            // coursedesc
            // 
            this.coursedesc.BackColor = System.Drawing.SystemColors.Menu;
            this.coursedesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.coursedesc.Font = new System.Drawing.Font("Minion Pro Med", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coursedesc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.coursedesc.Location = new System.Drawing.Point(0, 0);
            this.coursedesc.Name = "coursedesc";
            this.coursedesc.Size = new System.Drawing.Size(323, 69);
            this.coursedesc.TabIndex = 4;
            this.coursedesc.Text = "";
            this.coursedesc.MouseClick += new System.Windows.Forms.MouseEventHandler(this.coursedesc_MouseClick);
            this.coursedesc.KeyUp += new System.Windows.Forms.KeyEventHandler(this.coursedesc_KeyUp);
            this.coursedesc.MouseLeave += new System.EventHandler(this.coursedesc_MouseLeave);
            // 
            // bunifuGradientPanel22
            // 
            this.bunifuGradientPanel22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel22.BackgroundImage")));
            this.bunifuGradientPanel22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel22.Controls.Add(this.label10);
            this.bunifuGradientPanel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel22.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel22.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel22.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel22.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel22.Location = new System.Drawing.Point(10, 5);
            this.bunifuGradientPanel22.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel22.Name = "bunifuGradientPanel22";
            this.bunifuGradientPanel22.Quality = 10;
            this.bunifuGradientPanel22.Size = new System.Drawing.Size(177, 69);
            this.bunifuGradientPanel22.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 26);
            this.label10.TabIndex = 1;
            this.label10.Text = "Description";
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.bunifuGradientPanel33);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuGradientPanel34);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuGradientPanel14);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuGradientPanel13);
            this.bunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(5, 5);
            this.bunifuGradientPanel2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(932, 51);
            this.bunifuGradientPanel2.TabIndex = 25;
            // 
            // bunifuGradientPanel33
            // 
            this.bunifuGradientPanel33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel33.BackgroundImage")));
            this.bunifuGradientPanel33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel33.Controls.Add(this.combofaculty);
            this.bunifuGradientPanel33.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel33.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel33.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel33.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel33.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel33.Location = new System.Drawing.Point(687, 5);
            this.bunifuGradientPanel33.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel33.Name = "bunifuGradientPanel33";
            this.bunifuGradientPanel33.Quality = 10;
            this.bunifuGradientPanel33.Size = new System.Drawing.Size(250, 46);
            this.bunifuGradientPanel33.TabIndex = 9;
            // 
            // combofaculty
            // 
            this.combofaculty.AutoCompleteCustomSource.AddRange(new string[] {
            "Choose Course"});
            this.combofaculty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.combofaculty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.combofaculty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combofaculty.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.combofaculty.Font = new System.Drawing.Font("Minion Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combofaculty.ForeColor = System.Drawing.Color.LightGray;
            this.combofaculty.FormattingEnabled = true;
            this.combofaculty.Items.AddRange(new object[] {
            "Choose Faculty",
            "Science & Engineering",
            "Management"});
            this.combofaculty.Location = new System.Drawing.Point(0, 0);
            this.combofaculty.Name = "combofaculty";
            this.combofaculty.Size = new System.Drawing.Size(250, 30);
            this.combofaculty.TabIndex = 38;
            // 
            // bunifuGradientPanel34
            // 
            this.bunifuGradientPanel34.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel34.BackgroundImage")));
            this.bunifuGradientPanel34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel34.Controls.Add(this.label13);
            this.bunifuGradientPanel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel34.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel34.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel34.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel34.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel34.Location = new System.Drawing.Point(510, 5);
            this.bunifuGradientPanel34.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel34.Name = "bunifuGradientPanel34";
            this.bunifuGradientPanel34.Quality = 10;
            this.bunifuGradientPanel34.Size = new System.Drawing.Size(177, 46);
            this.bunifuGradientPanel34.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 26);
            this.label13.TabIndex = 1;
            this.label13.Text = "Faculty";
            // 
            // bunifuGradientPanel14
            // 
            this.bunifuGradientPanel14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel14.BackgroundImage")));
            this.bunifuGradientPanel14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel14.Controls.Add(this.combolabel);
            this.bunifuGradientPanel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel14.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel14.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel14.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel14.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel14.Location = new System.Drawing.Point(187, 5);
            this.bunifuGradientPanel14.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel14.Name = "bunifuGradientPanel14";
            this.bunifuGradientPanel14.Quality = 10;
            this.bunifuGradientPanel14.Size = new System.Drawing.Size(323, 46);
            this.bunifuGradientPanel14.TabIndex = 1;
            // 
            // combolabel
            // 
            this.combolabel.AutoCompleteCustomSource.AddRange(new string[] {
            "Choose Course"});
            this.combolabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.combolabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.combolabel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combolabel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.combolabel.Font = new System.Drawing.Font("Minion Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combolabel.ForeColor = System.Drawing.Color.LightGray;
            this.combolabel.FormattingEnabled = true;
            this.combolabel.Items.AddRange(new object[] {
            "Choose Lavel",
            "Lavel 4",
            "Lavel 5",
            "Lavel 6"});
            this.combolabel.Location = new System.Drawing.Point(0, 0);
            this.combolabel.Name = "combolabel";
            this.combolabel.Size = new System.Drawing.Size(323, 30);
            this.combolabel.TabIndex = 37;
            // 
            // bunifuGradientPanel13
            // 
            this.bunifuGradientPanel13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel13.BackgroundImage")));
            this.bunifuGradientPanel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel13.Controls.Add(this.label1);
            this.bunifuGradientPanel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel13.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel13.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel13.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel13.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel13.Location = new System.Drawing.Point(10, 5);
            this.bunifuGradientPanel13.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel13.Name = "bunifuGradientPanel13";
            this.bunifuGradientPanel13.Quality = 10;
            this.bunifuGradientPanel13.Size = new System.Drawing.Size(177, 46);
            this.bunifuGradientPanel13.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lavel";
            // 
            // tabControlprincipal
            // 
            this.tabControlprincipal.Controls.Add(this.tabPageaddcourse);
            this.tabControlprincipal.Controls.Add(this.tabPagesecurityQuestions);
            this.tabControlprincipal.Controls.Add(this.tabPage1);
            this.tabControlprincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlprincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlprincipal.ItemSize = new System.Drawing.Size(100, 40);
            this.tabControlprincipal.Location = new System.Drawing.Point(0, 0);
            this.tabControlprincipal.Name = "tabControlprincipal";
            this.tabControlprincipal.SelectedIndex = 0;
            this.tabControlprincipal.Size = new System.Drawing.Size(950, 495);
            this.tabControlprincipal.TabIndex = 5;
            // 
            // tabPagesecurityQuestions
            // 
            this.tabPagesecurityQuestions.Controls.Add(this.dataGridViewsecurityquestions);
            this.tabPagesecurityQuestions.Controls.Add(this.bunifuGradientPanel1);
            this.tabPagesecurityQuestions.Controls.Add(this.bunifuGradientPanel3);
            this.tabPagesecurityQuestions.Controls.Add(this.bunifuGradientPanel8);
            this.tabPagesecurityQuestions.Location = new System.Drawing.Point(4, 44);
            this.tabPagesecurityQuestions.Name = "tabPagesecurityQuestions";
            this.tabPagesecurityQuestions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagesecurityQuestions.Size = new System.Drawing.Size(942, 447);
            this.tabPagesecurityQuestions.TabIndex = 1;
            this.tabPagesecurityQuestions.Text = "Security Questions";
            this.tabPagesecurityQuestions.UseVisualStyleBackColor = true;
            // 
            // dataGridViewsecurityquestions
            // 
            this.dataGridViewsecurityquestions.AllowUserToAddRows = false;
            this.dataGridViewsecurityquestions.AllowUserToDeleteRows = false;
            this.dataGridViewsecurityquestions.AllowUserToResizeColumns = false;
            this.dataGridViewsecurityquestions.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dataGridViewsecurityquestions.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewsecurityquestions.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewsecurityquestions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewsecurityquestions.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridViewsecurityquestions.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.GrayText;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewsecurityquestions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewsecurityquestions.ColumnHeadersHeight = 50;
            this.dataGridViewsecurityquestions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colsid,
            this.colqid,
            this.colquestion,
            this.coldesc});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewsecurityquestions.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewsecurityquestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewsecurityquestions.EnableHeadersVisualStyles = false;
            this.dataGridViewsecurityquestions.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewsecurityquestions.Location = new System.Drawing.Point(3, 191);
            this.dataGridViewsecurityquestions.Name = "dataGridViewsecurityquestions";
            this.dataGridViewsecurityquestions.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowFrame;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.GrayText;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewsecurityquestions.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewsecurityquestions.RowHeadersWidth = 10;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridViewsecurityquestions.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewsecurityquestions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewsecurityquestions.ShowEditingIcon = false;
            this.dataGridViewsecurityquestions.Size = new System.Drawing.Size(936, 253);
            this.dataGridViewsecurityquestions.TabIndex = 39;
            // 
            // colsid
            // 
            this.colsid.HeaderText = "S.N";
            this.colsid.Name = "colsid";
            this.colsid.ReadOnly = true;
            // 
            // colqid
            // 
            this.colqid.HeaderText = "Qid";
            this.colqid.Name = "colqid";
            this.colqid.ReadOnly = true;
            this.colqid.Visible = false;
            // 
            // colquestion
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Blue;
            this.colquestion.DefaultCellStyle = dataGridViewCellStyle10;
            this.colquestion.HeaderText = "Question";
            this.colquestion.Name = "colquestion";
            this.colquestion.ReadOnly = true;
            this.colquestion.Width = 300;
            // 
            // coldesc
            // 
            this.coldesc.HeaderText = "Description";
            this.coldesc.Name = "coldesc";
            this.coldesc.ReadOnly = true;
            this.coldesc.Width = 400;
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.buttonsecurityclear);
            this.bunifuGradientPanel1.Controls.Add(this.buttonsecuritysubmit);
            this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(3, 128);
            this.bunifuGradientPanel1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(936, 63);
            this.bunifuGradientPanel1.TabIndex = 38;
            // 
            // buttonsecurityclear
            // 
            this.buttonsecurityclear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonsecurityclear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonsecurityclear.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonsecurityclear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonsecurityclear.Location = new System.Drawing.Point(388, 5);
            this.buttonsecurityclear.Name = "buttonsecurityclear";
            this.buttonsecurityclear.Size = new System.Drawing.Size(122, 55);
            this.buttonsecurityclear.TabIndex = 7;
            this.buttonsecurityclear.Text = "Clear";
            this.buttonsecurityclear.UseVisualStyleBackColor = false;
            this.buttonsecurityclear.Click += new System.EventHandler(this.buttonsecurityclear_Click);
            // 
            // buttonsecuritysubmit
            // 
            this.buttonsecuritysubmit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonsecuritysubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonsecuritysubmit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonsecuritysubmit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonsecuritysubmit.Location = new System.Drawing.Point(204, 6);
            this.buttonsecuritysubmit.Name = "buttonsecuritysubmit";
            this.buttonsecuritysubmit.Size = new System.Drawing.Size(122, 55);
            this.buttonsecuritysubmit.TabIndex = 6;
            this.buttonsecuritysubmit.Text = "Submit";
            this.buttonsecuritysubmit.UseVisualStyleBackColor = false;
            this.buttonsecuritysubmit.Click += new System.EventHandler(this.buttonsecuritysubmit_Click);
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel3.BackgroundImage")));
            this.bunifuGradientPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel3.Controls.Add(this.bunifuGradientPanel6);
            this.bunifuGradientPanel3.Controls.Add(this.bunifuGradientPanel7);
            this.bunifuGradientPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(3, 54);
            this.bunifuGradientPanel3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(936, 74);
            this.bunifuGradientPanel3.TabIndex = 37;
            // 
            // bunifuGradientPanel6
            // 
            this.bunifuGradientPanel6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel6.BackgroundImage")));
            this.bunifuGradientPanel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel6.Controls.Add(this.textdesc);
            this.bunifuGradientPanel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel6.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel6.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel6.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel6.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel6.Location = new System.Drawing.Point(187, 5);
            this.bunifuGradientPanel6.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel6.Name = "bunifuGradientPanel6";
            this.bunifuGradientPanel6.Quality = 10;
            this.bunifuGradientPanel6.Size = new System.Drawing.Size(323, 69);
            this.bunifuGradientPanel6.TabIndex = 7;
            // 
            // textdesc
            // 
            this.textdesc.BackColor = System.Drawing.SystemColors.MenuBar;
            this.textdesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textdesc.Font = new System.Drawing.Font("Minion Pro Med", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textdesc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textdesc.Location = new System.Drawing.Point(0, 0);
            this.textdesc.Name = "textdesc";
            this.textdesc.Size = new System.Drawing.Size(323, 69);
            this.textdesc.TabIndex = 4;
            this.textdesc.Text = "";
            this.textdesc.Click += new System.EventHandler(this.textdesc_Click);
            this.textdesc.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textdesc_KeyUp);
            this.textdesc.MouseLeave += new System.EventHandler(this.textdesc_MouseLeave);
            // 
            // bunifuGradientPanel7
            // 
            this.bunifuGradientPanel7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel7.BackgroundImage")));
            this.bunifuGradientPanel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel7.Controls.Add(this.label4);
            this.bunifuGradientPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel7.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel7.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel7.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel7.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel7.Location = new System.Drawing.Point(10, 5);
            this.bunifuGradientPanel7.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel7.Name = "bunifuGradientPanel7";
            this.bunifuGradientPanel7.Quality = 10;
            this.bunifuGradientPanel7.Size = new System.Drawing.Size(177, 69);
            this.bunifuGradientPanel7.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 26);
            this.label4.TabIndex = 1;
            this.label4.Text = "Description";
            // 
            // bunifuGradientPanel8
            // 
            this.bunifuGradientPanel8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel8.BackgroundImage")));
            this.bunifuGradientPanel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel8.Controls.Add(this.bunifuGradientPanel12);
            this.bunifuGradientPanel8.Controls.Add(this.bunifuGradientPanel15);
            this.bunifuGradientPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel8.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel8.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel8.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel8.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel8.Location = new System.Drawing.Point(3, 3);
            this.bunifuGradientPanel8.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.bunifuGradientPanel8.Name = "bunifuGradientPanel8";
            this.bunifuGradientPanel8.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.bunifuGradientPanel8.Quality = 10;
            this.bunifuGradientPanel8.Size = new System.Drawing.Size(936, 51);
            this.bunifuGradientPanel8.TabIndex = 36;
            // 
            // bunifuGradientPanel12
            // 
            this.bunifuGradientPanel12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel12.BackgroundImage")));
            this.bunifuGradientPanel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel12.Controls.Add(this.textsecurityquestion);
            this.bunifuGradientPanel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel12.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel12.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel12.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel12.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel12.Location = new System.Drawing.Point(187, 5);
            this.bunifuGradientPanel12.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel12.Name = "bunifuGradientPanel12";
            this.bunifuGradientPanel12.Quality = 10;
            this.bunifuGradientPanel12.Size = new System.Drawing.Size(323, 46);
            this.bunifuGradientPanel12.TabIndex = 1;
            // 
            // textsecurityquestion
            // 
            this.textsecurityquestion.BackColor = System.Drawing.SystemColors.Menu;
            this.textsecurityquestion.BorderColor = System.Drawing.Color.SeaGreen;
            this.textsecurityquestion.Dock = System.Windows.Forms.DockStyle.Top;
            this.textsecurityquestion.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textsecurityquestion.ForeColor = System.Drawing.Color.Silver;
            this.textsecurityquestion.Location = new System.Drawing.Point(0, 0);
            this.textsecurityquestion.MaxLength = 300;
            this.textsecurityquestion.Multiline = true;
            this.textsecurityquestion.Name = "textsecurityquestion";
            this.textsecurityquestion.Size = new System.Drawing.Size(323, 47);
            this.textsecurityquestion.TabIndex = 10;
            this.textsecurityquestion.Text = "Enter Security Questions";
            this.textsecurityquestion.Click += new System.EventHandler(this.textsecurityquestion_Click);
            this.textsecurityquestion.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textsecurityquestion_KeyUp);
            this.textsecurityquestion.MouseLeave += new System.EventHandler(this.textsecurityquestion_MouseLeave);
            // 
            // bunifuGradientPanel15
            // 
            this.bunifuGradientPanel15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel15.BackgroundImage")));
            this.bunifuGradientPanel15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel15.Controls.Add(this.label6);
            this.bunifuGradientPanel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel15.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel15.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel15.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel15.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel15.Location = new System.Drawing.Point(10, 5);
            this.bunifuGradientPanel15.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.bunifuGradientPanel15.Name = "bunifuGradientPanel15";
            this.bunifuGradientPanel15.Quality = 10;
            this.bunifuGradientPanel15.Size = new System.Drawing.Size(177, 46);
            this.bunifuGradientPanel15.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 26);
            this.label6.TabIndex = 0;
            this.label6.Text = "Questions";
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 44);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(942, 447);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Verify Documents";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // colsn
            // 
            this.colsn.HeaderText = "S.N";
            this.colsn.Name = "colsn";
            this.colsn.ReadOnly = true;
            // 
            // colcid
            // 
            this.colcid.HeaderText = "cid";
            this.colcid.Name = "colcid";
            this.colcid.ReadOnly = true;
            this.colcid.Visible = false;
            // 
            // collabel
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Blue;
            this.collabel.DefaultCellStyle = dataGridViewCellStyle3;
            this.collabel.HeaderText = "Lavel";
            this.collabel.Name = "collabel";
            this.collabel.ReadOnly = true;
            this.collabel.Width = 150;
            // 
            // colfaculty
            // 
            this.colfaculty.HeaderText = "Faculty";
            this.colfaculty.Name = "colfaculty";
            this.colfaculty.ReadOnly = true;
            this.colfaculty.Width = 160;
            // 
            // colCourse
            // 
            this.colCourse.HeaderText = "Course";
            this.colCourse.Name = "colCourse";
            this.colCourse.ReadOnly = true;
            this.colCourse.Width = 230;
            // 
            // coldescription
            // 
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.coldescription.DefaultCellStyle = dataGridViewCellStyle4;
            this.coldescription.HeaderText = "Description";
            this.coldescription.Name = "coldescription";
            this.coldescription.ReadOnly = true;
            this.coldescription.Width = 380;
            // 
            // principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlprincipal);
            this.Name = "principal";
            this.Size = new System.Drawing.Size(950, 495);
            this.tabPageaddcourse.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewcourse)).EndInit();
            this.bunifuGradientPanel11.ResumeLayout(false);
            this.bunifuGradientPanel4.ResumeLayout(false);
            this.bunifuGradientPanel4.PerformLayout();
            this.bunifuGradientPanel24.ResumeLayout(false);
            this.bunifuGradientPanel24.PerformLayout();
            this.bunifuGradientPanel21.ResumeLayout(false);
            this.bunifuGradientPanel22.ResumeLayout(false);
            this.bunifuGradientPanel22.PerformLayout();
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel33.ResumeLayout(false);
            this.bunifuGradientPanel34.ResumeLayout(false);
            this.bunifuGradientPanel34.PerformLayout();
            this.bunifuGradientPanel14.ResumeLayout(false);
            this.bunifuGradientPanel13.ResumeLayout(false);
            this.bunifuGradientPanel13.PerformLayout();
            this.tabControlprincipal.ResumeLayout(false);
            this.tabPagesecurityQuestions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewsecurityquestions)).EndInit();
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel3.ResumeLayout(false);
            this.bunifuGradientPanel6.ResumeLayout(false);
            this.bunifuGradientPanel7.ResumeLayout(false);
            this.bunifuGradientPanel7.PerformLayout();
            this.bunifuGradientPanel8.ResumeLayout(false);
            this.bunifuGradientPanel12.ResumeLayout(false);
            this.bunifuGradientPanel12.PerformLayout();
            this.bunifuGradientPanel15.ResumeLayout(false);
            this.bunifuGradientPanel15.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tabPageaddcourse;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel11;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btnsubmit;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel4;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textcourse;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel24;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel21;
        private System.Windows.Forms.RichTextBox coursedesc;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel22;
        private System.Windows.Forms.Label label10;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel33;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel34;
        private System.Windows.Forms.Label label13;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel14;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControlprincipal;
        private System.Windows.Forms.DataGridView dataGridViewcourse;
        private System.Windows.Forms.TabPage tabPagesecurityQuestions;
        private System.Windows.Forms.DataGridView dataGridViewsecurityquestions;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private System.Windows.Forms.Button buttonsecurityclear;
        private System.Windows.Forms.Button buttonsecuritysubmit;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel6;
        private System.Windows.Forms.RichTextBox textdesc;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel7;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel8;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel12;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox textsecurityquestion;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox combolabel;
        private System.Windows.Forms.ComboBox combofaculty;
        private System.Windows.Forms.Button buttondelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn colsid;
        private System.Windows.Forms.DataGridViewTextBoxColumn colqid;
        private System.Windows.Forms.DataGridViewTextBoxColumn colquestion;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldesc;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colsn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colcid;
        private System.Windows.Forms.DataGridViewTextBoxColumn collabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colfaculty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCourse;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldescription;
    }
}
