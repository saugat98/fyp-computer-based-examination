﻿namespace UserInterface
{
    partial class admission
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admission));
            this.admissionpanelcontainer = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.admissionpanelheader = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.admissionstudentadd = new Bunifu.Framework.UI.BunifuFlatButton();
            this.admissionstudentview = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuGradientPanel1.SuspendLayout();
            this.admissionpanelheader.SuspendLayout();
            this.SuspendLayout();
            // 
            // admissionpanelcontainer
            // 
            this.admissionpanelcontainer.BackColor = System.Drawing.SystemColors.ControlDark;
            this.admissionpanelcontainer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("admissionpanelcontainer.BackgroundImage")));
            this.admissionpanelcontainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.admissionpanelcontainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.admissionpanelcontainer.GradientBottomLeft = System.Drawing.Color.WhiteSmoke;
            this.admissionpanelcontainer.GradientBottomRight = System.Drawing.Color.WhiteSmoke;
            this.admissionpanelcontainer.GradientTopLeft = System.Drawing.Color.White;
            this.admissionpanelcontainer.GradientTopRight = System.Drawing.Color.White;
            this.admissionpanelcontainer.Location = new System.Drawing.Point(0, 62);
            this.admissionpanelcontainer.Name = "admissionpanelcontainer";
            this.admissionpanelcontainer.Quality = 10;
            this.admissionpanelcontainer.Size = new System.Drawing.Size(1312, 530);
            this.admissionpanelcontainer.TabIndex = 2;
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.admissionpanelheader);
            this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.DarkGray;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.DarkGray;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(1312, 62);
            this.bunifuGradientPanel1.TabIndex = 0;
            // 
            // admissionpanelheader
            // 
            this.admissionpanelheader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.admissionpanelheader.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("admissionpanelheader.BackgroundImage")));
            this.admissionpanelheader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.admissionpanelheader.Controls.Add(this.admissionstudentadd);
            this.admissionpanelheader.Controls.Add(this.admissionstudentview);
            this.admissionpanelheader.Controls.Add(this.bunifuCustomLabel2);
            this.admissionpanelheader.Controls.Add(this.bunifuCustomLabel1);
            this.admissionpanelheader.Dock = System.Windows.Forms.DockStyle.Top;
            this.admissionpanelheader.GradientBottomLeft = System.Drawing.Color.DarkGray;
            this.admissionpanelheader.GradientBottomRight = System.Drawing.Color.DarkGray;
            this.admissionpanelheader.GradientTopLeft = System.Drawing.Color.White;
            this.admissionpanelheader.GradientTopRight = System.Drawing.Color.White;
            this.admissionpanelheader.Location = new System.Drawing.Point(0, 0);
            this.admissionpanelheader.Name = "admissionpanelheader";
            this.admissionpanelheader.Padding = new System.Windows.Forms.Padding(10);
            this.admissionpanelheader.Quality = 10;
            this.admissionpanelheader.Size = new System.Drawing.Size(1312, 62);
            this.admissionpanelheader.TabIndex = 1;
            // 
            // admissionstudentadd
            // 
            this.admissionstudentadd.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.admissionstudentadd.BackColor = System.Drawing.Color.SteelBlue;
            this.admissionstudentadd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.admissionstudentadd.BorderRadius = 0;
            this.admissionstudentadd.ButtonText = "Add";
            this.admissionstudentadd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.admissionstudentadd.DisabledColor = System.Drawing.Color.Gray;
            this.admissionstudentadd.Dock = System.Windows.Forms.DockStyle.Right;
            this.admissionstudentadd.Iconcolor = System.Drawing.Color.Transparent;
            this.admissionstudentadd.Iconimage = global::UserInterface.Properties.Resources.Plus_100px;
            this.admissionstudentadd.Iconimage_right = null;
            this.admissionstudentadd.Iconimage_right_Selected = null;
            this.admissionstudentadd.Iconimage_Selected = null;
            this.admissionstudentadd.IconMarginLeft = 0;
            this.admissionstudentadd.IconMarginRight = 0;
            this.admissionstudentadd.IconRightVisible = true;
            this.admissionstudentadd.IconRightZoom = 0D;
            this.admissionstudentadd.IconVisible = true;
            this.admissionstudentadd.IconZoom = 90D;
            this.admissionstudentadd.IsTab = true;
            this.admissionstudentadd.Location = new System.Drawing.Point(1096, 10);
            this.admissionstudentadd.Name = "admissionstudentadd";
            this.admissionstudentadd.Normalcolor = System.Drawing.Color.SteelBlue;
            this.admissionstudentadd.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.admissionstudentadd.OnHoverTextColor = System.Drawing.Color.White;
            this.admissionstudentadd.selected = false;
            this.admissionstudentadd.Size = new System.Drawing.Size(100, 42);
            this.admissionstudentadd.TabIndex = 5;
            this.admissionstudentadd.Text = "Add";
            this.admissionstudentadd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.admissionstudentadd.Textcolor = System.Drawing.Color.White;
            this.admissionstudentadd.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admissionstudentadd.Click += new System.EventHandler(this.admissionstudentadd_Click);
            // 
            // admissionstudentview
            // 
            this.admissionstudentview.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.admissionstudentview.BackColor = System.Drawing.Color.SteelBlue;
            this.admissionstudentview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.admissionstudentview.BorderRadius = 0;
            this.admissionstudentview.ButtonText = "View";
            this.admissionstudentview.Cursor = System.Windows.Forms.Cursors.Hand;
            this.admissionstudentview.DisabledColor = System.Drawing.Color.Gray;
            this.admissionstudentview.Dock = System.Windows.Forms.DockStyle.Right;
            this.admissionstudentview.Iconcolor = System.Drawing.Color.Transparent;
            this.admissionstudentview.Iconimage = global::UserInterface.Properties.Resources.Document_96px;
            this.admissionstudentview.Iconimage_right = null;
            this.admissionstudentview.Iconimage_right_Selected = null;
            this.admissionstudentview.Iconimage_Selected = null;
            this.admissionstudentview.IconMarginLeft = 0;
            this.admissionstudentview.IconMarginRight = 0;
            this.admissionstudentview.IconRightVisible = true;
            this.admissionstudentview.IconRightZoom = 0D;
            this.admissionstudentview.IconVisible = true;
            this.admissionstudentview.IconZoom = 90D;
            this.admissionstudentview.IsTab = true;
            this.admissionstudentview.Location = new System.Drawing.Point(1196, 10);
            this.admissionstudentview.Name = "admissionstudentview";
            this.admissionstudentview.Normalcolor = System.Drawing.Color.SteelBlue;
            this.admissionstudentview.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.admissionstudentview.OnHoverTextColor = System.Drawing.Color.White;
            this.admissionstudentview.selected = false;
            this.admissionstudentview.Size = new System.Drawing.Size(106, 42);
            this.admissionstudentview.TabIndex = 3;
            this.admissionstudentview.Text = "View";
            this.admissionstudentview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.admissionstudentview.Textcolor = System.Drawing.Color.White;
            this.admissionstudentview.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admissionstudentview.Click += new System.EventHandler(this.admissionstudentview_Click);
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Minion Pro", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(410, 36);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(101, 17);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "Only Use by Office";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Minion Pro Cond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(408, 10);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(118, 26);
            this.bunifuCustomLabel1.TabIndex = 0;
            this.bunifuCustomLabel1.Text = "Enquiry Form";
            // 
            // admission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.admissionpanelcontainer);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Name = "admission";
            this.Size = new System.Drawing.Size(1312, 592);
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.admissionpanelheader.ResumeLayout(false);
            this.admissionpanelheader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuGradientPanel admissionpanelcontainer;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        public Bunifu.Framework.UI.BunifuFlatButton admissionstudentadd;
        public Bunifu.Framework.UI.BunifuFlatButton admissionstudentview;
        public Bunifu.Framework.UI.BunifuGradientPanel admissionpanelheader;
    }
}
