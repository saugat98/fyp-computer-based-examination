﻿namespace UserInterface
{
    partial class viewusersdetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.userdisplay = new System.Windows.Forms.DataGridView();
            this.colid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colsn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colfullname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colemail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colphone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collabel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colintake = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colimage = new System.Windows.Forms.DataGridViewImageColumn();
            this.colrole = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.userdisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // userdisplay
            // 
            this.userdisplay.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userdisplay.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.userdisplay.BackgroundColor = System.Drawing.Color.PaleTurquoise;
            this.userdisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.userdisplay.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.userdisplay.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.GrayText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userdisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.userdisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userdisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colid,
            this.colsn,
            this.colfullname,
            this.colemail,
            this.colphone,
            this.collabel,
            this.colintake,
            this.colimage,
            this.colrole});
            this.userdisplay.Cursor = System.Windows.Forms.Cursors.Default;
            this.userdisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userdisplay.EnableHeadersVisualStyles = false;
            this.userdisplay.Location = new System.Drawing.Point(0, 0);
            this.userdisplay.Name = "userdisplay";
            this.userdisplay.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.GrayText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userdisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.userdisplay.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.userdisplay.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userdisplay.Size = new System.Drawing.Size(1304, 606);
            this.userdisplay.StandardTab = true;
            this.userdisplay.TabIndex = 1;
            this.userdisplay.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.userdisplay_RowHeaderMouseDoubleClick);
            // 
            // colid
            // 
            this.colid.HeaderText = "User Id";
            this.colid.Name = "colid";
            this.colid.ReadOnly = true;
            this.colid.Visible = false;
            this.colid.Width = 5;
            // 
            // colsn
            // 
            this.colsn.HeaderText = "S.N";
            this.colsn.Name = "colsn";
            this.colsn.ReadOnly = true;
            this.colsn.Width = 60;
            // 
            // colfullname
            // 
            this.colfullname.HeaderText = "Full Name";
            this.colfullname.Name = "colfullname";
            this.colfullname.ReadOnly = true;
            this.colfullname.Width = 180;
            // 
            // colemail
            // 
            this.colemail.HeaderText = "E-mail";
            this.colemail.Name = "colemail";
            this.colemail.ReadOnly = true;
            this.colemail.Width = 180;
            // 
            // colphone
            // 
            this.colphone.HeaderText = "Phone";
            this.colphone.Name = "colphone";
            this.colphone.ReadOnly = true;
            this.colphone.Width = 180;
            // 
            // collabel
            // 
            this.collabel.HeaderText = "Label";
            this.collabel.Name = "collabel";
            this.collabel.ReadOnly = true;
            // 
            // colintake
            // 
            this.colintake.HeaderText = "Intake";
            this.colintake.Name = "colintake";
            this.colintake.ReadOnly = true;
            this.colintake.Width = 130;
            // 
            // colimage
            // 
            this.colimage.HeaderText = "Image";
            this.colimage.Name = "colimage";
            this.colimage.ReadOnly = true;
            this.colimage.Width = 120;
            // 
            // colrole
            // 
            this.colrole.HeaderText = "Role";
            this.colrole.Name = "colrole";
            this.colrole.ReadOnly = true;
            this.colrole.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colrole.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(1198, 169);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 2;
            // 
            // viewusersdetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 606);
            this.Controls.Add(this.userdisplay);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "viewusersdetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "viewusersdetail";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.userdisplay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView userdisplay;
        private System.Windows.Forms.DataGridViewTextBoxColumn colid;
        private System.Windows.Forms.DataGridViewTextBoxColumn colsn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colfullname;
        private System.Windows.Forms.DataGridViewTextBoxColumn colemail;
        private System.Windows.Forms.DataGridViewTextBoxColumn colphone;
        private System.Windows.Forms.DataGridViewTextBoxColumn collabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colintake;
        private System.Windows.Forms.DataGridViewImageColumn colimage;
        private System.Windows.Forms.DataGridViewTextBoxColumn colrole;
        private System.Windows.Forms.Panel panel1;
    }
}