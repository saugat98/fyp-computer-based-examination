﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Types;
using Oracle.DataAccess.Client;
using DAL;
using BIL;
using System.Data;

namespace DLL
{
   public class CRUD_operation
    {
        Sql Crud = new Sql();
        public int RegisterCourse(information info)//user registration to database
        {
            OracleParameter[] param = new OracleParameter[]
           {
               new OracleParameter("course",info.Course),
               new OracleParameter("faculty",info.Faculty),
               new OracleParameter("label",info.Label),
               new OracleParameter("descp",info.Description),
               
           };
            string sql = "Proc_course_Register";
            int i = Crud.Nonexecutable(sql, param, CommandType.StoredProcedure);
            return i;

        }
        public int UpdateCourse(information info)//user registration to database
        {
            string sql = "Proc_course_Update";
            OracleParameter[] param = new OracleParameter[]
           {
               new OracleParameter("course",info.Course),
               new OracleParameter("fac",info.Faculty),
               new OracleParameter("Stu_label",info.Label),
               new OracleParameter("descp",info.Description),
                new OracleParameter("courseid ",info.Course_id)

           };
            
            int i = Crud.Nonexecutable(sql, param, CommandType.StoredProcedure);
            return i;

        }
        public int DeleteCourse(information info)//user registration to database
        {
            string sql = "Proc_course_Delete";
            OracleParameter[] param = new OracleParameter[]
           {
               
                new OracleParameter("courseid ",info.Course_id)

           };

            int i = Crud.Nonexecutable(sql, param, CommandType.StoredProcedure);
            return i;

        }
        public DataTable Check_existed_course(information info)//check user data which already stored or not
        {
            string sql= "select * from sitaram47.tbl_course where Upper(course_name)=Upper('"+info.Course+"') and upper(faculty)=upper('"+info.Faculty+"') and upper(label)=upper('"+info.Label+"') ";
            OracleParameter[] param = new OracleParameter[]
          {
              
          };
            DataTable dt = Crud.ViewQuery(sql, param,CommandType.Text);
            return dt;

        }
       
        public DataTable Display_course()
        {
            string sql = "select * from sitaram47.tbl_course order by course_id desc";
            OracleParameter[] param = new OracleParameter[]
          {
               

          };
            DataTable dt = Crud.ViewQuery(sql, param, CommandType.Text);
            return dt;

        }
        public DataTable View_for_Update_course(information info)
        {
            string sql = "select * from sitaram47.tbl_course where course_id="+info.Course_id;
            OracleParameter[] param = new OracleParameter[]
          {


          };
            DataTable dt = Crud.ViewQuery(sql, param, CommandType.Text);
            return dt;

        }
        //Proc_Security_Register
        public int RegisterSecurity(information info)//user registration to database
        {
            OracleParameter[] param = new OracleParameter[]
           {
               new OracleParameter("questions",info.questions),
               new OracleParameter("descp",info.Description),

           };
            string sql = "Proc_Security_Register";
            int i = Crud.Nonexecutable(sql, param, CommandType.StoredProcedure);
            return i;

        }

        public DataTable Check_existed_security(information info)
        {
            string sql = "select * from sitaram47.tbl_security_question where Upper(questions_title)=Upper('" + info.questions + "')";
            OracleParameter[] param = new OracleParameter[]
          {
               //new OracleParameter("course",info.Course),
               //new OracleParameter("Stu_fac",info.Faculty),
               //new OracleParameter("Stu_label",info.Label)

          };
            DataTable dt = Crud.ViewQuery(sql, param, CommandType.Text);
            return dt;
        }
        public DataTable Display_Security()
        {
            string sql = "select * from sitaram47.tbl_security_question";
            OracleParameter[] param = new OracleParameter[]
          {


          };
            DataTable dt = Crud.ViewQuery(sql, param, CommandType.Text);
            return dt;

        }
        public DataTable View_for_Update_security(information info)
        {
            string sql = "select * from sitaram47.tbl_security_question where questions_id=" + info.questions_id;
            OracleParameter[] param = new OracleParameter[]
          {


          };
            DataTable dt = Crud.ViewQuery(sql, param, CommandType.Text);
            return dt;

        }
    }
}
