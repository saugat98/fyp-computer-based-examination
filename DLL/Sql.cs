﻿using BIL;
using DAL;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL
{
    public class Sql
    {
        DB_Connection connect = new DB_Connection();
        information info = new information();
        /*-----------------------------------------Function for Create, delete and update database---------------------------------------------------------*/
        public int Nonexecutable(string sql, OracleParameter[] param, CommandType cmdtype)
        {
            using (OracleConnection conn = connect.DatabaseConn())
            {
                OracleCommand cmd = new OracleCommand(sql, conn);

                cmd.CommandType = cmdtype;
                if (param != null)
                {
                    cmd.Parameters.AddRange(param);
                }

                return cmd.ExecuteNonQuery();
            }
        }

        /*-----------------------------------------end function---------------------------------------------------------*/

        /*-----------------------------------------Display Data from database---------------------------------------------------------*/
        public DataTable ViewQuery(string sql, OracleParameter[] param, CommandType cmdtype)
        {
            using (OracleConnection conn = connect.DatabaseConn())
            {
                OracleCommand cmd = new OracleCommand(sql, conn);
                cmd.CommandType = cmdtype;
                if (param != null)
                {
                    cmd.Parameters.AddRange(param);
                }
                OracleDataAdapter sd = new OracleDataAdapter(cmd);
                DataTable dt = new DataTable();

                sd.Fill(dt);
                return dt;

            }
        }

        /*-----------------------------------------end function---------------------------------------------------------*/
    }
}
