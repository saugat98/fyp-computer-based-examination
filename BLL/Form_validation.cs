﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BIL;
using System.Windows.Forms;

namespace BLL
{
   public class Form_validation
    {
        information info = new information();
        public bool Course_empty_validation(information info)
        {
            if (info.Stu_label.Equals(0)|| info.Course.Equals("Enter Course Name Here") || info.Stu_fac.Equals(0)
                || info.Description.Equals("Describe Course In Details"))
            {
                MessageBox.Show("Empty Form Fields!");
                return false;
            }
            if (info.Course.Length < 6 || info.Course.Length>100)
            {
                MessageBox.Show("Course need to be between 6 and 100 words");
                return false;
            }
            if (info.Description.Length < 70 || info.Description.Length > 300)
            {
                MessageBox.Show("Description need to be between 70 and 300 words");
                return false;
            }

            return true;
        }

        public bool Security_empty_validation(information info)
        {
            if (info.questions.Equals("Enter Security Questions")
                || info.Description.Equals("Describe in details"))
            {
                MessageBox.Show("Empty Form Fields!");
                return false;
            }
            if (info.questions.Length < 10)
            {
                MessageBox.Show("Questions need to be more than 10 words");
                return false;
            }
            if (info.Description.Length < 60 || info.Description.Length > 200)
            {
                MessageBox.Show("Description  need to be between 70 and 200 Words");
                return false;
            }

            return true;
        }


    }
}
