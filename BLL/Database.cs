﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BIL;
using DLL;
using System.Windows.Forms;
using System.Data;

namespace BLL
{
    public class Database
    {
        information info = new information();
        CRUD_operation Db_opera = new CRUD_operation();
        public bool Course_register( information desc){
           int i= Db_opera.RegisterCourse(desc);
            if (i == 0)
            {
                MessageBox.Show("Sorry! try again, Database Error. ", "Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
            }
        public bool Course_update(information desc)
        {
            int i = Db_opera.UpdateCourse(desc)*-1;
            if (i == 0)
            {
                MessageBox.Show("Sorry! try again, Database Error. ", "Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        public bool Course_Delete(information desc)
        {
            int i = Db_opera.DeleteCourse(desc) * -1;
            if (i == 0)
            {
                MessageBox.Show("Sorry! try again, Database Error. ", "Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public bool Check_existed_course(information desc)
        {
            DataTable dt = Db_opera.Check_existed_course(desc);
            if (dt.Rows.Count >0)
            {
                MessageBox.Show( "Course Already Exist!", "Already Existed Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
        public bool Check_existed_course_update(information desc)
        {
            DataTable dt = Db_opera.Check_existed_course(desc);
            if (dt.Rows.Count>1)
            {
                MessageBox.Show("Course Already Exist! Can not Update", "Existed Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            
            return true;
        }

        public DataTable Display_course()
        {
            DataTable dt = Db_opera.Display_course();
            return dt;
            
        }
        public DataTable View_for_Update_course(information info)
        {
            DataTable dt = Db_opera.View_for_Update_course(info);
            return dt;

        }
        public bool RegisterSecurity(information desc)
        {
            int i = Db_opera.RegisterSecurity(desc);
            if (i == 0)
            {
                MessageBox.Show("Sorry! try again, Database Error. ", "Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        //
        public bool Check_existed_security(information desc)
        {
            DataTable dt = Db_opera.Check_existed_security(desc);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Questions Already Exist!", "Existed Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
        public DataTable Display_Security()
        {
            DataTable dt = Db_opera.Display_Security();
            return dt;

        }
        public DataTable View_for_Update_Security(information info)
        {
            DataTable dt = Db_opera.View_for_Update_security(info);
            return dt;

        }
    }
}
